/**
 * @team: FsFlex Team
 * @since: 1.0.0
 * @author: KP
 */
(function ($) {
    var extra_service = {};
    var order_price = {};
    var _edit_order = false;
    var _edit_btn_sm = 2;
    var _data = {
            action: 'swh_filter',
        },
        form_data = {},
        _avai = true;
    var _g = $('.swh-checking-form').attr('data-g-m');
    var _swh_search = {
        init: function () {
            var _thisss = this;
            this.handles.select2Define();
            $(document).ready(function () {
                _thisss.handles.dateRange(false);
                $(document).on('click', '.swh-btn-trig-priplan', function (e) {
                    e.preventDefault();
                    $('.cms-bootstrap-tabs a[href="#room-tab-pricing"]').trigger('click');
                });
                if (typeof extra_service_checkout !== 'undefined') {
                    extra_service = extra_service_checkout;
                }
                if (typeof order_price_checkout !== 'undefined') {
                    order_price = order_price_checkout;
                }
            });
            this.events.submitFilter();
            this.events.bookingSubmit();
            this.events.upDownGuests();
            this.events.priceRange();
            this.events.otherClickEvents();
            this.events.checkOutButtonHandle();
            this.events.dashboardUpdatePass();
            this.events.updateProfile();
            this.events.changeExtraServiceOrder();
            this.handles.submitBookingHanle();
            this.events.saveActiveTabCookie();
        },
        events: {
            otherClickEvents: function () {
                $(document).on('change','.sort_by',function (e) {
                    e.preventDefault();
                    console.log('123');
                    _swh_search.handles.ajaxFilter($('.swh-search-layout-4'));
                });

                $(document).on('click', '.btn-change-pass', function (e) {
                    e.preventDefault();
                    var _p = $(this).parent().parent().next();
                    if (_p.css('display') === 'block') {
                        _p.slideToggle("fast", function () {
                        });
                        _p.removeClass('is-active');
                    } else {
                        _p.slideToggle("fast", function () {
                            _p.css('display', 'block');
                        });
                        _p.addClass('is-active');
                    }
                });
                $(document).on('click', '.swh-ep.is-active', function (e) {
                    e.preventDefault();
                    return false;
                });

                $(document).on('click', '.swh-btn-cancel', function (e) {
                    e.preventDefault();
                    $(document).find('.swh-popup-edit-order').addClass('o-hidden');
                    setTimeout(function () {
                        $(document).find('.swh-popup-edit-order').remove();
                    }, 1001);
                });

                $(document).on('click', '.swh-popup-edit-order', function (e) {
                    // e.preventDefault();
                    if ($(e.target)[0] === $(this)[0]) {
                        $(document).find('.swh-popup-edit-order').addClass('o-hidden');
                        setTimeout(function () {
                            $(document).find('.swh-popup-edit-order').remove();
                        }, 1001);
                    }
                });

                $(document).on('click', '.swh-order-delete', function (e) {
                    e.preventDefault();
                    var _this = $(this),
                        _oid = _this.attr('data-oid');
                    var _cf = confirm(search_obj.confirm_delete_order);
                    if (_cf === true) {
                        $.ajax({
                            url: search_obj.ajax_url,
                            type: 'POST',
                            beforeSend: function () {
                                _this.parents('.item-inner').before('<div class="swh-dashboard-loading" style="display: block;">\n' +
                                    '        <span class="fa fa-spinner fa-spin"></span>\n' +
                                    '    </div>');
                                $('.swh-dashboard-loading').css('display', 'block');
                            },
                            data: {
                                action: 'dashboard_delete_order',
                                _o_id: _oid
                            }
                        })
                            .done(function (data) {
                                if (data.stt === 'error') {
                                    var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                    $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                    setTimeout(function () {
                                        $('.' + x_cl).remove();
                                    }, 4000);
                                    return true;
                                }
                                if (data.stt === 'success') {
                                    var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                    $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                    setTimeout(function () {
                                        $('.' + x_cl).remove();
                                        window.location.reload();
                                    }, 2000);
                                    return true;
                                }
                            })
                            .fail(function () {
                                return false;
                            })
                            .always(function () {
                                $('.swh-dashboard-loading').remove();
                                return false;
                            });
                    }
                    return false;
                });
                $(document).on('click', '.swh-order-edit', function (e) {
                    e.preventDefault();
                    var _this = $(this),
                        _oid = _this.attr('data-oid');
                    $.ajax({
                        url: search_obj.ajax_url,
                        type: 'POST',
                        beforeSend: function () {
                            _this.parents('.item-inner').before('<div class="swh-dashboard-loading" style="display: block;">\n' +
                                '        <span class="fa fa-spinner fa-spin"></span>\n' +
                                '    </div>');
                            $('.swh-dashboard-loading').css('display', 'block');
                        },
                        data: {
                            action: 'dashboard_edit_order_layout',
                            _o_id: _oid
                        }
                    })
                        .done(function (data) {
                            if (data.stt === 'error') {
                                var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                setTimeout(function () {
                                    $('.' + x_cl).remove();
                                }, 4000);
                                return true;
                            }
                            if (data.stt === 'success') {
                                extra_service = data.ex;
                                order_price = data.order_price;
                                _edit_order = true;
                                _this.parents('body').prepend('<div class="swh-popup-edit-order o-hidden">' + data.layout + '</div>');
                                setTimeout(function () {
                                    $(document).find('.swh-popup-edit-order').removeClass('o-hidden');
                                    _swh_search.handles.dateRange(true);
                                }, 100);
                                return true;
                            }
                        })
                        .fail(function () {
                            return false;
                        })
                        .always(function () {
                            $('.swh-dashboard-loading').remove();
                            return false;
                        });
                    return false;
                });

                $(document).on('click', '.swh-btn-update', function (e) {
                    e.preventDefault();
                    var _this = $(this);
                    if (_edit_btn_sm !== 1) {
                        return false;
                    }
                    var _sv_l_d = [];
                    $(document).find('input.swh-ex-sv:checked').each(function () {
                        var _this_s = $(this);
                        _sv_l_d.push(parseInt(_this_s.val()));
                    });
                    var _ex_s = _sv_l_d.join();
                    $.ajax({
                        url: search_obj.ajax_url,
                        type: 'POST',
                        beforeSend: function () {
                            _this.parents('.container').before('<div class="swh-dashboard-loading" style="display: block;">\n' +
                                '        <span class="fa fa-spinner fa-spin"></span>\n' +
                                '    </div>');
                            $('.swh-dashboard-loading').css('display', 'block');
                        },
                        data: {
                            action: 'dashboard_update_order',
                            _o_id: _this.attr('data-oid'),
                            _st_date: $("#swh-room-check-in").val(),
                            _en_date: $("#swh-room-check-out").val(),
                            _guest: $('#check-number-guests').val(),
                            _ex_s: _ex_s

                        }
                    })
                        .done(function (data) {
                            if (data.stt === 'error') {
                                var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                setTimeout(function () {
                                    $('.' + x_cl).remove();
                                }, 4000);
                                return true;
                            }
                            if (data.stt === 'successfully') {
                                var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                setTimeout(function () {
                                    $('.' + x_cl).remove();
                                    window.location.reload();
                                }, 2000);
                                return true;
                            }
                        })
                        .fail(function () {
                            return false;
                        })
                        .always(function () {
                            $('.swh-dashboard-loading').remove();
                            return false;
                        });

                });

                $(document).on('change', 'input[type=radio][name=swh-follow]', function (e) {
                    e.preventDefault();
                    var _this = $(this);
                    $.ajax({
                        url: search_obj.ajax_url,
                        type: 'POST',
                        beforeSend: function () {
                        },
                        data: {
                            action: 'swh_user_change_follow',
                            _new_val: _this.val()
                        }
                    })
                        .done(function (data) {
                            if (data.stt === 'error') {
                                var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                setTimeout(function () {
                                    $('.' + x_cl).remove();
                                }, 4000);
                                return true;
                            }
                            if (data.stt === 'success') {
                                var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                setTimeout(function () {
                                    $('.' + x_cl).remove();
                                }, 4000);
                                return true;
                            }
                        })
                        .fail(function () {
                            return false;
                        })
                        .always(function () {
                            return false;
                        });
                });
                $(document).on('change', 'input[type=checkbox]#swh-receive-email', function (e) {
                    e.preventDefault();
                    var _this = $(this);
                    var _val = _this.is(":checked");
                    $.ajax({
                        url: search_obj.ajax_url,
                        type: 'POST',
                        beforeSend: function () {
                        },
                        data: {
                            action: 'swh_user_change_receive_notice',
                            _new_val: _val,
                            _type: 'swh-receive-email'
                        }
                    })
                        .done(function (data) {
                            if (data.stt === 'error') {
                                var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                setTimeout(function () {
                                    $('.' + x_cl).remove();
                                }, 4000);
                                return true;
                            }
                            if (data.stt === 'success') {
                                var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                setTimeout(function () {
                                    $('.' + x_cl).remove();
                                }, 4000);
                                return true;
                            }
                        })
                        .fail(function () {
                            return false;
                        })
                        .always(function () {
                            return false;
                        });
                });
                $(document).on('change', 'input[type=checkbox]#swh-receive-booking', function (e) {
                    e.preventDefault();
                    var _this = $(this);
                    var _val = _this.is(":checked");
                    $.ajax({
                        url: search_obj.ajax_url,
                        type: 'POST',
                        beforeSend: function () {
                        },
                        data: {
                            action: 'swh_user_change_receive_notice',
                            _new_val: _val,
                            _type: 'swh-receive-booking'
                        }
                    })
                        .done(function (data) {
                            if (data.stt === 'error') {
                                var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                setTimeout(function () {
                                    $('.' + x_cl).remove();
                                }, 4000);
                                return true;
                            }
                            if (data.stt === 'success') {
                                var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                setTimeout(function () {
                                    $('.' + x_cl).remove();
                                }, 4000);
                                return true;
                            }
                        })
                        .fail(function () {
                            return false;
                        })
                        .always(function () {
                            return false;
                        });
                });
            },
            dashboardUpdatePass: function () {
                $(document).on('click', '.swh-change-pass', function (e) {
                    e.preventDefault();
                    var _this = $(this),
                        _this_form = _this.parents('.infor-change-popup'),
                        _current_pass = _this_form.find('#current_password'),
                        _pass1 = _this_form.find('#new_password'),
                        _pass2 = _this_form.find('#new_password_re');
                    _current_pass.removeClass('swh-error');
                    _pass1.removeClass('swh-error');
                    _pass2.removeClass('swh-error');
                    $(document).find('.swh-notice-f').remove();
                    if (_current_pass.val() === "") {
                        _current_pass.addClass('swh-error');
                        return;
                    }
                    if (_pass1.val() === "") {
                        _pass1.addClass('swh-error');
                        return;
                    }
                    if (_pass2.val() === "") {
                        _pass2.addClass('swh-error');
                        return;
                    }
                    if (_pass1.val() !== _pass2.val()) {
                        _pass1.addClass('swh-error');
                        _pass2.addClass('swh-error');
                        _this.before('<p class="swh-notice-f swh-field-error">' + swh_notice_msg.pass_confirm + '</p>');
                        return false;
                    }
                    $.ajax({
                        url: search_obj.ajax_url,
                        type: 'POST',
                        beforeSend: function () {
                            $('.swh-dashboard-loading').css('display', 'block');
                        },
                        data: {
                            action: 'dashboard_change_pass',
                            old_pass: _current_pass.val(),
                            new_pass: _pass1.val(),
                        }
                    })
                        .done(function (data) {
                            if (data.status === 'error') {
                                _this.before('<p class="swh-notice-f swh-field-error">' + data.msg + '</p>');
                                return true;
                            }
                            if (data.status === 'success') {
                                _this.before('<p class="swh-notice-f swh-field-success">' + data.msg + '</p>');
                                return true;
                            }
                        })
                        .fail(function () {
                            return false;
                        })
                        .always(function () {
                            $('.swh-dashboard-loading').css('display', 'none');
                            return false;
                        });
                });
            },
            updateProfile: function () {
                $(document).on('click', '.swh-save-profile-info', function (e) {
                    e.preventDefault();
                    var _this = $(this),
                        _check = true,
                        _this_form = _this.parents('form'),
                        _top_f = _this.parents('.col-user-content'),
                        _l_name = _this_form.find('#l_name'),
                        _surname = _this_form.find('#surname'),
                        _email = _this_form.find('#email'),
                        _phone = _this_form.find('#phone'),
                        _address = _this_form.find('#address'),
                        _city = _this_form.find('#city');
                    var _fields = [_l_name, _surname, _phone, _address, _city];
                    _fields.forEach(function (item, index) {
                        item.removeClass('swh-error');
                        if (item[0].hasAttribute('required') && item.val() === "") {
                            item.addClass('swh-error');
                            _top_f[0].scrollIntoView({
                                behavior: 'smooth',
                                block: "start",
                                inline: "start"
                            });
                            _check = false;
                            return false;
                        }
                    });
                    if (_email.length > 0) {
                        var atpos = _email.val().indexOf("@");
                        var dotpos = _email.val().lastIndexOf(".");
                        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= _email.val().length) {
                            _email.addClass('swh-error');
                            _top_f[0].scrollIntoView({
                                behavior: 'smooth',
                                block: "start",
                                inline: "start"
                            });
                            return false;
                        }
                    } else {
                        _email.addClass('swh-error');
                        _top_f[0].scrollIntoView({
                            behavior: 'smooth',
                            block: "start",
                            inline: "start"
                        });
                        return false;
                    }
                    if (!_check) return;
                    $.ajax({
                        url: search_obj.ajax_url,
                        type: 'POST',
                        beforeSend: function () {
                            $('.swh-dashboard-loading').css('display', 'block');
                        },
                        data: {
                            action: 'dashboard_save_profile',
                            _l_name: _l_name.val(),
                            _surname: _surname.val(),
                            _email: _email.val(),
                            _phone: _phone.val(),
                            _address: _address.val(),
                            _city: _city.val(),
                        }
                    })
                        .done(function (data) {
                            if (data.status === 'error') {
                                var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                setTimeout(function () {
                                    $('.' + x_cl).remove();
                                }, 4000);
                                return true;
                            }
                            if (data.stt === 'success') {
                                var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                setTimeout(function () {
                                    $('.' + x_cl).remove();
                                    window.location.reload();
                                }, 5000);
                                return true;
                            }
                        })
                        .fail(function () {
                            return false;
                        })
                        .always(function () {
                            $('.swh-dashboard-loading').css('display', 'none');
                            return false;
                        });
                });
            },
            upDownGuests: function () {
                $(document).on('click', '.swh-plus-guest', function (e) {
                    e.preventDefault();
                    var _parent = $(this).parents('.swh-search-guests'),
                        _input_val = _parent.find('input[name=number-guests]'),
                        _val = 1;
                    if (_input_val.length > 0 && _input_val.val() !== "") {
                        _val = _input_val.val();
                    }
                    if (parseInt(_val) > 0) {
                        var new_val = parseInt(_val) + 1;
                        _input_val.val(new_val);
                        _parent.find('.guest-layout span').html(new_val);
                    } else {
                        return false;
                    }
                });
                $(document).on('click', '.swh-minus-guest', function (e) {
                    e.preventDefault();
                    var _parent = $(this).parents('.swh-search-guests'),
                        _input_val = _parent.find('input[name=number-guests]'),
                        _val = 1;
                    if (_input_val.length > 0 && _input_val.val() !== "") {
                        _val = _input_val.val();
                    }
                    if (parseInt(_val) > 1) {
                        var new_val = parseInt(_val) - 1;
                        _input_val.val(new_val);
                        _parent.find('.guest-layout span').html(new_val);
                    } else {
                        return false;
                    }
                });
                $(document).on('click', '.swh-check-plus-guest', function (e) {
                    e.preventDefault();
                    var _parent = $(this).parents('.swh-checking-form');
                    if (_edit_order) {
                        _parent = $(this).parents('.swh-checking-form2');
                    }
                    var _input_val = _parent.find('#check-number-guests'),
                        _val = 1;
                    if (_input_val.length > 0 && _input_val.val() !== "") {
                        _val = _input_val.val();
                    }
                    _g = _parent.attr('data-g-m');
                    if (parseInt(_val) > 0 && parseInt(_val) < _g) {
                        var new_val = parseInt(_val) + 1;
                        _input_val.val(new_val);
                        _parent.find('.check-guest-layout span').html(new_val);
                        if (_edit_order) {
                            _swh_search.handles.updateLayoutForm($("#swh-room-check-in"), true);
                        }
                    } else {
                        return false;
                    }
                });
                $(document).on('click', '.swh-check-minus-guest', function (e) {
                    e.preventDefault();
                    var _parent = $(this).parents('.swh-checking-form');
                    if (_edit_order) {
                        _parent = $(this).parents('.swh-checking-form2');
                    }
                    var _input_val = _parent.find('#check-number-guests'),
                        _val = 1;
                    if (_input_val.length > 0 && _input_val.val() !== "") {
                        _val = _input_val.val();
                    }
                    if (parseInt(_val) > 1) {
                        var new_val = parseInt(_val) - 1;
                        _input_val.val(new_val);
                        _parent.find('.check-guest-layout span').html(new_val);
                        if (_edit_order) {
                            _swh_search.handles.updateLayoutForm($("#swh-room-check-in"), true);
                        }
                    } else {
                        return false;
                    }
                });
            },
            submitFilter: function () {
                $(document).on('click', '.swh-filter-sm', function (e) {
                    e.preventDefault();
                    var _this_sm = $(this),
                        _parent_form = _this_sm.parents('form');
                    if (!_avai) {
                        return;
                    }
                    _avai = false;
                    var _check = true;
                    if (typeof search_obj === 'undefined' || !search_obj.is_swh_archive) {
                        _parent_form.submit();
                    } else {
                        _swh_search.handles.ajaxFilter(_parent_form);
                    }
                });
                $(document).on('click', '.page-numbers1', function (e) {
                    if (typeof search_obj !== 'undefined' && search_obj.is_swh_archive) {
                        e.preventDefault();
                        var _this_p = $(this);
                        var __page = 0;
                        if (_this_p.parent().find('.prev').length > 0) {
                            __page = _this_p.index();
                        } else {
                            __page = _this_p.index() + 1;
                        }
                        if (_this_p.hasClass('current1')) {
                            return;
                        }
                        if (_this_p.hasClass('prev1')) {
                            __page = _this_p.parent().find('.current1').index() - 1;
                        }
                        if (_this_p.hasClass('next')) {
                            if (_this_p.parent().find('.prev1').length > 0) {
                                __page = _this_p.parent().find('.current1').index() + 1;
                            } else {
                                __page = _this_p.parent().find('.current1').index() + 2;
                            }
                        }
                        document.getElementById("content").scrollIntoView({
                            behavior: 'smooth',
                            block: "start",
                            inline: "start"
                        });
                        var new_data = _data;
                        new_data['swh_page1'] = __page;
                        $.ajax({
                            url: search_obj.ajax_url,
                            type: 'POST',
                            beforeSend: function () {
                                $('#swh-rooms').removeClass('swh-effect');
                                $('#swh-rooms').removeClass('active');
                                $('#swh-rooms').addClass('swh-effect');
                            },
                            data: new_data
                        })
                            .done(function (data) {
                                if (data.status === 'done') {
                                    $('#swh-rooms').html(data.layout);
                                    $('#swh-rooms').addClass('active');
                                }
                            })
                            .fail(function () {
                                return false;
                            })
                            .always(function () {
                                return false;
                            });
                    }
                });
            },
            bookingSubmit: function () {
                $(document).on('focus', '.swh-form-check-count input', function (e) {
                    $(this).removeClass('swh-f-error');
                });
                $(document).on('click', 'a.swh-btn-done-order', function (e) {
                    e.preventDefault();
                    var f_name = $('input[name="your-name"]'),
                        _surname = $('input[name="your-surname"]'),
                        _email = $('input[name="your-email"]'),
                        _phone = $('input[name="text-phone"]'),
                        _address = $('input[name="your-address"]'),
                        _zipcode = $('input[name="text-zip"]'),
                        _msg = $('textarea[name="your-message"]'),
                        _payment = $('input[name="swh-payment-method"]:checked'),
                        _terms = $('input[name="swh-terms"]:checked'),
                        _checkout_check = true;
                    f_name.removeClass('swh-f-error');
                    _surname.removeClass('swh-f-error');
                    _email.removeClass('swh-f-error');
                    _phone.removeClass('swh-f-error');
                    _address.removeClass('swh-f-error');

                    if (f_name.val() === "") {
                        f_name.addClass('swh-f-error');
                        _checkout_check = false;
                    }
                    if (_surname.val() === "") {
                        _surname.addClass('swh-f-error');
                        _checkout_check = false;
                    }
                    if (_email.val() === "") {
                        _email.addClass('swh-f-error');
                        _checkout_check = false;
                    }
                    if (_phone.val() === "") {
                        _phone.addClass('swh-f-error');
                        _checkout_check = false;
                    }
                    if (_address.val() === "") {
                        _address.addClass('swh-f-error');
                        _checkout_check = false;
                    }
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (_email.val() === "" || re.test(_email.val()) === false) {
                        _email.addClass('swh-f-error');
                        _checkout_check = false;
                    }
                    if (_payment.val() === "" || (_payment.val() !== "paypal" && _payment.val() !== "offline" && _payment.val() !== "bank")) {
                        _checkout_check = false;
                    }
                    if (_terms.length < 1) {
                        var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                        $('body').prepend('<div class="' + x_cl + '">' + swh_order_data['terms_error'] + '</div>');
                        setTimeout(function () {
                            $('.' + x_cl).remove();
                        }, 5000);
                        _checkout_check = false;
                    }
                    if (_checkout_check) {
                        var _new_data = {};
                        _new_data['_name'] = f_name.val();
                        _new_data['_surname'] = _surname.val();
                        _new_data['_email'] = _email.val();
                        _new_data['_phone'] = _phone.val();
                        _new_data['_address'] = _address.val();
                        _new_data['_zipcode'] = _zipcode.val();
                        _new_data['_payment'] = _payment.val();
                        _new_data['_msg'] = _msg.val();
                        $.ajax({
                            url: search_obj.ajax_url,
                            type: 'POST',
                            beforeSend: function () {
                                $('.swh-order-loading').css('display', 'block');
                            },
                            data: {
                                action: 'swh_checkout_handles',
                                form_data: _new_data
                            }
                        }).done(function (data) {
                                if (data.status === 'error') {
                                  alert('error');
                                    var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                    $('body').prepend('<div class="' + x_cl + '">' + data.message + '</div>');
                                    setTimeout(function () {
                                        $('.' + x_cl).remove();
                                    }, 5000);
                                } else {
                                    if (data.status === 'success' && data.url !== false) {
                                        window.location.href = data.url;
                                    } else {
                                       $('.customModel').css('display', 'block');
                                       alert('success');
                                        $('.swh-order-loading').after(data.message);
                                        setTimeout(function () {
                                            $('.swh-order-notification').addClass('open');
                                        }, 100);
                                    }
                                }
                            }).fail(function () {
                                return false;
                            }).always(function () {
                                $('.swh-order-loading').css('display', 'none');
                                return false;
                            });
                    } else {
                        $('.tab-reservation-room')[0].scrollIntoView({
                            behavior: "smooth",
                            block: "start",
                            inline: "start"
                        });
                    }
                });
                $(document).on('click', '.swh-noti-close', function (e) {
                    e.preventDefault();
                    var _this = $(this);
                    _this.parents('.swh-order-notification').removeClass('open');
                    setTimeout(function () {
                        _this.parents('.swh-order-notification').remove();
                        window.location.href = search_obj.room_page;
                    }, 300);
                });
            },
            priceRange: function () {
                var _price_range = $(".swh-price-filter");
                _price_range.slider({
                    range: "min",
                    min: 1,
                    max: parseInt(_price_range.attr('data-max')),
                    value: parseInt(_price_range.attr('data-max')),
                    slide: function (event, ui) {
                        var _p = _price_range.attr('data-price-format').replace('111', ui.value);
                        _price_range.parent().find('.swh-p-filter').html(_p);
                    },
                    change: function (event, ui) {
                        _price_range.parent().find('.swh-price-ft-val').val(ui.value);
                        _swh_search.handles.ajaxFilter($('.swh-search-layout-4'));
                    }
                });
            },
            changeExtraServiceOrder: function () {
                $(document).on('change', 'input.swh-ex-sv', function () {
                    var _rs = _swh_search.handles.calcOrderPrice();
                    $(document).find('.swh-value').html(_rs);
                });
                $(document).on('change', 'input[name="swh_select_extra_sv"]', function () {
                    var _check = $(this);
                    if (_check.val() === 'select_all') {
                        $(document).find('input.swh-ex-sv').each(function () {
                            $(this).prop('checked', true);
                        });
                        var _rs = _swh_search.handles.calcOrderPrice();
                        $(document).find('.swh-value').html(_rs);
                        return;
                    }
                    if (_check.val() === 'deselect_all') {
                        $(document).find('input.swh-ex-sv').each(function () {
                            $(this).prop('checked', false);
                        });
                        var _rs = _swh_search.handles.calcOrderPrice();
                        $(document).find('.swh-value').html(_rs);
                        return;
                    }
                });
            },
            checkOutButtonHandle: function () {
                $(document).on('click', '.swh-btn-reser', function (e) {
                    e.preventDefault();
                    $(this).parents('.cms-bootstrap-tabs-checkout').find('a[href="#tab-rome-reservation"]').trigger('click');
                });

            },
            saveActiveTabCookie: function () {
                $(document).on('click', 'a[href="#tab-rome-reservation"]', function (e) {
                    if ($(this).parent().hasClass('active')) return;
                    _swh_search.handles.saveActiveTabCheckout('t_2');
                });
                $(document).on('click', 'a[href="#tab-rome-rate"]', function (e) {
                    if ($(this).parent().hasClass('active')) return;
                    _swh_search.handles.saveActiveTabCheckout('');
                });
            }
        },
        handles: {
            ajaxFilter: function (_parent_form) {
                $.ajax({
                    url: search_obj.ajax_url,
                    type: 'POST',
                    beforeSend: function () {
                        $('#swh-rooms').removeClass('swh-effect');
                        $('#swh-rooms').removeClass('active');
                        $('#swh-rooms').addClass('swh-effect');
                    },
                    data: {
                        action: 'swh_filter',
                        swh_type: _parent_form.find('select[name=room-type]').val(),
                        date_start: _parent_form.find('#swh-ft-check-in').val(),
                        date_end: _parent_form.find('#swh-ft-check-out').val(),
                        number_guests: _parent_form.find('select[name=number-guests]').val(),
                        max_price: $(document).find('.swh-price-ft-val').val(),
                        sort_by: $(document).find('.sort_by').val(),
                    }
                })
                    .done(function (data) {
                        if (data.status === 'done') {
                            $('#swh-rooms').html(data.layout);
                            _data = {
                                action: 'swh_filter',
                                swh_type: _parent_form.find('select[name=room-type]').val(),
                                date_start: _parent_form.find('#swh-ft-check-in').val(),
                                date_end: _parent_form.find('#swh-ft-check-out').val(),
                                number_guests: _parent_form.find('select[name=number-guests]').val(),
                                sort_by: $(document).find('.sort_by').val()
                            };
                            $('#swh-rooms').addClass('active');
                        }
                    })
                    .fail(function () {
                        return false;
                    })
                    .always(function () {
                        _avai = true;
                        return false;
                    });
            },
            select2Define: function () {
                var swh_type_select = $('#swh-fl-type');
                swh_type_select.select2({
                    placeholder: swh_type_select.attr('data-place'),
                    allowClear: true
                });
                $('#swh-type').select2({
                    placeholder: $('#swh-type').attr('data-place'),
                    allowClear: true
                });
                $(document).on('click', '.swh-alert-close', function (e) {
                    e.preventDefault();
                    $(this).parents('.swh-alert').remove();
                });
            },
            dateRange: function (_edit) {
                var _now = new Date();
                var dateFormat = "mm/dd/yy";

                /**
                 * Single Room filter price order
                 */
                var date_from_r = $("#swh-room-check-in"),
                    date_to_r = $("#swh-room-check-out");
                var dateValFormat = "yy-mm-dd",
                    from_r = date_from_r
                        .datepicker({
                            changeMonth: true,
                            numberOfMonths: 1,
                            dateFormat: dateFormat,
                            minDate: new Date(),
                            onSelect: function () {
                                var date = getDate(this);
                                var date_val = getDate(this);
                                if (date) {
                                    date.setDate(date.getDate() + 1);
                                }
                                to_r.datepicker("option", "minDate", date);
                                _swh_search.handles.updateLayoutForm($(this), _edit);
                                setTimeout(function () {
                                    to_r.datepicker("show");
                                }, 300);
                            }
                        }),
                    to_r = date_to_r.datepicker({
                        changeMonth: true,
                        numberOfMonths: 1,
                        dateFormat: dateFormat,
                        minDate: (_now.getDate + 1),
                        onSelect: function () {
                            var date = getDate(this);
                            var date_val = getDate(this);
                            if (date) {
                                date.setDate(date.getDate() - 1);
                            }
                            _swh_search.handles.updateLayoutForm($(this), _edit);
                            from_r.datepicker("option", "maxDate", date);
                        }
                    });

                /**
                 * Date range search
                 */
                $(document).on('click', '.date-checkin-layout', function (e) {
                    e.preventDefault();
                    $(this).parent().find('input[name=check-in]').trigger('focus');
                });
                $(document).on('click', '.date-checkout-layout', function (e) {
                    e.preventDefault();
                    $(this).parent().find('input[name=check-out]').trigger('focus');
                });
                $("#swh-book-date").datepicker({
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat: dateFormat,
                    minDate: new Date()
                });
                var date_froms = $(".swh-ft-check-in"),
                    date_tos = $(".swh-ft-check-out");
                date_froms.each(function () {
                    var date_from = $(this);
                    var date_to = $(this).parents('form').find('.swh-ft-check-out');
                    var from = date_from
                            .datepicker({
                                changeMonth: true,
                                numberOfMonths: 1,
                                dateFormat: dateFormat,
                                minDate: new Date(),
                                onSelect: function () {
                                    var date = getDate(this);
                                    var date_val = getDate(this);
                                    if (date) {
                                        date.setDate(date.getDate() + 1);
                                    }
                                    $(this).parent().find('.date').html(date_val.getDate());
                                    $(this).parent().find('.month').html(_swh_search.handles.swhGetMonth(date_val));
                                    to.datepicker("option", "minDate", date);
                                    setTimeout(function () {
                                        to.datepicker("show");
                                    }, 300);
                                }
                            }),
                        to = date_to.datepicker({
                            changeMonth: true,
                            numberOfMonths: 1,
                            dateFormat: dateFormat,
                            minDate: (_now.getDate + 1),
                            onSelect: function () {
                                var date = getDate(this);
                                var date_val = getDate(this);
                                if (date) {
                                    date.setDate(date.getDate() - 1);
                                }

                                $(this).parent().find('.date').html(date_val.getDate());
                                $(this).parent().find('.month').html(_swh_search.handles.swhGetMonth(date_val));
                                from.datepicker("option", "maxDate", date);
                            }
                        });
                });
                function getDate(element) {
                    var date;
                    try {
                        date = $.datepicker.parseDate(dateFormat, element.value);
                    } catch (error) {
                        date = null;
                    }

                    return date;
                }


                function convertDate(date_old) {
                    var month = date_old.getMonth() + 1,
                        day = date_old.getDate();
                    month = month.toString().length === 1 ? '0' + month : month;
                    day = day.toString().length === 1 ? '0' + day : day;

                    var date = date_old.getFullYear() + '-' + (month) + '-' + day;
                    return date;
                }


            },
            swhGetMonth: function (date) {
                var _mths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                return _mths[date.getMonth()];
            },
            getDayOfWeek: function (date) {
                var _w = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                return _w[date.getDay()];
            },
            updateLayoutForm: function (_this, _edit) {
                var _parents = _this.parents('.swh-checking-form');
                if (_edit) {
                    _parents = _this.parents('.swh-checking-form2');
                    $.ajax({
                        url: search_obj.ajax_url,
                        type: 'POST',
                        beforeSend: function () {
                            _edit_btn_sm = 2;
                            $(document).find('.swh-btn-update').css('opacity', '0.4');
                            $(document).find('.swh-btn-update').css('cursor', 'context-menu');
                        },
                        data: {
                            action: 'swh_refresh_order_price',
                            _o_id: _parents.attr('data-oid'),
                            _st_date: $("#swh-room-check-in").val(),
                            _en_date: $("#swh-room-check-out").val(),
                            _guest: $('#check-number-guests').val()
                        }
                    })
                        .done(function (data) {
                            if (data.stt === 'error') {
                                _edit_btn_sm = 0;
                                $(document).find('.swh-btn-update').css('opacity', '0.4');
                                $(document).find('.swh-btn-update').css('cursor', 'context-menu');
                                var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                $('body').prepend('<div class="' + x_cl + '">' + data.msg + '</div>');
                                setTimeout(function () {
                                    $('.' + x_cl).remove();
                                }, 5000);
                            }
                            if (data.stt === 'success') {
                                _edit_btn_sm = 1;
                                $(document).find('.swh-btn-update').css('opacity', '1');
                                $(document).find('.swh-btn-update').css('cursor', 'pointer');
                                extra_service = data.ex;
                                order_price = data.order_price;
                                var _rs = _swh_search.handles.calcOrderPrice();
                                $(document).find('.swh-value').html(_rs);
                            }
                        })
                        .fail(function () {
                            return false;
                        })
                        .always(function () {
                            return false;
                        });
                }
                var _from_val = new Date(_parents.find('#swh-room-check-in').val()),
                    _to_val = new Date(_parents.find('#swh-room-check-out').val());
                _parents.find('.swh-check-in').find('.date-label').html(_from_val.getDate());
                _parents.find('.swh-check-in').find('.detail-date-label').html(_swh_search.handles.swhGetMonth(_from_val) + ', ' + _from_val.getFullYear() + ' - ' + _swh_search.handles.getDayOfWeek(_from_val));

                _parents.find('.swh-check-out').find('.date-label').html(_to_val.getDate());
                _parents.find('.swh-check-out').find('.detail-date-label').html(_swh_search.handles.swhGetMonth(_to_val) + ', ' + _to_val.getFullYear() + ' - ' + _swh_search.handles.getDayOfWeek(_to_val));

                var days = Math.floor(_to_val.getTime() - _from_val.getTime()) / (1000 * 60 * 60 * 24);
                days = parseInt(days) >= 1 || days === "" ? days : 0;
                $(document).find('.check-night-layout span').html(days);
            },

            checkDateAvailable: function (from_date, to_date) {
                form_data.st_date = from_date;
                form_data.en_date = to_date;
                $('.acm-book-date').removeAttr('style');
                $('.acm-book-checkin').removeAttr('style');
                $('.acm-book-checkout').removeAttr('style');
                $('.acm-notification').remove();
                $.ajax({
                    url: booking_form_data.ajax_url,
                    type: 'POST',
                    beforeSend: function () {
                        $('.acm-loading').css('display', 'block');
                    },
                    data: {
                        action: 'check_available_date_booking',
                        acm_id: booking_form_data.acm_id,
                        date_start: from_date,
                        date_end: to_date
                    }
                })
                    .done(function (data) {
                        if (data.status === 'error') {
                            $('.acm-order-submit').after('<p class="acm-notification">' + data.message + '</p>');
                            var style_error = 'border-color:red';
                            $('.acm-book-date').attr('style', style_error);
                            $('.acm-book-checkin').attr('style', style_error);
                            $('.acm-book-checkout').attr('style', style_error);
                            _check = false;
                        } else {
                            _check = true;
                        }
                    })
                    .fail(function () {
                        return false;
                    })
                    .always(function () {
                        $('.acm-loading').css('display', 'none');
                        return false;
                    });
            },
            submitBookingHanle: function () {
                var _check_b = true;
                form_data = {};
                $(document).on('click', '.swh-book', function (e) {
                    e.preventDefault();
                    if (typeof swh_single === 'undefined' || !_check_b) {
                        return;
                    }
                    $('.swh-alert').remove();
                    _check_b = false;

                    form_data.st_date = $('#swh-room-check-in').val();
                    form_data.en_date = $('#swh-room-check-out').val();
                    form_data.guest = $('#check-number-guests').val();

                    $.ajax({
                        url: search_obj.ajax_url,
                        type: 'POST',
                        beforeSend: function () {
                        },
                        data: {
                            action: 'swh_order_handles',
                            swh_id: swh_single.id,
                            form_data: form_data
                        }
                    })
                        .done(function (data) {
                            if (data.status === 'error') {
                                var x_cl = 'swh-' + (Math.floor((Math.random() * 1000) + 1) + Math.floor((Math.random() * 1000) + 1));
                                $('body').prepend('<div class="' + x_cl + '">' + data.message + '</div>');
                                setTimeout(function () {
                                    $('.' + x_cl).remove();
                                }, 5000);
                            } else {
                                if (data.status === 'done' && data.url !== false) {
                                    window.location.href = data.url;
                                } else {
                                    $('.swh-book').after('<p class="swh-notification">' + data.message + '</p>');
                                }
                            }
                        })
                        .fail(function () {
                            return false;
                        })
                        .always(function () {
                            _check_b = true;
                            return false;
                        });
                });
            },
            calcOrderPrice: function () {
                if (typeof extra_service === 'undefined' || typeof order_price === 'undefined') {
                    return;
                }
                var _ex_s = 0;
                var _sv_l = [];
                $(document).find('input.swh-ex-sv:checked').each(function () {
                    var _this_s = $(this);
                    _sv_l.push(parseInt(_this_s.val()));
                    _ex_s += parseInt(extra_service[parseInt(_this_s.val())]);
                });
                if (!_edit_order) {
                    _swh_search.handles.saveCookieExtraServices(_sv_l.join());
                }
                var or_p = _ex_s + parseInt(order_price['price']);
                var or_p_with_vat = (or_p + (or_p * parseInt(order_price['vat']) / 100)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                document.getElementById("orderPrice").value = or_p_with_vat;
                var _rs_p = '',
                    _symbol = order_price['symbol'];
                switch (order_price['position']) {
                    case 'right':
                        _rs_p = or_p_with_vat + _symbol;
                        break;
                    case 'right_space':
                        _rs_p = or_p_with_vat + ' ' + _symbol;
                        break;
                    case 'left_space':
                        _rs_p = _symbol + ' ' + or_p_with_vat;
                        break;
                    default:
                        _rs_p = _symbol + or_p_with_vat;
                        break;
                }
                if (_edit_btn_sm === 2) {
                    _edit_btn_sm = 1;
                    $(document).find('.swh-btn-update').css('opacity', '1');
                    $(document).find('.swh-btn-update').css('cursor', 'pointer');
                }
                return _rs_p;
            },
            saveCookieExtraServices: function (list) {
                $.ajax({
                    url: search_obj.ajax_url,
                    type: 'POST',
                    beforeSend: function () {
                    },
                    data: {
                        action: 'swh_change_service_cookie',
                        sv: list
                    }
                })
                    .done(function (data) {
                    })
                    .fail(function () {
                        return false;
                    })
                    .always(function () {
                        return false;
                    });
            },
            saveActiveTabCheckout: function (event) {
                $.ajax({
                    url: search_obj.ajax_url,
                    type: 'POST',
                    beforeSend: function () {
                    },
                    data: {
                        action: 'swh_change_active_tab_cookie',
                        swh_tab_act: event
                    }
                })
                    .done(function (data) {
                    })
                    .fail(function () {
                        return false;
                    })
                    .always(function () {
                        return false;
                    });
            }
        }
    };
    _swh_search.init();
})(jQuery);
