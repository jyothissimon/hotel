/**
 * @since: 1.0.0
 * @author: KP
 */
(function ($) {
    var booking_handle = {
        init: function () {
            booking_handle.eventsDefine.reviewSWH();
        },
        eventsDefine: {
            reviewSWH: function () {
                $('.acm-rating-value').attr('data-hover', $('.swh-rating-hover').html());
                $('.swh-star').hover(function () {
                        var _this = $(this);
                        var _index = $(this).index();
                        $('.swh-rating-hover').html(review_hover[_index]);
                        for (var i = 1; i <= 5; i++) {
                            if (i <= _index) {
                                var _star = _this.parent().find('span').eq(i);
                                _star.removeClass("zmdi-star-outline").addClass("zmdi-star");
                            } else {
                                var __star = _this.parent().find('span').eq(i);
                                __star.removeClass("zmdi-star").addClass("zmdi-star-outline");
                            }
                        }
                    },
                    function () {
                    });
                $('.swh-rating-select').hover(
                    function () {
                    },
                    function () {
                        $('.swh-rating-hover').html($('.swh-rating-value').attr('data-hover'));
                        var _this = $(this);
                        var _vote_val = $('.swh-rating-value').val();
                        if (_vote_val === '') {
                            _vote_val = 0;
                        }
                        for (var i = 0; i < 5; i++) {
                            if (i < _vote_val) {
                                var _star = _this.parent().find('span').eq(i);
                                _star.removeClass("zmdi-star-outline").addClass("zmdi-star");
                            } else {
                                var __star = _this.parent().find('span').eq(i);
                                __star.removeClass("zmdi-star").addClass("zmdi-star-outline");
                            }
                        }
                    }
                );
                $(document).on('click', '.swh-star', function (e) {
                    e.preventDefault();
                    var _vote_val = $(this).index();
                    $('.swh-rating-value').attr('data-hover', review_hover[_vote_val]);
                    $('.swh-rating-hover').html(review_hover[_vote_val]);
                    $('.swh-rating-value').val(parseInt(_vote_val) + 1);
                });
                $(document).on('click', '.swh-rv-submit', function (e) {
                    e.preventDefault();
                    var _check = true,
                        _this = $(this),
                        _rv_title = $('#swh-rate-title'),
                        _rv_content = $('#swh-rate-content'),
                        _rv_name = $('#swh-rate-name'),
                        _rv_email = $('#swh-rate-email');
                    _rv_title.removeClass('swh-error');
                    _rv_content.removeClass('swh-error');
                    _rv_name.removeClass('swh-error');
                    _rv_email.removeClass('swh-error');
                    if (_rv_title.val().length === 0) {
                        _rv_title.addClass('swh-error');
                        _check = false;
                    }
                    if (_rv_content.val().length === 0) {
                        _rv_content.addClass('swh-error');
                        _check = false;
                    }
                    if (_rv_name.length === 1 && _rv_name.val().length === 0) {
                        _rv_name.addClass('swh-error');
                        _check = false;
                    }
                    if (_rv_email.length === 1 && _rv_email.val().length === 0) {
                        _rv_email.addClass('swh-error');
                        _check = false;
                    }
                    if (_check) {
                        _this.parents('form').submit();
                    }
                });
                $(document).on('click', '.swh-alert-close', function (e) {
                    e.preventDefault();
                    $(this).parents('.swh-alert').remove();
                });
                setTimeout(function () {
                    if(!$(document).find('.swh-alert').hasClass('secret')){
                        $('.swh-alert').remove();
                    }
                }, 5000);
            }
        },
        handles: {
        }
    };
    booking_handle.init();
})(jQuery);
