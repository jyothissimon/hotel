<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="#">
    <title>Reservation &#8211; Nanovi</title>
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="Nanovi &raquo; Feed" href="#" />
    <link rel="alternate" type="application/rss+xml" title="Nanovi &raquo; Comments Feed" href="#" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/demo.themenovo.com\/nanovi\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
      img.wp-smiley,
      img.emoji {
      	display: inline !important;
      	border: none !important;
      	box-shadow: none !important;
      	height: 1em !important;
      	width: 1em !important;
      	margin: 0 .07em !important;
      	vertical-align: -0.1em !important;
      	background: none !important;
      	padding: 0 !important;
      }
    </style>
    <link rel='stylesheet' id='wp-block-library-css'  href='<?php echo base_url(); ?>assets/css/style.min.css'  media='all' />
    <link rel='stylesheet' id='cms-plugin-stylesheet-css'  href='<?php echo base_url(); ?>assets/css/cms-style.css'  media='all' />
    <link property="stylesheet" rel='stylesheet' id='owl-carousel-css'  href='<?php echo base_url(); ?>assets/css/owl.carousel.min.css'  media='all' />
    <link rel='stylesheet' id='contact-form-7-css'  href='<?php echo base_url(); ?>assets/css/styles.css'  media='all' />
    <link rel='stylesheet' id='widget_style-css'  href='<?php echo base_url(); ?>assets/css/default.css'  media='all' />
    <link rel='stylesheet' id='font-awesome-css'  href='<?php echo base_url(); ?>assets/css/font-awesome.min.css'  media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css'  href='<?php echo base_url(); ?>assets/css/settings.css'  media='all' />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
      #rs-demo-id {}
    </style>
    <link rel='stylesheet' id='swh-select-2-css'  href='<?php echo base_url(); ?>assets/css/select2.min.css'  media='all' />
    <link rel='stylesheet' id='swh-jquery-ui-date-range-css-css'  href='<?php echo base_url(); ?>assets/css/jquery-ui.min.css'  media='all' />
    <link rel='stylesheet' id='swh-front-css-css'  href='<?php echo base_url(); ?>assets/css/swh-front.css'  media='all' />
    <link rel='stylesheet' id='bootstrap-css'  href='<?php echo base_url(); ?>assets/css/bootstrap.min.css'  media='all' />
    <link rel='stylesheet' id='font-material-icon-css'  href='<?php echo base_url(); ?>assets/css/material-design-iconic-font.min.css'  media='all' />
    <link rel='stylesheet' id='font-etline-icon-css'  href='<?php echo base_url(); ?>assets/css/et-line.css'  media='all' />
    <link rel='stylesheet' id='magnific-popup-css'  href='<?php echo base_url(); ?>assets/css/magnific-popup.css'  media='all' />
    <link rel='stylesheet' id='nanovi-theme-css'  href='<?php echo base_url(); ?>assets/css/theme.css'  media='all' />
    <style id='nanovi-theme-inline-css' type='text/css'>
      #site-header-wrap .site-branding a img{max-height:38px}#site-header-wrap .site-branding a.logo-mobile img{max-height:45px}.site-footer::before{background-color:rgba(197,164,109,0.01)}
    </style>
    <link rel='stylesheet' id='nanovi-menu-css'  href='<?php echo base_url(); ?>assets/css/menu.css'  media='all' />
    <link rel='stylesheet' id='nanovi-style-css'  href='<?php echo base_url(); ?>assets/css/style.css'  media='all' />
    <link rel='stylesheet' id='nanovi-google-fonts-css'  href='https://fonts.googleapis.com/css?family=Cormorant%3A300%2C400%2C500%2C600%2C700%7CMontserrat%3A300%2C400%2C500%2C600%2C700%2C800%7CLora%3A400%2C400i%2C700%2C700i&#038;subset=latin%2Clatin-ext&#038;ver=5.0.3'  media='all' />
    <link rel='stylesheet' id='newsletter-subscription-css'  href='<?php echo base_url(); ?>assets/css/style.css'  media='all' />
    <script  src='<?php echo base_url(); ?>assets/js/jquery.js'></script>
    <script  src='<?php echo base_url(); ?>assets/js/jquery-migrate.min.js'></script>
    <script  src='<?php echo base_url(); ?>assets/js/jquery.validate.js'></script>
    <script type='text/javascript'>
      /* <![CDATA[ */
        var jsPassData = {"ajax_url":"https:\/\/demo.themenovo.com\/nanovi\/wp-admin\/admin-ajax.php","display_labels":"Labels","type_modal":"Popup","get_login_redirect":"Current Page","login_redirect":"","register_redirect":"","generated_pass":"","login_success":"Login Successfull!","login_error":"Wrong Username or Password!"};
        /* ]]> */
    </script>
    <script  src='<?php echo base_url(); ?>assets/js/widget-script.js'></script>
    <script  src='<?php echo base_url(); ?>assets/js/jquery.themepunch.tools.min.js'></script>
    <script  src='<?php echo base_url(); ?>assets/js/jquery.themepunch.revolution.min.js'></script>
    <script  src='<?php echo base_url(); ?>assets/js/jquery-ui.min.js'></script>
    <link rel='#' href='#' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="#" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://demo.themenovo.com/nanovi/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 5.0.3" />
    <link rel="canonical" href="<?php echo base_url(); ?>reservation/" />
    <link rel='shortlink' href='#' />
    <link rel="alternate" type="application/json+oembed" href="https://demo.themenovo.com/nanovi/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdemo.themenovo.com%2Fnanovi%2Freservation%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://demo.themenovo.com/nanovi/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdemo.themenovo.com%2Fnanovi%2Freservation%2F&#038;format=xml" />
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
    <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://demo.themenovo.com/nanovi/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.7.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
    <link rel="icon" href="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/cropped-thumbnails-300x300.jpg" sizes="32x32" />
    <link rel="icon" href="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/cropped-thumbnails-300x300.jpg" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/cropped-thumbnails-300x300.jpg" />
    <meta name="msapplication-TileImage" content="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/cropped-thumbnails-300x300.jpg" />
    <script >function setREVStartSize(e){
						try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
							if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
						}catch(d){console.log("Failure at Presize of Slider:"+d)}
					};
    </script>
    <style type="text/css" title="dynamic-css" class="options-output">a{color:#222;}a:hover{color:#c5a46d;}.site-footer .bottom-footer a{color:#c5a46d;}</style><style id="cms-page-dynamic-css" data-type="redux-output-css">#pagetitle{background-image:url('https://demo.themenovo.com/nanovi/wp-content/uploads/2018/11/bg-page-title-reservation.jpg');}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>

<body class="page-template-default page page-id-26 header-1 visual-composer wpb-js-composer js-comp-ver-5.6 vc_responsive">
<div id="page" class="site">
        <header id="masthead" class="site-header">
          <div id="site-header-wrap" class="header-layout1 rf-active is-sticky">
              <!--<div class="topbar-panel-mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <ul class="top-information">
                                <li class="li-phone">
                                        <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i>1800 - 1111 - 2222                                        </a>
                                    </li>
                                      <li class="li-adderess">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        269 King - Melbourne Australia.</li>


                                              <li class="li-sign-up"><span class="menu-right-item btn-sign-up">
                                                <i class="login-icon fa fa-power-off"></i>
                                                            <a class="go_to_login_link" href="https://demo.themenovo.com/nanovi/wp-login.php" >Login / Register</a>
                                                        </span>
                                                    </li>
                                          </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="topbar-panel">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7">
                            <ul class="top-information pull-left">
                                                                    <li class="li-phone">
                                        <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i>1800 - 1111 - 2222                                        </a>
                                    </li><li class="li-adderess"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                        269 King - Melbourne Australia.</li>
                            </ul>
                        </div>
                        <div class="col-lg-5">
                            <ul class="top-information pull-right">
                              <li class="li-sign-up">
                                <span class="menu-right-item btn-sign-up">
                                  <i class="login-icon fa fa-power-off"></i>
                                      <a class="go_to_login_link" href="https://demo.themenovo.com/nanovi/wp-login.php" >Login / Register</a>
                                          </span>
                                            </li>
                          </ul>
                        </div>
                    </div>
                </div>
            </div>-->
              <div id="headroom" class="site-header-main">
                  <div class="container">
                      <div class="header-medium">
                          <div class="site-branding">
                              <a class="logo-dark" href="<?php echo site_url('home'); ?>" title="Tripenta" rel="home">
                                <img src="<?php echo base_url(); ?>assets/image/logo_inner_page.png" alt="Logo"/></a><a class="logo-light" href="https://demo.themenovo.com/nanovi/" title="Tripenta" rel="home">
                                  <img src="<?php echo base_url(); ?>assets/image/logo_inner_page.png" alt="Logo Light"/></a>
                              <a class="logo-mobile" href="<?php echo site_url('home'); ?>" title="Nanovi" rel="home">
                                <img src="<?php echo base_url(); ?>assets/image/logo_inner_page.png" alt="Logo Mobile"/></a>
                              </div>
                                  <nav id="site-navigation" class="main-navigation">
                                      <ul id="mastmenu" class="primary-menu">
                                        <li id="menu-item-20" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-20"><a href="#" class="no-one-page">Home</a>
                                          <!--<ul class="sub-menu">
                                            	<li id="menu-item-48" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-48"><a href="https://demo.themenovo.com/nanovi/" class="no-one-page">Home 1</a></li>
                                            	<li id="menu-item-47" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-47"><a href="https://demo.themenovo.com/nanovi/home-2/" class="no-one-page">Home 2</a></li>
                                            	<li id="menu-item-2430" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2430"><a href="https://demo.themenovo.com/nanovi/home-3/" class="no-one-page">Home 3</a></li>
                                            </ul>
                                        </li>-->
                                        <li id="menu-item-1019" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1019"><a href="https://demo.themenovo.com/nanovi/contact-us/" class="no-one-page">Contact</a></li>
                                      </ul>
                                    </nav>
                              <div class="site-menu-right d-none d-lg-block">
                            <span class="menu-right-item h-btn-search"><i class="fa fa-search"></i></span>
                      </div>
                </div>
            </div>
            <div id="main-menu-mobile">
                  <div id="search-mobile">
                      <span class="h-btn-search"><i class="fa fa-search"></i></span>
                  </div>
                  <div class="btn-login">
                      <span class="menu-right-item btn-sign-up"><i class="login-icon fa fa-power-off"></i>             <a class="go_to_login_link" href="https://demo.themenovo.com/nanovi/wp-login.php" >Login / Register</a>
                      </span>
                  </div>
                    <span class="btn-nav-mobile open-menu">
                    <span></span>
                </span>
            </div>
        </div>
    </div>
</header>    <div id="pagetitle" class="page-title">
    <div class="bg-overlay">
        <div class="container page-title-container">
            <div class="row">
                <div class="col-12 col-title-text">
                    <div class="page-title-content clearfix">
                        <h1 class="page-title">Reservation</h1><ul class="cms-breadcrumb"><li><a class="breadcrumb-entry" href="#">Home</a></li><li><span class="breadcrumb-entry">Reservation</span></li></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="content" class="site-content">
    <div class="container content-container">
        <div class="row content-row">
            <div id="primary"  class="content-area content-full-width col-12">
                <main id="main" class="site-main">

<article id="post-26" class="post-26 page type-page status-publish hentry">
    <div class="entry-content clearfix">
        <div class="cms-bootstrap-tabs-checkout">
            <div class="swh-order-loading" style="display: none">
                <span class="fa fa-spinner fa-spin"></span>
            </div>

            <ul class="tab-reservation-room nav nav-tabs" role="tablist">
                <li role="ROOM &amp; RATE"  class="active">
                    <a href="#tab-rome-rate" aria-controls="tab-rome-rate" role="tab" data-toggle="tab">
                      1. ROOM &amp; RATE
                    </a>
                </li>
                <li role="RESERVATION" class="">
                  <a href="#tab-rome-reservation" aria-controls="tab-rome-reservation" role="tab" data-toggle="tab" aria-expanded="true">
                        2. RESERVATION
                  </a>
                </li>
            </ul><!-- Nav tabs -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab-rome-rate">
                    <div class="cms-col-left">
                        <div class="inner-col inner-col-left">
                            <div class="cms-room-image">
                                <img src="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/08/room8.jpg"
                                     alt="Room8">
                                <h3 class="cms-room-title">
                                    <a href="<?php echo site_url('roomDetails/getdetails'); ?>/<?= $details->ixRoomCategory  ?>"><?= $details->categoryName ?></a>
                                </h3>
                            </div>
                            <div class="swh-checking-form2">
                                <div class="swh-checking-item swh-check-in">
                                    <div class="swh-wg-field">
                                        <span class="head-label">Check - In</span>
                                        <span class="date-label"><?= date('d', strtotime($sessionData['resCheckin'])); ?></span>
                                        <span class="detail-date-label"><?= date('M , Y - l', strtotime($sessionData['resCheckin'])); ?></span>
                                    </div>
                                </div>
                                <div class="swh-checking-item swh-check-out">
                                    <div class="swh-wg-field">
                                        <span class="head-label">Check - Out</span>
                                        <span class="date-label"><?= date('d', strtotime($sessionData['resCheckout'])); ?></span>
                                        <span class="detail-date-label"><?= date('M , Y - l', strtotime($sessionData['resCheckout'])); ?></span>
                                    </div>
                                </div>
                                <div class="swh-checking-item swh-guests-nights">
                                    <div class="swh-wg-field">
                                        <span class="head-label">Guests</span>
                                        <div class="check-guest-layout">
                                            <span><?= $sessionData['guests'] ?></span>
                                        </div>
                                        <input type="hidden" name="check-number-guests" id="check-number-guests" value="<?= $sessionData['guests'] ?>">
                                    </div>
                                    <div class="swh-wg-field">
                                    <span class="head-label"
                                          data-nights="Nights">Night</span>
                                        <div class="check-night-layout">
                                            <span><?= $sessionData['nights'] ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cms-col-right">
                        <div class="inner-col inner-col-right">
                            <div class="swh-row-total">
                                <div class="swh-left">
                                    <span class="swh-value">&#x20b9; <?= $todayPrice ?> </span>
                                    <span class="swh-suffixes">/ Total</span>
                                    <input type="hidden" name="orderPrice" id="orderPrice" value="">
                                </div>
                                <div class="swh-right">
                                <span><label>INCLUDED :</label> 10% VAT Already Applied </span>
                                </div>
                            </div>

                            <div class="swh-room-meta">
                                <ul>
                                    <!--<li class="cms-room-meta"
                                        style="background-image: url(https://demo.themenovo.com/nanovi/wp-content/themes/nanovi/assets/images/room-meta/icon-single-room-1.png);">
                                        <span>Acreage: 20 Ft&sup2;</span>
                                    </li>-->
                                    <li class="cms-room-meta"
                                        style="background-image: url(https://demo.themenovo.com/nanovi/wp-content/themes/nanovi/assets/images/room-meta/icon-single-room-2.png);">
                                        <span>Guests: <?= $details->guestNumber ?></span>
                                    </li>
                                    <li class="cms-room-meta"
                                        style="background-image: url(https://demo.themenovo.com/nanovi/wp-content/themes/nanovi/assets/images/room-meta/icon-single-room-3.png);">
                                        <span>Bed: <?= $details->bedNumber ?></span>
                                    </li>
                                </ul>
                            </div>

                            <div class="swh-row-extra-service">
                                <div class="swh-extra-service-label">
                                    <label class="lb-title">Add Extra Services :</label>
                                    <label class="lb-radio"><input type="radio" name="swh_select_extra_sv" value="select_all" id="select_all">Select all</label>
                                    <label class="lb-radio"><input type="radio" name="swh_select_extra_sv" value="deselect_all" id="deselect_all">Deselect all</label>
                                </div>
                              <?php if($extraServices){ ?>
                                <ul class="swh-extra-service-list">
                                  <?php foreach($extraServices as $dt){ ?>
                                        <li>
                                            <input type="checkbox" class="swh-ex-sv" id="<?= $dt->ixCategoryExtraServices ?>" name="<?= $dt->ixCategoryExtraServices ?>" value="<?= $dt->ixCategoryExtraServices ?>">
                                            <label for="<?= $dt->ixCategoryExtraServices ?>">
                                              <?= $dt->serviceName ?>
                                            </label> &#x20b9; <?= $details->guestNumber ?> Guest / Night= &#x20b9; <?= $dt->servicePrice ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                              <?php } ?>
                            </div>
                            <div class="swh-row-btn-action">
                                <a class="swh-btn btn-1 swh-btn-decline" href="#">decline</a>
                                <a class="swh-btn btn-2 swh-btn-reser" href="#">reservation</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane"
                     id="tab-rome-reservation">
                    <div class="swh-form-title">Your Informations :
                      <a class="go_to_login_link" href="https://demo.themenovo.com/nanovi/wp-login.php" >Login / Register</a>
                    </div>
                    <form class="swh-form-check-count" action="#" method="post">
                      <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                          <div class="swh-filed">
                            <label>Name *</label>
                            <input type="text" name="your-name" >
                          </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                          <div class="swh-filed">
                            <label>Surname *</label>
                            <input type="text" name="your-surname" >
                          </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                          <div class="swh-filed">
                            <label>Email *</label>
                            <input type="email" name="your-email" >
                          </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                            <div class="swh-filed">
                                <label>Telephone *</label>
                                <input type="text" name="text-phone" >
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                            <div class="swh-filed">
                                <label>Address *</label>
                                <input type="text" name="your-address" >
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                            <div class="swh-filed">
                                <label>ZIP *</label>
                                <input type="text" name="text-zip" >
                            </div>
                        </div>
                      </div>
                      <div class="row">
                          <div class="col-12">
                              <div class="swh-filed swh-field-textarea">
                                  <label>Requests</label>
                                  <textarea name="your-message" cols="40" rows="10"></textarea>
                              </div>
                          </div>
                      </div>
                    </form>
                    <div class="swh-payments">
                        <div class="row">
                            <div class="col-xl-6 cl-lg-6 cl-md-6 col-sm-12 col-payment-left">
                                <div class="swh-accept-terms">
                                    <input type="checkbox" id="swh-terms" name="swh-terms" value="accept-terms">
                                    <span class="checkmark"></span>
                                    <label for="swh-terms">I have read and accepted the <a
                                                href="#">Terms</a>, as well as read and understand our terms of bussiness conditions                </label>
                                </div>
                            </div>
                            <div class="col-xl-6 cl-lg-6 cl-md-6 col-sm-12 col-payment-right">
                                <div class="swh-payment-methods">
                                    <h3 class="swh-payment-methods-title">Payment Method</h3>
                                    <div class="swh-pm-method">
                                                                <div class="swh-pm-method-item swh-paypal">
                                                <div class="swh-field-radio">
                                                    <input type="radio" name="swh-payment-method" id="swh-payment-method-paypal"
                                                           value="paypal" >
                                                    <span class="checkmark"></span>
                                                    <label for="swh-payment-method-paypal">Paypal</label>
                                                </div>
                                                                            <div class="swh-notice">
                                                    Pay via PayPal; you can pay with your credit card if you don’t have a PayPal
                                                    account.
                                                </div>
                                            </div>
                                                                <div class="swh-pm-method-item swh-offline active">
                                                <div class="swh-field-radio">
                                                    <input type="radio" name="swh-payment-method" id="swh-payment-method-offline"
                                                           value="offline" checked>
                                                    <span class="checkmark"></span>
                                                    <label for="swh-payment-method-offline">Offline payment</label>
                                                </div>
                                                <div class="swh-notice">Pay on Arrival.</div>
                                            </div>
                                                                <div class="swh-pm-method-item swh-bank-transfer">
                                                <div class="swh-field-radio">
                                                    <input type="radio" name="swh-payment-method" id="swh-payment-method-direct"
                                                           value="bank">
                                                    <span class="checkmark"></span>
                                                    <label for="swh-payment-method-direct">Direct Bank Transfer</label>
                                                </div>
                                                <div class="swh-notice">
                                                    <p>Pay directly to our bank account. Please use your Order ID, Your Username, Your Email as a Payment Reference. Your order will be approved until the amount paid in our account and our will notify to you.</p>
                                                                                        <div class="swh-bank-account swh-bank-0">
                                                            <div class="swh-account-name">
                                                                <span>Account Name: </span>KP                                        </div>
                                                            <div class="swh-account-number">
                                                                <span>Account Number: </span>12209481928                                        </div>
                                                            <div class="swh-bank-name">
                                                                <span>Bank Name: </span>VietComBank                                        </div>
                                                            <div class="swh-bank-bic">
                                                                <span>BIC / Swift: </span>12312938                                        </div>
                                                        </div>
                                                                                    </div>
                                            </div>
                                                        </div>
                                </div>
                                <div class="swh-row-btn-action">
                                    <a class="swh-btn swh-btn-done-order"
                                       href="#">Complete Your Order</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            </div><!-- .entry-content -->

    </article><!-- #post-26 -->
                </main><!-- #main -->
            </div><!-- #primary -->


        </div>
    </div>

</div><!-- #content -->

<footer id="colophon" class="site-footer footer-layout1">
  <div class="top-footer">
  <div class="container">
      <div class="row row-widget-bottom">
          <div class="cms-footer-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">

            <div class="widget cms-widget-text clearfix">
                  <h2 class="widget-title"> CONTACT US</h2>
              <ul>
                <li class="cms-widget-infor wg-infor-title">Address : UL cyberpark, Kozhikode, 673016. </li>
                <li class="cms-widget-infor wg-infor-phone"> Email : Tripentahotel@gmail.com</li>
                <li class="cms-widget-infor wg-infor-email">Call phone us : 1800 - 1111 - 2222</li>
              </ul>
            </div>
          </div>
          <div class="cms-footer-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="top-footer-logo">
                <a href="<?php echo site_url('home'); ?>"><img src="<?php echo base_url(); ?>assets/image/logo.png" alt="Logo Light"/></a>
            </div>
            <div id="text-3" class="widget widget_text">
              <div class="textwidget">
                <p>We are proud to say that since our opening in ’98 we have been serving our visitors in the best possible way. live the “Tripenta experience”</p>
              </div>
            </div>
         </div>
         <div class="cms-footer-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
           <div id="newsletterwidget-2" class="widget widget_newsletterwidget">
             <h2 class="footer-widget-title">NEWSLETTER</h2>
             <div class="tnp tnp-widget">
               <form method="post" action="https://demo.themenovo.com/nanovi/?na=s" onsubmit="return newsletter_check(this)">
                  <input type="hidden" name="nr" value="widget">
                  <input type='hidden' name='nl[]' value='0'>
                  <div class="tnp-field tnp-field-email"><label>Email</label>
                    <input class="tnp-email" type="email" name="ne" required>
                  </div>
                  <div class="tnp-field tnp-field-button">
                    <input class="tnp-submit" type="submit" value="Subscribe">
                  </div>
                </form>
              </div>
            </div>
            <div id="cms_social_widget-2" class="widget widget_cms_social_widget">
              <ul class='cms-social'>
                <li><a target="_blank" href="#"><i class="fa fa-facebook"></i><span>Facebook</span></a></li>
                <li><a target="_blank" href="#"><i class="zmdi zmdi-rss"></i><span>Rss</span></a></li>
                <li><a target="_blank" href="#"><i class="fa fa-youtube"></i><span>YouTube</span></a></li>
                <li><a target="_blank" href="#"><i class="fa fa-twitter"></i><span>Twitter</span></a></li>
                <li><a target="_blank" href="#"><i class="fa fa-skype"></i><span>Skype</span></a></li></ul>
              </div>
            </div>
          </div>
        </div>
      </div>
  <div class="bottom-footer">
      <div class="container">
          <div class="copyright-content">
              © 2018 Tripenta Hotel made by <a href="<?php echo site_url('home'); ?>">Wr Solutions</a>            </div>
      </div>
  </div>
</footer><!-- #colophon -->
<!--<div class="swh-order-notification">
  <div class="swh-noti-overlay"></div>
  <div class="swh-order-noti-inner">
    <i class="swh-noti-close zmdi zmdi-close"></i>
    <h3 class="swh-noti-title">Order Successfully</h3>
    <div class="swh-noti-contents">
      <div>
        <p>OrderID: SWH-2733</p>
        <p>Name: SDCFV</p>
        <p>Email: SDSFVD@sadf.com</p>
        <p>Payment method: Offline payment</p>
        <p>Please remember your Order ID, Your Username, Your Email. You will pay on Arrival. We will contact soon</p></div>
      </div>
    </div>
  </div>-->
  <!-- Modal -->
<div class="cms-modal customModel" style="display:none">
  <p>model</p>
</div>
<div class="cms-modal cms-search-popup">
        <div class="cms-close"></div>
        <div class="cms-modal-content">
            <form role="search" method="get" class="cms-search-form placeholder-white" action="#">
                <i class="fa fa-search"></i>
                <input type="text" placeholder="Search..." name="s" class="search-field" />
            </form>
        </div>
</div>
    <a href="#" class="scroll-top"><i class="zmdi zmdi-long-arrow-up"></i></a>
</div><!-- #page -->
<input class="cshlg-inline-css" type="hidden" value=".cshlg-popup { background-color: rgba(0, 0, 0, 0.8); }">
<input class="cshlg-inline-css" type="hidden" value=""><div id="csh-login-wrap" class="cshlg-popup">

    <div class="login_dialog">

        <a class="boxclose"></a>

        <form class="login_form" id="login_form" method="post" action="#">
            <h2>Please Login</h2>
            <input type="text" class="alert_status" readonly>
                            <label for="login_user"> Username</label>

            <input type="text" name="login_user" id="login_user" />
                            <label for="pass_user"> Password </label>

            <input type="password" name="pass_user" id="pass_user" />
            <label for="rememberme" id="lb_rememberme"><input name="rememberme" type="checkbox" id="rememberme" value="forever"  /> Remember Me</label>
            <input type="submit" name="login_submit" value="LOGIN" class="login_submit" />

            <div class="social-login">
              <a class="login-btn-facebook" href="https://demo.themenovo.com/nanovi/wp-content/plugins/csh-login/inc/login-with-facebook"><i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a>
            </div>
        </form>

        <form class="register_form" id="register_form"
            action="https://demo.themenovo.com/nanovi" method="post">
            <h2>Registration</h2>
            <input type="text" class="alert_status" readonly>
            <label for="register_user">Username</label>
            <input type="text" name="register_user" id="register_user" value="" >
            <label for="register_email">E-mail</label>
            <input type="email" name="register_email" id="register_email" value="">
            <div id="allow_pass">
                <label for="register_pass">Password</label>
                <input type="password" name="register_pass" id="register_pass" value="">
                <label for="confirm_pass">Confirm Password</label>
                <input type="password" name="confirm_pass" id="confirm_pass" value="">
            </div>
                        <input type="submit" name="register_submit" id="register_submit" value="REGISTER" />
        </form>

        <form class="lost_pwd_form" action="https://demo.themenovo.com/nanovi" method="post">
            <h2>Forgotten Password?</h2>
            <input type="text" class="alert_status" readonly>
            <label for="lost_pwd_user_email">Username or Email Adress</label>
            <input type="text" name="lost_pwd_user_email" id="lost_pwd_user_email">
            <input type="submit" name="lost_pwd_submit" id="lost_pwd_submit" value="GET NEW PASSWORD">
        </form>
                <div class="pass_and_register" id="pass_and_register">

            <a class="go_to_register_link" href="" style="">Register</a>
            <span style="color: black"> </span>
            <a class="go_to_lostpassword_link" href="">Forgot Password</a>
            <span style="color: black"></span>
            <a class="back_login" href="">Back to Login</a>

        </div>


    </div>
</div>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <link rel='stylesheet' id='cshlg_layout_1-css'  href='<?php echo base_url(); ?>assets/css/layout1.css'  media='all' />
  <script  src='<?php echo base_url(); ?>assets/js/cms-front.js'></script>
  <script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"https:\/\/demo.themenovo.com\/nanovi\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
    /* ]]> */
  </script>
  <script  src='<?php echo base_url(); ?>assets/js/scripts.js'></script>
  <script  src='<?php echo base_url(); ?>assets/js/select2.min.js'></script>
  <script type='text/javascript'>
    /* <![CDATA[ */
    var swh_order_data = {"st_date":"01\/24\/2019","en_date":"01\/25\/2019","guest":"1","swh_id":"1205","ajax_url":"https:\/\/demo.themenovo.com\/nanovi\/wp-admin\/admin-ajax.php","terms_error":"    <div class=\"swh-alert swh-warning \">\n        <div class=\"swh-alert-contents\">Please accept our Terms and Conditions!<\/div>\n        <span class=\"swh-alert-close zmdi zmdi-close\"><\/span>\n    <\/div>\n    "};
    /* ]]> */
  </script>
  <script  src='<?php echo base_url(); ?>assets/js/swh-post.js'></script>
  <script type='text/javascript'>
    /* <![CDATA[ */
    var search_obj = {"is_swh_archive":"false","ajax_url":"<?= base_url() . 'index.php/reservation/booking' ?>","room_page":"<?php echo site_url('roomDetails/getdetails'); ?>/<?= $details->ixRoomCategory ?>","confirm_delete_order":"Are you sure to delete your Reservation?"};
    var extra_service_checkout = {
      <?php foreach($extraServices as $dt){ ?>
      "<?= $dt->ixCategoryExtraServices ?>":"<?= $dt->servicePrice ?>",
      <?php } ?>
     };
    var order_price_checkout = {"price":"<?= $todayPrice ?>","symbol":"&#x20b9;","position":"left_space","vat":"0"};
    /* ]]> */
  </script>
  <script  src='<?php echo base_url(); ?>assets/js/swh-search.js'></script>
  <script type='text/javascript'>
    /* <![CDATA[ */
    var nectarLove = {"ajaxurl":"https:\/\/demo.themenovo.com\/nanovi\/wp-admin\/admin-ajax.php","postID":"26","rooturl":"https:\/\/demo.themenovo.com\/nanovi\/"};
    /* ]]> */
  </script>
  <script  src='<?php echo base_url(); ?>assets/js/post_favorite.js'></script>
  <script  src='<?php echo base_url(); ?>assets/js/headroom.min.js'></script>
  <script  src='<?php echo base_url(); ?>assets/js/headroom.js'></script>
  <script  src='<?php echo base_url(); ?>assets/js/cms-parallax.js'></script>
  <script  src='<?php echo base_url(); ?>assets/js/bootstrap.min.js'></script>
  <script  src='<?php echo base_url(); ?>assets/js/match-height-min.js'></script>
  <script  src='<?php echo base_url(); ?>assets/js/sidebar-scroll-fixed.js'></script>
  <script  src='<?php echo base_url(); ?>assets/js/magnific-popup.min.js'></script>
  <script  src='<?php echo base_url(); ?>assets/js/nice-select.min.js'></script>
  <script  src='<?php echo base_url(); ?>assets/js/jquery-ui.js'></script>
  <script  src='<?php echo base_url(); ?>assets/js/main.js'></script>
  <script type='text/javascript'>
    /* <![CDATA[ */
    var newsletter = {"messages":{"email_error":"Email address is not correct","name_error":"Name is required","surname_error":"Last name is required","privacy_error":"You must accept the privacy policy"},"profile_max":"20"};
    /* ]]> */
  </script>
  <script  src='<?php echo base_url(); ?>assets/js/validate.js'></script>
  <script  src='<?php echo base_url(); ?>assets/js/wp-embed.min.js'></script>

</body>
</html>
