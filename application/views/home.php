<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="#">
    <title>Tripenta &#8211; Hotel</title>
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="Tripenta &raquo; Feed" href="#" />
    <link rel="alternate" type="application/rss+xml" title="Tripenta &raquo; Comments Feed" href="#" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/demo.themenovo.com\/nanovi\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
      img.wp-smiley,
      img.emoji {
      	display: inline !important;
      	border: none !important;
      	box-shadow: none !important;
      	height: 1em !important;
      	width: 1em !important;
      	margin: 0 .07em !important;
      	vertical-align: -0.1em !important;
      	background: none !important;
      	padding: 0 !important;
      }
    </style>
<link rel='stylesheet' id='wp-block-library-css'  href='<?php echo base_url(); ?>assets/css/style.min.css'  media='all' />
<link property="stylesheet" rel='stylesheet' id='owl-carousel-css'  href='<?php echo base_url(); ?>assets/css/owl.carousel.min.css'  media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='<?php echo base_url(); ?>assets/css/styles.css'  media='all' />
<link rel='stylesheet' id='widget_style-css'  href='<?php echo base_url(); ?>assets/css/default.css'  media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='<?php echo base_url(); ?>assets/css/font-awesome.min.css'  media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='<?php echo base_url(); ?>assets/css/settings.css'  media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
  #rs-demo-id {}
</style>
<link rel='stylesheet' id='swh-front-css-css'  href='<?php echo base_url(); ?>assets/css/swh-front.css'  media='all' />
<link rel='stylesheet' id='bootstrap-css'  href='<?php echo base_url(); ?>assets/css/bootstrap.min.css'  media='all' />
<link rel='stylesheet' id='font-material-icon-css'  href='<?php echo base_url(); ?>assets/css/material-design-iconic-font.min.css'  media='all' />
<link rel='stylesheet' id='font-etline-icon-css'  href='<?php echo base_url(); ?>assets/css/et-line.css'  media='all' />
<link rel='stylesheet' id='magnific-popup-css'  href='<?php echo base_url(); ?>assets/css/magnific-popup.css'  media='all' />
<link rel='stylesheet' id='nanovi-theme-css'  href='<?php echo base_url(); ?>assets/css/theme.css'  media='all' />
<style id='nanovi-theme-inline-css' type='text/css'>
#site-header-wrap .site-branding a img{max-height:38px}#site-header-wrap .site-branding a.logo-mobile img{max-height:45px}
</style>
<link rel='stylesheet' id='nanovi-menu-css'  href='<?php echo base_url(); ?>assets/css/menu.css'  media='all' />
<link rel='stylesheet' id='nanovi-style-css'  href='<?php echo base_url(); ?>assets/css/style.css'  media='all' />
<link rel='stylesheet' id='nanovi-google-fonts-css'  href='<?php echo base_url(); ?>assets/css/family.css'  media='all' />
<link rel='stylesheet' id='newsletter-subscription-css'  href='<?php echo base_url(); ?>assets/css/style1.css'  media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='<?php echo base_url(); ?>assets/css/js_composer.min.css'  media='all' />

<script  src='<?php echo base_url(); ?>assets/js/jquery.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/jquery-migrate.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/jquery.validate.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var jsPassData = {"ajax_url":"https:\/\/demo.themenovo.com\/nanovi\/wp-admin\/admin-ajax.php","display_labels":"Labels","type_modal":"Popup","get_login_redirect":"Current Page","login_redirect":"","register_redirect":"","generated_pass":"","login_success":"Login Successfull!","login_error":"Wrong Username or Password!"};
/* ]]> */
</script>
<script  src='<?php echo base_url(); ?>assets/js/widget-script.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/jquery.themepunch.tools.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/jquery.themepunch.revolution.min.js'></script>

<link rel='#' href='#' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="#" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="#" />
<meta name="generator" content="WordPress 5.0.3" />
<link rel="canonical" href="#" />
<link rel='shortlink' href='#' />
<link rel="alternate" type="application/json+oembed" href="#" />
<link rel="alternate" type="text/xml+oembed" href="#" />
<meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://demo.themenovo.com/nanovi/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.7.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<link rel="icon" href="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/cropped-thumbnails-300x300.jpg" sizes="32x32" />
<link rel="icon" href="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/cropped-thumbnails-300x300.jpg" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/cropped-thumbnails-300x300.jpg" />
<meta name="msapplication-TileImage" content="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/cropped-thumbnails-300x300.jpg" />
<script >function setREVStartSize(e){
						try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
							if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
						}catch(d){console.log("Failure at Presize of Slider:"+d)}
					};</script>
<style type="text/css" title="dynamic-css" class="options-output">a{color:#222;}a:hover{color:#c5a46d;}</style><style id="cms-page-dynamic-css" data-type="redux-output-css">#content{padding-top:0;padding-bottom:0;}</style><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1543066746381{background-color: #f7f7f7 !important;}.vc_custom_1542729353018{background-color: rgba(0,0,0,0.5) !important;*background-color: rgb(0,0,0) !important;}.vc_custom_1543076254495{padding-top: 0px !important;padding-bottom: 0px !important;}.vc_custom_1542729844355{background-color: rgba(0,0,0,0.8) !important;*background-color: rgb(0,0,0) !important;}.vc_custom_1544669235201{margin-top: -100px !important;}.vc_custom_1542945071787{padding-top: 0px !important;}.vc_custom_1544687133930{border-bottom-width: 0px !important;padding-top: 0px !important;padding-right: 100px !important;padding-left: 100px !important;background-color: #f5f5f5 !important;}.vc_custom_1544669374257{padding-top: 0px !important;padding-bottom: 0px !important;}.vc_custom_1542945632093{padding-top: 0px !important;}.vc_custom_1544669519639{background-image: url(https://demo.themenovo.com/nanovi/wp-content/uploads/2018/08/bg-tab-massar.jpg?id=518) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1533539698915{padding-right: 40px !important;padding-left: 40px !important;background-color: #222222 !important;}.vc_custom_1545021604508{background-image: url(https://demo.themenovo.com/nanovi/wp-content/uploads/2018/12/bg-restaurant-tab.jpg?id=2504) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1533537804591{padding-right: 40px !important;padding-left: 40px !important;background-color: #222222 !important;}.vc_custom_1543567562596{background-image: url(https://demo.themenovo.com/nanovi/wp-content/uploads/2018/09/Learn-to-play-golf-for-free.jpg?id=1388) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1533537904737{padding-right: 40px !important;padding-left: 40px !important;background-color: #222222 !important;}.vc_custom_1533537951223{background-image: url(https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/3.jpg?id=271) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1533541699251{padding-right: 40px !important;padding-left: 40px !important;background-color: #222222 !important;}.vc_custom_1544672760343{background-color: #222222 !important;}.vc_custom_1544672773377{background-image: url(https://demo.themenovo.com/nanovi/wp-content/uploads/2018/08/bg-section-right-testimonial.jpg?id=940) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
</head>

<body class="home page-template-default page page-id-40 header-2 visual-composer wpb-js-composer js-comp-ver-5.6 vc_responsive">
<div id="page" class="site">
            <div id="cms-loadding" class="loader2">
            <div class="loading-spin">
                <div class="spinner">
                    <div class="right-side"><div class="bar"></div></div>
                    <div class="left-side"><div class="bar"></div></div>
                </div>
                <div class="spinner color-2" style="">
                    <div class="right-side"><div class="bar"></div></div>
                    <div class="left-side"><div class="bar"></div></div>
                </div>
            </div>
        </div>
        <header id="masthead" class="site-header">
    <div id="site-header-wrap" class="header-layout2 header-transparent rf-active is-sticky">
                    <div class="topbar-panel-mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <ul class="top-information">
                                  <li class="li-phone">
                                        <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i>1800 - 1111 - 2222</a>
                                    </li>
                                    <li class="li-adderess">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        Kozhikode, Kerala.
                                    </li>
                                    <li class="li-sign-up">
                                        <span class="menu-right-item btn-sign-up"><i class="login-icon fa fa-power-off"></i>             <a class="go_to_login_link" href="https://demo.themenovo.com/nanovi/wp-login.php" >Login / Register</a></span>
                                    </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="topbar-panel">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7">
                            <ul class="top-information pull-left">
                                                                    <li class="li-phone">
                                        <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i>1800 - 1111 - 2222</a>
                                    </li>
                                                                                                    <li class="li-adderess">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        269 King - Melbourne Australia.                                    </li>
                                                            </ul>
                        </div>
                        <div class="col-lg-5">
                            <ul class="top-information pull-right">
                                                                                                    <li class="li-sign-up">
                                                                                    <span class="menu-right-item btn-sign-up"><i class="login-icon fa fa-power-off"></i>             <a class="go_to_login_link" href="https://demo.themenovo.com/nanovi/wp-login.php" >Login / Register</a>
            </span>
                                                                            </li>
                                                            </ul>
                        </div>
                    </div>
                </div>
            </div>-->
        <div id="headroom" class="site-header-main">
            <div class="container">
                <div class="header-medium">
                    <div class="site-branding">
                       <a class="logo-dark" href="<?php echo site_url('home'); ?>" title="Tripenta" rel="home">
                         <img src="<?php echo base_url(); ?>assets/image/logo.png" alt="Logo"/>
                       </a>
                       <a class="logo-light" href="<?php echo site_url('home'); ?>" title="Tripenta" rel="home">
                         <img src="<?php echo base_url(); ?>assets/image/logo.png" alt="Logo Light"/>
                       </a>
                       <a class="logo-mobile" href="<?php echo site_url('home'); ?>" title="Tripenta" rel="home">
                         <img src="<?php echo base_url(); ?>assets/image/logo.png" alt="Logo Mobile"/>
                       </a>
                     </div>
                    <nav id="site-navigation" class="main-navigation">
                        <ul id="mastmenu" class="primary-menu">
                          <li id="menu-item-20" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-20"><a href="#" class="no-one-page">Home</a>
                          </li>
                          <li id="menu-item-1777" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1777"><a href="#" class="no-one-page">Features</a>
                          </li>

                          <li id="menu-item-887" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-887"><a href="#" class="no-one-page">Room</a>
                          </li>
                          <li id="menu-item-1019" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1019"><a href="#" class="no-one-page">Contact</a></li>
                        </ul>
                      </nav>
                      <div class="site-menu-right d-none d-lg-block">
                          <span class="menu-right-item h-btn-search"><i class="fa fa-search"></i></span>
                      </div>
                      </div>
                  </div>
            <!--<div id="main-menu-mobile">
                                                            <div id="search-mobile">
                            <span class="h-btn-search"><i class="fa fa-search"></i></span>
                        </div>
                                                                <div class="btn-login">
                                                            <span class="menu-right-item btn-sign-up"><i class="login-icon fa fa-power-off"></i>             <a class="go_to_login_link" href="https://demo.themenovo.com/nanovi/wp-login.php" >Login / Register</a>
            </span>
                                                    </div>
                                                    <span class="btn-nav-mobile open-menu">
                    <span></span>
                </span>
            </div>-->
        </div>
    </div>
</header>        <div id="content" class="site-content">
    <div class="container content-container">
        <div class="row content-row">
            <div id="primary"  class="content-area content-full-width col-12">
                <main id="main" class="site-main">

<article id="post-40" class="post-40 page type-page status-publish hentry">
    <div class="entry-content clearfix">
        <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><link href="https://fonts.googleapis.com/css?family=Montserrat:400%2C300%2C500%7CCormorant+Garamond:700" rel="stylesheet" property="stylesheet" type="text/css" media="all">
<div id="rev_slider_3_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
<!-- START REVOLUTION SLIDER 5.4.7.2 fullwidth mode -->
	<div id="rev_slider_3_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7.2">
    <ul>	<!-- SLIDE  -->
	     <li data-index="rs-7" data-transition="slidingoverlayright,slidingoverlayleft,slidingoverlayhorizontal,slideremoveright,slideremoveleft,slideremovehorizontal" data-slotamount="default,default,default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default,default,default,default,default,default" data-easeout="default,default,default,default,default,default" data-masterspeed="default,default,default,default,default,default"  data-thumb=""  data-rotate="0,0,0,0,0,0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="<?php echo base_url(); ?>assets/image/banner.jpg"  alt="" title="bg-slide1"  width="1920" height="960" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="115" data-rotatestart="0" data-rotateend="0" data-blurstart="0" data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->

		<!-- LAYER NR. 1 -->
		<div class="tp-caption     rev_group"
			 id="slide-7-layer-5"
			 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
			 data-y="['top','middle','middle','middle']" data-voffset="['80','0','0','0']"
						data-width="['100%','100%','100%','733']"
			data-height="['340','339','339','337']"
			data-whitespace="nowrap"

			data-type="group"
			data-responsive_offset="on"

			data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-margintop="[0,0,0,0]"
			data-marginright="[0,0,0,0]"
			data-marginbottom="[0,0,0,0]"
			data-marginleft="[0,0,0,0]"
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 5; min-width: 100%px; max-width: 100%px; max-width: 340px; max-width: 340px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
		<!-- LAYER NR. 2 -->
		<div class="tp-caption   tp-resizeme"
			 id="slide-7-layer-1"
			 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
			 data-y="['top','top','middle','top']" data-voffset="['0','0','-82','0']"
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
			data-visibility="['on','on','on','off']"

			data-type="image"
			data-responsive_offset="on"

			data-frames='[{"delay":"+1590","speed":1300,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
			data-margintop="[0,0,0,0]"
			data-marginright="[0,0,0,0]"
			data-marginbottom="[0,0,0,0]"
			data-marginleft="[0,0,0,0]"
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 6;"><div class="image_class" style="border:0px solid red;width:470px;height:140px;background:url(<?php echo base_url(); ?>assets/image/logo.png);background-repeat: no-repeat;background-position: center; background-size: 500px 150px;"></div></div>

		<!-- LAYER NR. 3 -->
		</div>
	</li>
</ul>
<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
						if(htmlDiv) {
							htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
						}else{
							var htmlDiv = document.createElement("div");
							htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
							document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
						}
					</script>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
				if(htmlDiv) {
					htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
				}else{
					var htmlDiv = document.createElement("div");
					htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
					document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
				}
			</script>
		<script type="text/javascript">
if (setREVStartSize!==undefined) setREVStartSize(
	{c: '#rev_slider_3_1', responsiveLevels: [1240,1024,778,480], gridwidth: [1170,1024,991,767], gridheight: [995,768,768,480], sliderLayout: 'fullwidth'});

var revapi3,
	tpj;
(function() {
	if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded",onLoad); else onLoad();
	function onLoad() {
		if (tpj===undefined) { tpj = jQuery; if("off" == "on") tpj.noConflict();}
	if(tpj("#rev_slider_3_1").revolution == undefined){
		revslider_showDoubleJqueryError("#rev_slider_3_1");
	}else{
		revapi3 = tpj("#rev_slider_3_1").show().revolution({
			sliderType:"standard",
			jsFileLocation:"//demo.themenovo.com/nanovi/wp-content/plugins/revslider/public/assets/js/",
			sliderLayout:"fullwidth",
			dottedOverlay:"none",
			delay:9000,
			navigation: {
				keyboardNavigation:"off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation:"off",
 							mouseScrollReverse:"default",
				onHoverStop:"off",
				arrows: {
					style:"gyges",
					enable:true,
					hide_onmobile:false,
					hide_onleave:false,
					tmp:'',
					left: {
						h_align:"left",
						v_align:"center",
						h_offset:10,
						v_offset:0
					},
					right: {
						h_align:"right",
						v_align:"center",
						h_offset:10,
						v_offset:0
					}
				}
			},
			responsiveLevels:[1240,1024,778,480],
			visibilityLevels:[1240,1024,778,480],
			gridwidth:[1170,1024,991,767],
			gridheight:[995,768,768,480],
			lazyType:"none",
			shadow:0,
			spinner:"spinner0",
			stopLoop:"off",
			stopAfterLoops:-1,
			stopAtSlide:-1,
			shuffle:"off",
			autoHeight:"off",
			disableProgressBar:"on",
			hideThumbsOnMobile:"off",
			hideSliderAtLimit:0,
			hideCaptionAtLimit:0,
			hideAllCaptionAtLilmit:0,
			debugMode:false,
			fallbacks: {
				simplifyAll:"off",
				nextSlideOnWindowFocus:"off",
				disableFocusListener:false,
			}
		});
	}; /* END OF revapi call */

 }; /* END OF ON LOAD FUNCTION */
}()); /* END OF WRAPPING FUNCTION */
</script>
		<script>
					var htmlDivCss = unescape("ul.tp-revslider-mainul%20.slotholder%3A%3Abefore%20%7B%0A%20%20%20%20background%3A%20rgba%280%2C%200%2C%200%2C%200.55%29%3B%0A%7D");
					var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
					if(htmlDiv) {
						htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
					}
					else{
						var htmlDiv = document.createElement('div');
						htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
						document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
					}
		</script></div><!-- END REVOLUTION SLIDER --></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid row-visible"><div class="wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner vc_custom_1544669235201"><div class="wpb_wrapper"><div id="cms-space-5c3ea4605569f">
	     <div class="cms-inline-css" style="display:none" data-css="@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4605569f .cms-space {
					height: 50px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4605569f .cms-space {
					 height: 50px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4605569f .cms-space {
					height: 50px;
				}
			}">
    </div>
      <div class="cms-space"></div>
      </div>
      <form class="swh-search swh-search-default" name="swh-search-form" method="get" action="<?= base_url() . 'index.php/rooms/availability' ?>">
		    <div class="swh-search-meta clearfix">
        <div class="swh-search-item swh-search-checkin">
            <div class="swh-wg-field">
                <label for="swh-ft-check-in-2">Check - In</label>
                <div class="date-checkin-layout">
                    <span class="date"><?= date('d') ?></span>
                    <span class="month"><?= date('M') ?></span>
                </div>
                <input type="text" class="swh-ft-check-in" name="check-in" id="swh-ft-check-in-2" value="<?= date('m/d/Y') ?>" readonly>
            </div>
        </div>
        <div class="swh-search-item swh-search-checkout">
            <div class="swh-wg-field">
                <label for="swh-ft-check-out-2">Check - Out</label>
                <div class="date-checkout-layout">
                    <span class="date"><?= date('d',strtotime('tomorrow')) ?></span>
                    <span class="month"><?= date('M',strtotime('tomorrow')) ?></span>
                </div>
                <input type="text" class="swh-ft-check-out" name="check-out" id="swh-ft-check-out-2"
                       value="<?= date('m/d/Y', strtotime('tomorrow')) ?>" readonly>
            </div>
        </div>
        <!--<div class="swh-search-item swh-search-guests">
            <div class="swh-wg-field">
                <label>Guests</label>
                <div class="guest-layout">
                    <span>1</span>
                </div>
                <div class="btn-up-dow">
                    <span class="btn-up swh-plus-guest">
                        <i class="fa fa-angle-up"></i>
                    </span>
                    <span class="btn-dow swh-minus-guest">
                        <i class="fa fa-angle-down"></i>
                    </span>
                </div>
                <input type="hidden" name="number-guests" value="1">
            </div>
        </div>-->
        <div class="swh-search-item swh-search-submit">
            <div class="swh-wg-field">
                <input class="btn btn-block btn-primary-lighten" type="submit"
                       value="Check Availability">
            </div>
        </div>
      </div>
    </form>
    </div>
    </div>
  </div>
  </div>
<div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div id="cms-space-5c3ea4605669d">
	   <div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4605669d .cms-space {
					height: 120px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4605669d .cms-space {
					height: 120px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4605669d .cms-space {
					 height: 80px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4605669d .cms-space {
					height: 60px;
				}
			}
			"></div>	<div class="cms-space"></div>
    </div><div id="cms-mediabox-5c3ea46056a1a" class="cms-mediabox">
	     <div class="cms-mediabox-inner clearfix">
        <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-mediabox-content">
                <div class="cms-mediabox-content">
                    <div class="inner-content">
                                                    <h3 class="cms-mediabox-title" style="font-size:40px;font-weight:600;">
                                Tripenta Luxury Hotel                            </h3>
                                                                            <div class="cms-content" >
                                We are proud to say that since our opening in ’98 we have been serving our visitors in the best possible way. In Hotel Tripenta, where each one of our 17 rooms – with its own personality and style – will help you to feel at home and to live the “Tripenta experience” as you would never have imagined it.                            </div>
                                                                            <a class="btn-text" href="https://demo.themenovo.com/nanovi/about-us/" target="_self">
                                MORE ABOUT US                            </a>
                                            </div>
                </div>
            </div>
            <div class="col-xl-7 col-lg-7 col-md-6 col-mediabox-image">
                    <div class="cms-mediabox-feature">
                        <img src="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/08/bg-banner-box.jpg" alt="Build House"/>
                    </div>
                </div>
                    </div>
	         </div>
</div><div id="cms-space-5c3ea46056fe4">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46056fe4 .cms-space {
					height: 120px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46056fe4 .cms-space {
					height: 120px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46056fe4 .cms-space {
					 height: 80px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46056fe4 .cms-space {
					height: 60px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div></div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1543066746381 vc_row-has-fill"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner vc_custom_1542945071787"><div class="wpb_wrapper"><div id="cms-space-5c3ea460577d4">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea460577d4 .cms-space {
					height: 112px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea460577d4 .cms-space {
					height: 112px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea460577d4 .cms-space {
					 height: 80px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea460577d4 .cms-space {
					height: 60px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div><div id="cms-heading" class="cms-heading cms-heading-layout2  wpb_animate_when_almost_visible wpb_pulse pulse">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading .cms-heading-tag {
                    font-size: 50px !important;
                                    }
            }

                    @media (min-width: 768px) and (max-width: 991px) {
                #cms-heading .cms-heading-tag {
                    font-size: 30px !important;
                                    }
            }

                    @media screen and (max-width: 767px) {
                #cms-heading .cms-heading-tag {
                    font-size: 30px !important;

                }
            }


    "></div>
            <div class="cms-heading-sub" style="font-weight:300;text-transform:uppercase;">
            luxury hotel        </div>
                <h3 class="cms-heading-tag " style="color:#222222;font-size:60px;letter-spacing:0.1em;font-weight:700;text-transform:uppercase; ">
            <span>Best Room</span>
        </h3>

                        <div class="cms-fancybox-icon">
                <i class="fa fa-snowflake-o" ></i>
            </div>

    </div><div id="cms-space-5c3ea46057f1e">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46057f1e .cms-space {
					height: 60px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46057f1e .cms-space {
					height: 60px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46057f1e .cms-space {
					 height: 50px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46057f1e .cms-space {
					height: 30px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div>
<div class="cms-best-room1">
    <div class="cms-carousel owl-carousel featured-active" data-item-xs=1 data-item-sm=2 data-item-md=3 data-item-lg=3 data-margin=30 data-loop=false data-autoplay=false data-autoplaytimeout=5000 data-smartspeed=250 data-center=false data-arrows=false data-bullets=false data-stagepadding=0 data-rtl=false >
<?php foreach($resultSet as $rooms) { ?>
      <article class="cms-carousel-item cms-room-item cms-room-archive-item  ">
        <div class="cms-room-image">
            <a class="cms-bgimage" href="<?php echo site_url('roomDetails/getdetails'); ?>/<?= $rooms['ixRoomCategory']  ?>">
                <img src="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/08/room3-700x515.jpg" alt="room3">
            </a>
        </div>
        <div class="cms-room-holder">
            <div class="room-content">
                <h3 class="cms-room-title">
                    <a href="<?php echo site_url('roomDetails/getdetails'); ?>/<?= $rooms['ixRoomCategory']  ?>"><?= $rooms['categoryName'] ?></a>
                </h3>
                <div class="cms-room-rating">
                  <div class="swh-reviews-star">
                    <div class="swh-stars">
                      <span class="swh-star-show zmdi zmdi-star" data-val="1"></span><span class="swh-star-show zmdi zmdi-star-half" data-val="2"></span><span class="swh-star-show zmdi zmdi-star-outline" data-val="3"></span><span class="swh-star-show zmdi zmdi-star-outline" data-val="4"></span><span class="swh-star-show zmdi zmdi-star-outline" data-val="5"></span>
                    </div>
                  </div>
                </div>
            </div>
            <ul class="archive-book-room">
                <li class="cms-room-btn">
                    <a class="btn-book-now" href="<?php echo site_url('roomDetails/getdetails'); ?>/<?= $rooms['ixRoomCategory']  ?>">Book Now</a>
                </li>
                <li class="cms-room-meta-price">
                    <span class="cms-wrapper-price">
                        <span class="cms-price">&#x20b9; <?= $rooms['categoryBasePrice'] ?></span>
                        <span class="cms-suffix">Per Night</span>
                    </span>
                </li>
            </ul>
        </div>
       </article>
<?php } ?>
    </div>
    <div class="cms-best-room-all"><a href="<?php echo site_url('rooms'); ?>">View All Rooms</a></div>
</div>
<div id="cms-space-5c3ea4605cd84">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4605cd84 .cms-space {
					height: 120px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4605cd84 .cms-space {
					height: 120px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4605cd84 .cms-space {
					 height: 80px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4605cd84 .cms-space {
					height: 60px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="1.5" data-vc-parallax-image="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/11/bg-section-home1.jpg" class="vc_row wpb_row vc_row-fluid vc_custom_1542729353018 vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving row-overlay"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div id="cms-space-5c3ea4605d858">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4605d858 .cms-space {
					height: 60px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4605d858 .cms-space {
					height: 60px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4605d858 .cms-space {
					 height: 50px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4605d858 .cms-space {
					height: 50px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-2" class="cms-heading cms-heading-layout2  wpb_animate_when_almost_visible wpb_pulse pulse">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-2 .cms-heading-tag {
                    font-size: 50px !important;
                                    }
            }

                    @media (min-width: 768px) and (max-width: 991px) {
                #cms-heading-2 .cms-heading-tag {
                    font-size: 30px !important;
                                    }
            }

                    @media screen and (max-width: 767px) {
                #cms-heading-2 .cms-heading-tag {
                    font-size: 30px !important;
                 line-height: 35px !important;
                }
            }


    "></div>
            <div class="cms-heading-sub" style="color:#ffffff;font-size:20px;font-weight:500;text-transform:uppercase;">
            Summer Holiday Package        </div>
                <div class="cms-heading-tag " style="color:#ffffff;font-size:50px;font-weight:600;text-transform:uppercase; font-family:Montserrat; font-weight:700; font-style:normal; ">
            <span>STARTS AT &#x20B9; 500 PER NIGHT ONLY</span>
        </div>


    </div><div id="cms-space-5c3ea4605e1ec">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4605e1ec .cms-space {
					height: 17px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4605e1ec .cms-space {
					height: 17px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4605e1ec .cms-space {
					 height: 20px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4605e1ec .cms-space {
					height: 20px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div>

<div id="cms-button"  class="cms-button-wrapper btn-align-center  btn-align-center-md btn-align-center-sm btn-align-center-xs " >
    <div class="cms-inline-css" style="display:none" data-css="                        #cms-button.cms-button-wrapper .btn:hover {
                   background-color: #222222!important;
                   border-color: #222222!important;
                }

            "></div><a class="btn btn-default  btn-size-md" style="font-weight:500;background-color:#c5a46d;color:#ffffff;border-color:#c5a46d;"  href="<?php echo site_url('rooms'); ?>"   target="_self">
                <span>BOOK A ROOM NOW</span>
              </a></div><div id="cms-space-5c3ea4605e883">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4605e883 .cms-space {
					height: 100px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4605e883 .cms-space {
					height: 100px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4605e883 .cms-space {
					 height: 80px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4605e883 .cms-space {
					height: 80px;
				}
			}
			"></div>
      <div class="cms-space"></div>
    </div>
 </div>
</div>
</div>
</div>
<div class="vc_row-full-width vc_clearfix"></div><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_custom_1543076254495 vc_row-no-padding vc_row-o-equal-height vc_row-o-content-middle vc_row-flex"><div class="wpb_animate_when_almost_visible wpb_left-to-right left-to-right wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6 vc_col-has-fill rm-padding-lg"><div class="vc_column-inner vc_custom_1544687133930"><div class="wpb_wrapper"><div id="cms-space-5c3ea4605f123">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4605f123 .cms-space {
					height: 30px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4605f123 .cms-space {
					height: 30px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4605f123 .cms-space {
					 height: 80px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4605f123 .cms-space {
					height: 60px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div>
<div id="cms-heading-3" class="cms-heading align-center align-center-md align-center-sm align-center-xs ">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-3 .cms-heading-tag {
                    font-size: 40px !important;
                                    }
            }

                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-3 .cms-heading-tag {
                        font-size: 30px !important;
                                            }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-3 .cms-heading-tag {
                        font-size: 30px !important;

                    }
                }
            ">
    </div>
    <h3 class="cms-heading-tag " style="color:#222222;font-size:60px;font-weight:700;text-transform:uppercase; ">
        Special Deals
    </h3>


</div>
<div id="cms-space-5c3ea4605f7c5">
	<div class="cms-inline-css" style="display:none" data-css="@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4605f7c5 .cms-space {
					height: 18px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4605f7c5 .cms-space {
					height: 18px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4605f7c5 .cms-space {
					 height: 18px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4605f7c5 .cms-space {
					height: 18px;
				}
			}
			">
    </div>
    <div class="cms-space"></div>
  </div>
  <div id="cms-textbox-5c3ea4605fb28" class="cms-textbox-default text-center" style="margin-bottom: 20px">
            <div class="cms-textbox-content" style="font-style:normal;font-size:14px; ">
            Our largest discount program, you will have the best room rates. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat felis a odio blandit, quis eleifend elit pharetra. Donec id volutpat purus, et vehicula mauris. Nullam scelerisque, massa vitae fringilla ultricies, ligula sapien ultrices ante, ac<br />
            consequat mauris ante nec neque.</div>
    </div>
    <div id="cms-space-5c3ea4605fe8b">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4605fe8b .cms-space {
					height: 50px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4605fe8b .cms-space {
					height: 50px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4605fe8b .cms-space {
					 height: 30px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4605fe8b .cms-space {
					height: 30px;
				}
			}
			">
    </div>
    <div class="cms-space">
  </div>
</div>

<div id="cms-button-2"  class="cms-button-wrapper btn-align-center  btn-align-center-md btn-align-center-sm btn-align-center-xs " >
    <div class="cms-inline-css" style="display:none" data-css="                        #cms-button-2.cms-button-wrapper .btn:hover {
                   background-color: #c5a46d!important;
                   border-color: #c5a46d!important;
                }

            "></div>
            <a class="btn btn-default  btn-size-md" style="padding-top:8px;padding-bottom:8px;font-weight:400;font-size:17px;background-color:#222222;color:#ffffff;border-color:#222222;"  href="#"   target="_self">
              <span>CHECK ALL END NOW</span>
            </a>
</div>
<div id="cms-space-5c3ea46060502">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46060502 .cms-space {
					height: 50px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46060502 .cms-space {
					height: 50px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46060502 .cms-space {
					 height: 50px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46060502 .cms-space {
					height: 50px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div>
</div>
</div>
</div>
        <div class="wpb_animate_when_almost_visible wpb_right-to-left right-to-left wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6"><div class="vc_column-inner vc_custom_1544669374257"><div class="wpb_wrapper"><div class="cms-special-layout1">

          <div class="cms-carousel owl-carousel featured-active" data-item-xs=1 data-item-sm=1 data-item-md=1 data-item-lg=1 data-margin=30 data-loop=false data-autoplay=false data-autoplaytimeout=5000 data-smartspeed=250 data-center=false data-arrows=false data-bullets=false data-stagepadding=0 data-rtl=false >
              <?php foreach($special as $rooms) { ?>
                      <article class="cms-carousel-item cms-room-special-item">
                      <div class="cms-room-image">
                          <a class="cms-bgimage"
                             href="#">
                              <img src="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/08/room9-960x632.jpg" alt="Room9">
                          </a>
                      </div>
                      <div class="cms-room-holder">
                          <div class="room-content">
                              <h3 class="cms-room-title">
                                  <a href="<?php echo site_url('roomDetails/getdetails'); ?>/<?= $rooms['ixRoomCategory']  ?>"><?= $rooms['categoryName'] ?></a>
      							                                <span class="cms-room-sale">Sale 10%</span>
      								                        </h3>
                          </div>
                          <ul class="archive-book-room">
                              <li class="cms-room-btn">
                                  <a class="btn-book-now" href="<?php echo site_url('roomDetails/getdetails'); ?>/<?= $rooms['ixRoomCategory']  ?>">
                                      Book Now                            </a>
                              </li>
                              <li class="cms-room-meta-price">
                                  <span class="cms-wrapper-price">
                                      <span class="cms-price">&#x20b9; <?= $rooms['categoryBasePrice'] ?></span>
                                      <span class="cms-suffix">Per Night</span>
                                  </span>
                              </li>
                          </ul>
                      </div>
                  </article>
                  <?php } ?>
      			    </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="vc_row-full-width vc_clearfix"></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner vc_custom_1542945632093"><div class="wpb_wrapper"><div id="cms-space-5c3ea46064847">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46064847 .cms-space {
					height: 110px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46064847 .cms-space {
					height: 110px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46064847 .cms-space {
					 height: 80px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46064847 .cms-space {
					height: 60px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-4" class="cms-heading cms-heading-layout2  wpb_animate_when_almost_visible wpb_fadeInDown fadeInDown">
    <div class="cms-inline-css" style="display:none" data-css="@media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-4 .cms-heading-tag {
                    font-size: 50px !important;
                                    }
            }

                    @media (min-width: 768px) and (max-width: 991px) {
                #cms-heading-4 .cms-heading-tag {
                    font-size: 30px !important;
                                    }
            }

                    @media screen and (max-width: 767px) {
                #cms-heading-4 .cms-heading-tag {
                    font-size: 30px !important;

                }
            }

                    #cms-heading-4 .cms-fancybox-icon:before {
               background-color: #c5a46d!important;
            }
            #cms-heading-4 .cms-fancybox-icon:after {
               background-color: #c5a46d!important;
            }"></div>
            <div class="cms-heading-sub" style="margin-bottom:13px;text-transform:uppercase;">
            the best hotel        </div>
                <h3 class="cms-heading-tag " style="color:#222222;font-size:60px;letter-spacing:0.1em;font-weight:700;text-transform:uppercase; ">
            <span>our services</span>
        </h3>

                        <div class="cms-fancybox-icon">
                <i class="fa fa-snowflake-o"  style="color:#c5a46d;" ></i>
            </div>

    </div><div id="cms-space-5c3ea46064f66">
	<div class="cms-inline-css" style="display:none" data-css="@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46064f66 .cms-space {
					height: 55px;
				}
			}
					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46064f66 .cms-space {
					height: 55px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46064f66 .cms-space {
					 height: 20px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46064f66 .cms-space {
					height: 20px;
				}
			}"></div>	<div class="cms-space"></div>
</div><div class="vc_tta-container" data-vc-action="collapse"><div class="vc_general vc_tta vc_tta-tabs vc_tta-color-grey vc_tta-style-classic vc_tta-shape-square vc_tta-o-shape-group vc_tta-tabs-position-top vc_tta-controls-align-left wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp"><div class="vc_tta-tabs-container"><ul class="vc_tta-tabs-list"><li class="vc_tta-tab vc_active" data-vc-tab><a href="#1533523599387-ebfc7872-0b6489c3-4e73" data-vc-tabs data-vc-container=".vc_tta"><span class="vc_tta-title-text">Massage</span></a></li><li class="vc_tta-tab" data-vc-tab><a href="#1533527126194-a4d85402-a89e89c3-4e73" data-vc-tabs data-vc-container=".vc_tta"><span class="vc_tta-title-text">Restaurant</span></a></li><li class="vc_tta-tab" data-vc-tab><a href="#1533527285489-3dfde0e6-380a89c3-4e73" data-vc-tabs data-vc-container=".vc_tta"><span class="vc_tta-title-text">golf club</span></a></li><li class="vc_tta-tab" data-vc-tab><a href="#1533527245942-a34491fd-5f7f89c3-4e73" data-vc-tabs data-vc-container=".vc_tta"><span class="vc_tta-title-text">Bar club</span></a></li></ul></div><div class="vc_tta-panels-container"><div class="vc_tta-panels"><div class="vc_tta-panel vc_active" id="1533523599387-ebfc7872-0b6489c3-4e73" data-vc-content=".vc_tta-panel-body"><div class="vc_tta-panel-heading"><h4 class="vc_tta-panel-title"><a href="#1533523599387-ebfc7872-0b6489c3-4e73" data-vc-accordion data-vc-container=".vc_tta-container"><span class="vc_tta-title-text">Massage</span></a></h4></div><div class="vc_tta-panel-body"><div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill wpb_animate_when_almost_visible wpb_none none"><div class="vc_column-inner vc_custom_1544669519639"><div class="wpb_wrapper"><div id="cms-space-5c3ea4606664a">
	<div class="cms-inline-css" style="display:none" data-css="@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606664a .cms-space {
					height: 500px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606664a .cms-space {
					height: 500px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4606664a .cms-space {
					 height: 500px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4606664a .cms-space {
					height: 300px;
				}
			}"></div>	<div class="cms-space"></div>
</div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill"><div class="vc_column-inner vc_custom_1533539698915"><div class="wpb_wrapper"><div id="cms-space-5c3ea46066af6">
	<div class="cms-inline-css" style="display:none" data-css="@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46066af6 .cms-space {
					height: 25px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46066af6 .cms-space {
					height: 20px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46066af6 .cms-space {
					 height: 10px;
				}
			}"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-5" class="cms-heading align-left align-left-md align-left-sm align-left-xs ">
    <div class="cms-inline-css" style="display:none" data-css="@media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-5 .cms-heading-tag {
                    font-size: 13px !important;
                                    }
            }
                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-5 .cms-heading-tag {
                        font-size: 13px !important;
                        line-height: 20px !important;                    }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-5 .cms-heading-tag {
                        font-size: 13px !important;
                     line-height: 20px !important;
                    }
                }"></div>
    <div class="cms-heading-tag " style="margin-bottom:0px;color:#999999;font-size:13px;font-weight:300;text-transform:uppercase; ">
        Service is always the best    </div>


    </div><div id="cms-space-5c3ea4606717e">
	<div class="cms-inline-css" style="display:none" data-css="@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606717e .cms-space {
					height: 25px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606717e .cms-space {
					height: 23px;
				}
			}"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-6" class="cms-heading align-left align-left-md align-left-sm align-left-xs ">
    <div class="cms-inline-css" style="display:none" data-css="@media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-6 .cms-heading-tag {
                    font-size: 40px !important;
                                    }
            }

                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-6 .cms-heading-tag {
                        font-size: 30px !important;
                        line-height: 30px !important;                    }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-6 .cms-heading-tag {
                        font-size: 30px !important;
                     line-height: 30px !important;
                    }
                }"></div>
    <h3 class="cms-heading-tag " style="margin-bottom:0px;color:#ffffff;font-size:40px;font-weight:600;text-transform:uppercase; ">
        Relax with</h3>


    </div><div id="cms-space-5c3ea460677ca">
	<div class="cms-inline-css" style="display:none" data-css="@media screen and (min-width: 1200px) {
				#cms-space-5c3ea460677ca .cms-space {
					height: 12px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea460677ca .cms-space {
					height: 12px;
				}
			}"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-7" class="cms-heading align-left align-left-md align-left-sm align-left-xs ">
    <div class="cms-inline-css" style="display:none" data-css="@media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-7 .cms-heading-tag {
                    font-size: 40px !important;
                                    }
            }

                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-7 .cms-heading-tag {
                        font-size: 30px !important;
                        line-height: 30px !important;                    }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-7 .cms-heading-tag {
                        font-size: 30px !important;
                     line-height: 30px !important;
                    }
                }"></div>
    <h3 class="cms-heading-tag " style="margin-bottom:0px;color:#c5a46d;font-size:40px;font-weight:600;text-transform:uppercase; ">
        massage &amp; spa    </h3>


    </div><div id="cms-space-5c3ea46067e23">
	<div class="cms-inline-css" style="display:none" data-css="@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46067e23 .cms-space {
					height: 55px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46067e23 .cms-space {
					height: 55px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46067e23 .cms-space {
					 height: 25px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46067e23 .cms-space {
					height: 15px;
				}
			}"></div>	<div class="cms-space"></div>
</div>
<div id="cms-textbox-5c3ea46068134" class="cms-textbox-default text-left" style="margin-bottom: 20px">
            <div class="cms-textbox-content" style="color:#999999;font-style:normal;font-size:14px; ">
            Tripenta Hotel is one of the prestigious beauty sites trusted by many customers when they want to take care of their body, enjoy the relaxation, comfort. With standard beauty treatments: body massage, Thai massage, foot massage, sauna ... using natural ingredients combined with Oriental medicine, Sahul Massage &amp; Sauna will bring to customers Relaxing moments, relieving stress.        </div>
    </div><div id="cms-space-5c3ea46068457">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46068457 .cms-space {
					height: 48px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46068457 .cms-space {
					height: 20px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46068457 .cms-space {
					 height: 20px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46068457 .cms-space {
					height: 20px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div>

<div id="cms-button-3"  class="cms-button-wrapper btn-align-left  btn-align-left-md btn-align-left-sm btn-align-left-xs " >
    <div class="cms-inline-css" style="display:none" data-css="#cms-button-3.cms-button-wrapper .btn:hover {
                   background-color: #ffffff!important;
                   border-color: #ffffff!important;
                }

                        #cms-button-3.cms-button-wrapper .btn:hover {
                   color: #222222!important;
                }"></div>    <a class="btn btn-default  btn-size-md" style="font-weight:600;background-color:#c5a46d;color:#ffffff;"  href="#"   target="_self">
                                                        <span>READ MORE</span>
            </a>
</div><div id="cms-space-5c3ea46068a7d">
	<div class="cms-inline-css" style="display:none" data-css="@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46068a7d .cms-space {
					height: 70px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46068a7d .cms-space {
					height: 70px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46068a7d .cms-space {
					 height: 50px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46068a7d .cms-space {
					height: 50px;
				}
			}"></div>	<div class="cms-space"></div>
</div></div></div></div></div></div></div><div class="vc_tta-panel" id="1533527126194-a4d85402-a89e89c3-4e73" data-vc-content=".vc_tta-panel-body"><div class="vc_tta-panel-heading"><h4 class="vc_tta-panel-title"><a href="#1533527126194-a4d85402-a89e89c3-4e73" data-vc-accordion data-vc-container=".vc_tta-container"><span class="vc_tta-title-text">Restaurant</span></a></h4></div><div class="vc_tta-panel-body"><div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill"><div class="vc_column-inner vc_custom_1545021604508"><div class="wpb_wrapper"><div id="cms-space-5c3ea46069340">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46069340 .cms-space {
					height: 500px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46069340 .cms-space {
					height: 500px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46069340 .cms-space {
					 height: 500px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46069340 .cms-space {
					height: 300px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill"><div class="vc_column-inner vc_custom_1533537804591"><div class="wpb_wrapper"><div id="cms-space-5c3ea460697ec">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea460697ec .cms-space {
					height: 25px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea460697ec .cms-space {
					height: 20px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea460697ec .cms-space {
					 height: 20px;
				}
			}
					"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-8" class="cms-heading align-left align-left-md align-left-sm align-left-xs ">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-8 .cms-heading-tag {
                    font-size: 13px !important;
                                    }
            }

                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-8 .cms-heading-tag {
                        font-size: 13px !important;
                        line-height: 20px !important;                    }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-8 .cms-heading-tag {
                        font-size: 13px !important;
                     line-height: 20px !important;
                    }
                }
            "></div>
    <div class="cms-heading-tag " style="margin-bottom:0px;color:#999999;font-size:13px;font-weight:300;text-transform:uppercase; ">
        Service is always the best    </div>


    </div><div id="cms-space-5c3ea46069e36">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46069e36 .cms-space {
					height: 25px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46069e36 .cms-space {
					height: 20px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46069e36 .cms-space {
					 height: 20px;
				}
			}
					"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-9" class="cms-heading align-left align-left-md align-left-sm align-left-xs ">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-9 .cms-heading-tag {
                    font-size: 40px !important;
                                    }
            }

                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-9 .cms-heading-tag {
                        font-size: 30px !important;
                        line-height: 30px !important;                    }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-9 .cms-heading-tag {
                        font-size: 30px !important;
                     line-height: 30px !important;
                    }
                }
            "></div>
    <h3 class="cms-heading-tag " style="margin-bottom:0px;color:#ffffff;font-size:40px;font-weight:600;text-transform:uppercase; ">
        enjoy food    </h3>


    </div><div id="cms-space-5c3ea4606a487">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606a487 .cms-space {
					height: 12px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606a487 .cms-space {
					height: 12px;
				}
			}
							"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-10" class="cms-heading align-left align-left-md align-left-sm align-left-xs ">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-10 .cms-heading-tag {
                    font-size: 40px !important;
                                    }
            }

                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-10 .cms-heading-tag {
                        font-size: 30px !important;
                        line-height: 30px !important;                    }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-10 .cms-heading-tag {
                        font-size: 30px !important;
                     line-height: 30px !important;
                    }
                }
            "></div>
    <h3 class="cms-heading-tag " style="color:#c5a46d;font-size:40px;font-weight:600;text-transform:uppercase; ">
        restaurant    </h3>


    </div><div id="cms-space-5c3ea4606aae2">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606aae2 .cms-space {
					height: 35px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606aae2 .cms-space {
					height: 35px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4606aae2 .cms-space {
					 height: 10px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4606aae2 .cms-space {
					height: 10px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div>
<div id="cms-textbox-5c3ea4606add6" class="cms-textbox-default text-left" style="margin-bottom: 20px">
            <div class="cms-textbox-content" style="color:#999999;font-style:normal;font-size:14px; ">
            Chef Amari imaginatively crafts delicious dishes, from burgers and game to fine dining and traditional regional specialities. She sources fresh seasonal produce daily from local producers for our Hotel Restaurant and Wine Bar. Herbs from our gardens, Spier free-range eggs, SASSI green-listed seafood and organic, as far as is humanly possible.        </div>
    </div><div id="cms-space-5c3ea4606b0ca">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606b0ca .cms-space {
					height: 48px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606b0ca .cms-space {
					height: 20px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4606b0ca .cms-space {
					 height: 20px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4606b0ca .cms-space {
					height: 20px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div>

<div id="cms-button-4"  class="cms-button-wrapper btn-align-left  btn-align-left-md btn-align-left-sm btn-align-left-xs " >
    <div class="cms-inline-css" style="display:none" data-css="                        #cms-button-4.cms-button-wrapper .btn:hover {
                   background-color: #ffffff!important;
                   border-color: #ffffff!important;
                }

                        #cms-button-4.cms-button-wrapper .btn:hover {
                   color: #222222!important;
                }
            "></div>    <a class="btn btn-default  btn-size-md" style="font-weight:600;background-color:#c5a46d;color:#ffffff;border-color:#c5a46d;"  href="#"   target="_self">
                                                        <span>READ MORE</span>
            </a>
</div><div id="cms-space-5c3ea4606b6e3">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606b6e3 .cms-space {
					height: 70px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606b6e3 .cms-space {
					height: 70px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4606b6e3 .cms-space {
					 height: 50px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4606b6e3 .cms-space {
					height: 50px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div></div></div></div></div></div></div><div class="vc_tta-panel" id="1533527285489-3dfde0e6-380a89c3-4e73" data-vc-content=".vc_tta-panel-body"><div class="vc_tta-panel-heading"><h4 class="vc_tta-panel-title"><a href="#1533527285489-3dfde0e6-380a89c3-4e73" data-vc-accordion data-vc-container=".vc_tta-container"><span class="vc_tta-title-text">golf club</span></a></h4></div><div class="vc_tta-panel-body"><div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill"><div class="vc_column-inner vc_custom_1543567562596"><div class="wpb_wrapper"><div id="cms-space-5c3ea4606c0a0">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606c0a0 .cms-space {
					height: 500px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606c0a0 .cms-space {
					height: 400px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4606c0a0 .cms-space {
					 height: 300px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4606c0a0 .cms-space {
					height: 300px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill"><div class="vc_column-inner vc_custom_1533537904737"><div class="wpb_wrapper"><div id="cms-space-5c3ea4606c545">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606c545 .cms-space {
					height: 25px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606c545 .cms-space {
					height: 20px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4606c545 .cms-space {
					 height: 10px;
				}
			}
					"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-11" class="cms-heading align-left align-left-md align-left-sm align-left-xs ">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-11 .cms-heading-tag {
                    font-size: 13px !important;
                                    }
            }

                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-11 .cms-heading-tag {
                        font-size: 13px !important;
                        line-height: 20px !important;                    }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-11 .cms-heading-tag {
                        font-size: 13px !important;
                     line-height: 20px !important;
                    }
                }
            "></div>
    <div class="cms-heading-tag " style="margin-bottom:0px;color:#999999;font-size:13px;font-weight:300;text-transform:uppercase; ">
        Service is always the best    </div>


    </div><div id="cms-space-5c3ea4606cba2">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606cba2 .cms-space {
					height: 25px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606cba2 .cms-space {
					height: 20px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4606cba2 .cms-space {
					 height: 10px;
				}
			}
					"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-12" class="cms-heading align-left align-left-md align-left-sm align-left-xs ">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-12 .cms-heading-tag {
                    font-size: 40px !important;
                                    }
            }

                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-12 .cms-heading-tag {
                        font-size: 30px !important;
                        line-height: 30px !important;                    }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-12 .cms-heading-tag {
                        font-size: 30px !important;
                     line-height: 30px !important;
                    }
                }
            "></div>
    <h3 class="cms-heading-tag " style="margin-bottom:0px;color:#ffffff;font-size:40px;font-weight:600;text-transform:uppercase; ">
        entertain with    </h3>


    </div><div id="cms-space-5c3ea4606d217">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606d217 .cms-space {
					height: 12px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606d217 .cms-space {
					height: 12px;
				}
			}
							"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-13" class="cms-heading align-left align-left-md align-left-sm align-left-xs ">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-13 .cms-heading-tag {
                    font-size: 40px !important;
                                    }
            }

                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-13 .cms-heading-tag {
                        font-size: 30px !important;
                        line-height: 30px !important;                    }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-13 .cms-heading-tag {
                        font-size: 30px !important;
                     line-height: 30px !important;
                    }
                }
            "></div>
    <h3 class="cms-heading-tag " style="margin-bottom:0px;color:#c5a46d;font-size:40px;font-weight:600;text-transform:uppercase; ">
        golf course    </h3>


    </div><div id="cms-space-5c3ea4606d969">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606d969 .cms-space {
					height: 55px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606d969 .cms-space {
					height: 55px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4606d969 .cms-space {
					 height: 35px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4606d969 .cms-space {
					height: 25px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div>
<div id="cms-textbox-5c3ea4606dc6c" class="cms-textbox-default text-left" style="margin-bottom: 20px">
            <div class="cms-textbox-content" style="color:#999999;font-style:normal;font-size:14px; ">
            Golf or golf is a sport where players use a variety of clubs to hit the ball in a small hole on the golf course so that the number of hits is as short as possible. Unlike most other ball games, golf does not require a standardized play area. Are you looking to become a professional golfer, or have a need to learn about Golf? So what else do you hesitate, come Tripenta hotel.        </div>
    </div><div id="cms-space-5c3ea4606df6d">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606df6d .cms-space {
					height: 48px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606df6d .cms-space {
					height: 20px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4606df6d .cms-space {
					 height: 20px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4606df6d .cms-space {
					height: 20px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div>

<div id="cms-button-5"  class="cms-button-wrapper btn-align-left  btn-align-left-md btn-align-left-sm btn-align-left-xs " >
    <div class="cms-inline-css" style="display:none" data-css="                        #cms-button-5.cms-button-wrapper .btn:hover {
                   background-color: #ffffff!important;
                   border-color: #ffffff!important;
                }

                        #cms-button-5.cms-button-wrapper .btn:hover {
                   color: #222222!important;
                }
            "></div>    <a class="btn btn-default  btn-size-md" style="font-weight:600;background-color:#c5a46d;color:#ffffff;border-color:#c5a46d;"  href="#"   target="_self">
                                                        <span>READ MORE</span>
            </a>
</div><div id="cms-space-5c3ea4606e59f">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606e59f .cms-space {
					height: 70px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606e59f .cms-space {
					height: 70px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4606e59f .cms-space {
					 height: 50px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4606e59f .cms-space {
					height: 50px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div></div></div></div></div></div></div><div class="vc_tta-panel" id="1533527245942-a34491fd-5f7f89c3-4e73" data-vc-content=".vc_tta-panel-body"><div class="vc_tta-panel-heading"><h4 class="vc_tta-panel-title"><a href="#1533527245942-a34491fd-5f7f89c3-4e73" data-vc-accordion data-vc-container=".vc_tta-container"><span class="vc_tta-title-text">Bar club</span></a></h4></div><div class="vc_tta-panel-body"><div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill"><div class="vc_column-inner vc_custom_1533537951223"><div class="wpb_wrapper"><div id="cms-space-5c3ea4606ee49">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606ee49 .cms-space {
					height: 500px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606ee49 .cms-space {
					height: 400px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4606ee49 .cms-space {
					 height: 300px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4606ee49 .cms-space {
					height: 300px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill"><div class="vc_column-inner vc_custom_1533541699251"><div class="wpb_wrapper"><div id="cms-space-5c3ea4606f2e0">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606f2e0 .cms-space {
					height: 25px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606f2e0 .cms-space {
					height: 20px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4606f2e0 .cms-space {
					 height: 10px;
				}
			}
					"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-14" class="cms-heading align-left align-left-md align-left-sm align-left-xs ">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-14 .cms-heading-tag {
                    font-size: 13px !important;
                                    }
            }

                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-14 .cms-heading-tag {
                        font-size: 13px !important;
                        line-height: 20px !important;                    }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-14 .cms-heading-tag {
                        font-size: 13px !important;
                     line-height: 20px !important;
                    }
                }
            "></div>
    <div class="cms-heading-tag " style="margin-bottom:0px;color:#999999;font-size:13px;font-weight:300;text-transform:uppercase; ">
        Service is always the best    </div>


    </div><div id="cms-space-5c3ea4606f97b">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606f97b .cms-space {
					height: 25px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606f97b .cms-space {
					height: 20px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4606f97b .cms-space {
					 height: 10px;
				}
			}
					"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-15" class="cms-heading align-left align-left-md align-left-sm align-left-xs ">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-15 .cms-heading-tag {
                    font-size: 40px !important;
                                    }
            }

                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-15 .cms-heading-tag {
                        font-size: 30px !important;
                        line-height: 30px !important;                    }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-15 .cms-heading-tag {
                        font-size: 30px !important;
                     line-height: 30px !important;
                    }
                }
            "></div>
    <h3 class="cms-heading-tag " style="margin-bottom:0px;color:#ffffff;font-size:40px;font-weight:600;text-transform:uppercase; ">
        Friend meeting    </h3>


    </div><div id="cms-space-5c3ea4606ffdd">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4606ffdd .cms-space {
					height: 12px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4606ffdd .cms-space {
					height: 12px;
				}
			}
							"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-16" class="cms-heading align-left align-left-md align-left-sm align-left-xs ">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-16 .cms-heading-tag {
                    font-size: 40px !important;
                                    }
            }

                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-16 .cms-heading-tag {
                        font-size: 30px !important;
                        line-height: 30px !important;                    }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-16 .cms-heading-tag {
                        font-size: 30px !important;
                     line-height: 30px !important;
                    }
                }
            "></div>
    <h3 class="cms-heading-tag " style="color:#c5a46d;font-size:40px;font-weight:600;text-transform:uppercase; ">
        bar Tripenta    </h3>


    </div><div id="cms-space-5c3ea46070623">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46070623 .cms-space {
					height: 35px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46070623 .cms-space {
					height: 35px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46070623 .cms-space {
					 height: 20px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46070623 .cms-space {
					height: 10px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div>
<div id="cms-textbox-5c3ea46070933" class="cms-textbox-default text-left" style="margin-bottom: 20px">
            <div class="cms-textbox-content" style="color:#999999;font-style:normal;font-size:14px; ">
            Located in the heart of the upscale 8th District, the Tripenta Bar Hotel has a lounge bar with custom tailored cocktails, fitness and wellness center as well as 24-hour front desk. Tripenta Bar Hotel serves typical Parisian breakfasts daily. Guests can also enjoy international cuisine at VrayMonde Restaurant, which is decorated in Asian style and has views of the garden.        </div>
    </div><div id="cms-space-5c3ea46070cbe">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46070cbe .cms-space {
					height: 48px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46070cbe .cms-space {
					height: 20px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46070cbe .cms-space {
					 height: 20px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46070cbe .cms-space {
					height: 20px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div>

<div id="cms-button-6"  class="cms-button-wrapper btn-align-left  btn-align-left-md btn-align-left-sm btn-align-left-xs " >
    <div class="cms-inline-css" style="display:none" data-css="                        #cms-button-6.cms-button-wrapper .btn:hover {
                   background-color: #ffffff!important;
                   border-color: #ffffff!important;
                }

                        #cms-button-6.cms-button-wrapper .btn:hover {
                   color: #222222!important;
                }
            "></div>    <a class="btn btn-default  btn-size-md" style="font-weight:600;background-color:#c5a46d;color:#ffffff;border-color:#c5a46d;"  href="#"   target="_self">
                                                        <span>READ MORE</span>
            </a>
</div><div id="cms-space-5c3ea46071366">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46071366 .cms-space {
					height: 70px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46071366 .cms-space {
					height: 70px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46071366 .cms-space {
					 height: 50px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46071366 .cms-space {
					height: 50px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div></div></div></div></div></div></div></div></div></div></div><div id="cms-space-5c3ea4607188f">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4607188f .cms-space {
					height: 120px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4607188f .cms-space {
					height: 120px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4607188f .cms-space {
					 height: 60px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4607188f .cms-space {
					height: 60px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill wpb_animate_when_almost_visible wpb_pulse pulse"><div class="vc_column-inner vc_custom_1544672760343"><div class="wpb_wrapper"><div id="cms-space-5c3ea460725c8">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea460725c8 .cms-space {
					height: 60px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea460725c8 .cms-space {
					height: 30px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea460725c8 .cms-space {
					 height: 25px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea460725c8 .cms-space {
					height: 25px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div>
<div id="cms-carousel-testimonial" class="cms-carousel-testimonial cms-carousel-testimonial-layout1 owl-carousel cms-carousel-1row  " data-item-xs=1 data-item-sm=1 data-item-md=1 data-item-lg=1 data-margin=30 data-loop=true data-autoplay=false data-autoplaytimeout=5000 data-smartspeed=250 data-center=false data-arrows=false data-bullets=true data-stagepadding=0 data-rtl=false >
    <div class="cms-carousel-item-wrap">        <div class="cms-carousel-item">
            <div class="cms-item-inner">
                                    <div class="testimonial-feature">
                        <div class="overlay">
                        <a  href="#"  >
                            <img src="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/t3.jpg" alt="T3" />
                        </a>
                        </div>
                    </div>
                                                    <div class="testimonial-content"  style="color: #ffffff">
                        " We really liked staying at Tripenta Hotel. The staff was kind, the room was clean and spacious, and it's a great location, near the shopping area. The breakfast was simply delicious, with a variety of different meal."                    </div>
                                                    <h3 class="testimonial-title"  style="color: #c5a46d">
                        <a  href="#"   >
                            JULIA ROSE                        </a>
                    </h3>

                                    <span class="testimonial-position"  style="color: #999999">
                        From London, England                    </span>
                            </div>
        </div>
        </div><div class="cms-carousel-item-wrap">        <div class="cms-carousel-item">
            <div class="cms-item-inner">
                                    <div class="testimonial-feature">
                        <div class="overlay">
                        <a  href="#"  >
                            <img src="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/t2.jpg" alt="" />
                        </a>
                        </div>
                    </div>
                                                    <div class="testimonial-content"  style="color: #ffffff">
                        " We really liked staying at Tripenta Hotel. The staff was kind, the room was clean and spacious, and it's a great location, near the shopping area. The breakfast was simply delicious, with a variety of different meal."                    </div>
                                                    <h3 class="testimonial-title"  style="color: #c5a46d">
                        <a  href="#"   >
                            Julia Crush                         </a>
                    </h3>

                                    <span class="testimonial-position"  style="color: #999999">
                        New York                    </span>
                            </div>
        </div>
        </div><div class="cms-carousel-item-wrap">        <div class="cms-carousel-item">
            <div class="cms-item-inner">
                                    <div class="testimonial-feature">
                        <div class="overlay">
                        <a  href="#"   target="_self">
                            <img src="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/t1.jpg" alt="T1" />
                        </a>
                        </div>
                    </div>
                                                    <div class="testimonial-content"  style="color: #ffffff">
                        " We really liked staying at Tripenta Hotel. The staff was kind, the room was clean and spacious, and it's a great location, near the shopping area. The breakfast was simply delicious, with a variety of different meal."                    </div>
                                                    <h3 class="testimonial-title"  style="color: #c5a46d">
                        <a  href="#"    target="_self">
                            David Done                         </a>
                    </h3>

                                    <span class="testimonial-position"  style="color: #999999">
                        Florida                    </span>
                            </div>
        </div>
        </div></div><div id="cms-space-5c3ea460734ae">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea460734ae .cms-space {
					height: 95px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea460734ae .cms-space {
					height: 95px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea460734ae .cms-space {
					 height: 80px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea460734ae .cms-space {
					height: 60px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill wpb_animate_when_almost_visible wpb_pulse pulse"><div class="vc_column-inner vc_custom_1544672773377"><div class="wpb_wrapper"><div id="cms-space-5c3ea46073974">
	<div class="cms-inline-css" style="display:none" data-css="
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46073974 .cms-space {
					 height: 620px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46073974 .cms-space {
					height: 420px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div></div></div></div></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div id="cms-space-5c3ea46074172">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46074172 .cms-space {
					height: 110px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46074172 .cms-space {
					height: 110px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46074172 .cms-space {
					 height: 100px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46074172 .cms-space {
					height: 60px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-17" class="cms-heading cms-heading-layout2  wpb_animate_when_almost_visible wpb_pulse pulse">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-17 .cms-heading-tag {
                    font-size: 50px !important;
                                    }
            }

                    @media (min-width: 768px) and (max-width: 991px) {
                #cms-heading-17 .cms-heading-tag {
                    font-size: 30px !important;
                                    }
            }

                    @media screen and (max-width: 767px) {
                #cms-heading-17 .cms-heading-tag {
                    font-size: 30px !important;

                }
            }


    "></div>
            <div class="cms-heading-sub" style="text-transform:uppercase;">
            the hotel new        </div>
                <h3 class="cms-heading-tag " style="color:#222222;font-size:60px;letter-spacing:0.1em;font-weight:700;text-transform:uppercase; ">
            <span>blog &amp; event</span>
        </h3>

                        <div class="cms-fancybox-icon">
                <i class="fa fa-snowflake-o" ></i>
            </div>

    </div><div id="cms-space-5c3ea46074861">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46074861 .cms-space {
					height: 25px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46074861 .cms-space {
					height: 25px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46074861 .cms-space {
					 height: 25px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46074861 .cms-space {
					height: 10px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div>
<div id="cms-carousel-post" class="cms-blog-carousel owl-carousel  " data-item-xs=1 data-item-sm=2 data-item-md=3 data-item-lg=3 data-margin=30 data-loop=true data-autoplay=false data-autoplaytimeout=5000 data-smartspeed=250 data-center=false data-arrows=false data-bullets=true data-stagepadding=0 data-rtl=false >
    <div class="cms-carousel-item-wrap">            <div class="cms-carousel-item">
                <div class="cms-blog-inner-item">
                                            <div class="entry-box entry-featured">
                            <div class="inner-featured">
                                <a href="#">
                                    <img class="" src="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/12/bg-blog-post1-800x541.jpg" width="800" height="541" alt="bg-blog-post1" title="bg-blog-post1" />                                </a>
                            </div>
                        </div>

                    <div class="entry-content">
                        <ul class="entry-meta-blog">
                                                <li class="li-date">24 July 2018</li>
                            <li class="li-comment"><a href="#"><i class="fa fa-comments"></i> 3 Comments</a></li>
                                    </ul>
                        <div class="inner-content">
                            <h3 class="entry-title">
                                <a href="#">
                                    Travel with your lover                                </a>
                            </h3>
                            <div class="entry-excerpt">
                                I’ve travelled to five continents. I’ve sauntered along the Great Wall with a mild&hellip;                            </div>
                            <div class="read-more">
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div><div class="cms-carousel-item-wrap">            <div class="cms-carousel-item">
                <div class="cms-blog-inner-item">
                                            <div class="entry-box entry-featured">
                            <div class="inner-featured">
                                <a href="#">
                                    <img class="" src="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/12/bg-blog-post2-800x541.jpg" width="800" height="541" alt="bg-blog-post2" title="bg-blog-post2" />                                </a>
                            </div>
                        </div>

                    <div class="entry-content">
                        <ul class="entry-meta-blog">
                                                <li class="li-date">24 July 2018</li>
                            <li class="li-comment"><a href="#"><i class="fa fa-comments"></i> 0 Comment</a></li>
                                    </ul>
                        <div class="inner-content">
                            <h3 class="entry-title">
                                <a href="#">
                                    Find a good hotel                                </a>
                            </h3>
                            <div class="entry-excerpt">
                                You don't have to overpay for a hotel room. There are tricks and tips&hellip;                            </div>
                            <div class="read-more">
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div><div class="cms-carousel-item-wrap">            <div class="cms-carousel-item">
                <div class="cms-blog-inner-item">
                                            <div class="entry-box entry-featured">
                            <div class="inner-featured">
                                <a href="#">
                                    <img class="" src="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/12/bg-blog-post3-800x541.jpg" width="800" height="541" alt="bg-blog-post3" title="bg-blog-post3" />                                </a>
                            </div>
                        </div>

                    <div class="entry-content">
                        <ul class="entry-meta-blog">
                                                <li class="li-date">24 July 2018</li>
                            <li class="li-comment"><a href="#"><i class="fa fa-comments"></i> 0 Comment</a></li>
                                    </ul>
                        <div class="inner-content">
                            <h3 class="entry-title">
                                <a href="#">
                                    Simple room                                </a>
                            </h3>
                            <div class="entry-excerpt">
                                Sometimes the most luxurious rooms are the simplest. Whether you want to cure insomnia&hellip;                            </div>
                            <div class="read-more">
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div><div class="cms-carousel-item-wrap">            <div class="cms-carousel-item">
                <div class="cms-blog-inner-item">
                                            <div class="entry-box entry-featured">
                            <div class="inner-featured">
                                <a href="#">
                                    <img class="" src="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/12/bg-blog-post4-800x541.jpg" width="800" height="541" alt="bg-blog-post4" title="bg-blog-post4" />                                </a>
                            </div>
                        </div>

                    <div class="entry-content">
                        <ul class="entry-meta-blog">
                                                <li class="li-date">24 July 2018</li>
                            <li class="li-comment"><a href="#"><i class="fa fa-comments"></i> 0 Comment</a></li>
                                    </ul>
                        <div class="inner-content">
                            <h3 class="entry-title">
                                <a href="#">
                                    10 Killer Ideas For Your Hotel Blog                                </a>
                            </h3>
                            <div class="entry-excerpt">
                                Google Local Guides reconfirms the fact that local content is the way forward to&hellip;                            </div>
                            <div class="read-more">
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div></div>



<div id="cms-space-5c3ea46078557">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46078557 .cms-space {
					height: 112px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46078557 .cms-space {
					height: 112px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46078557 .cms-space {
					 height: 80px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46078557 .cms-space {
					height: 60px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div></div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="1.5" data-vc-parallax-image="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/11/bg-section-newletter.jpg" class="vc_row wpb_row vc_row-fluid vc_custom_1542729844355 vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving row-overlay"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div id="cms-space-5c3ea46078fd3">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46078fd3 .cms-space {
					height: 70px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46078fd3 .cms-space {
					height: 70px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46078fd3 .cms-space {
					 height: 50px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46078fd3 .cms-space {
					height: 50px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div><div id="cms-heading-18" class="cms-heading align-center align-center-md align-center-sm align-center-xs  wpb_animate_when_almost_visible wpb_pulse pulse">
    <div class="cms-inline-css" style="display:none" data-css="

            "></div>
    <div class="cms-heading-tag " style="color:#c5a46d;text-transform:uppercase; ">
        STAY IN TOUCH    </div>


    </div><div id="cms-heading-19" class="cms-heading align-center align-center-md align-center-sm align-center-xs  wpb_animate_when_almost_visible wpb_pulse pulse">
    <div class="cms-inline-css" style="display:none" data-css="                    @media (min-width: 991px) and (max-width: 1200px) {
                #cms-heading-19 .cms-heading-tag {
                    font-size: 30px !important;
                    line-height: 30px !important;                }
            }

                        @media (min-width: 768px) and (max-width: 991px) {
                    #cms-heading-19 .cms-heading-tag {
                        font-size: 30px !important;
                                            }
                }

                        @media screen and (max-width: 767px) {
                    #cms-heading-19 .cms-heading-tag {
                        font-size: 30px !important;

                    }
                }
            "></div>
    <h3 class="cms-heading-tag " style="color:#ffffff;font-size:30px;line-height:45px;font-weight:700;text-transform:uppercase; ">
        Join our email list and be the<br />
first to know about specials, events and more!    </h3>


    </div><div id="cms-space-5c3ea46079a41">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea46079a41 .cms-space {
					height: 37px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea46079a41 .cms-space {
					height: 37px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea46079a41 .cms-space {
					 height: 37px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea46079a41 .cms-space {
					height: 37px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div><div class="vc_row wpb_row vc_inner vc_row-fluid cms-row-newlleter"><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6 wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp"><div class="vc_column-inner"><div class="wpb_wrapper"><div  class="wpb_widgetised_column wpb_content_element new-letter-form-home1">
		<div class="wpb_wrapper">

			<div id="newsletterwidgetminimal-2" class="widget widget_newsletterwidgetminimal"><div class="widget-content"><h2 class="widget-title">NEWSLETTER SIGN-UP</h2><div class="tnp tnp-widget-minimal"><form action="https://demo.themenovo.com/nanovi/?na=s" method="post" onsubmit="return newsletter_check(this)"><input type="hidden" name="nr" value="widget-minimal"/><input class="tnp-email" type="email" required name="ne" value="" placeholder="Email"><input class="tnp-submit" type="submit" value="SIGN UP !"></form></div></div></div>
		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div></div><div id="cms-heading-20" class="cms-heading align-center align-center-md align-center-sm align-center-xs  wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp">
    <div class="cms-inline-css" style="display:none" data-css="

            "></div>
    <div class="cms-heading-tag " style="margin-top:10px;color:#c5a46d;font-size:20px;font-weight:300; ">
        See our Privacy Policy    </div>


    </div><div id="cms-space-5c3ea4607ab8c">
	<div class="cms-inline-css" style="display:none" data-css="					@media screen and (min-width: 1200px) {
				#cms-space-5c3ea4607ab8c .cms-space {
					height: 95px;
				}
			}

					@media (min-width: 992px) and (max-width: 1200px) {
				#cms-space-5c3ea4607ab8c .cms-space {
					height: 95px;
				}
			}
							@media (min-width: 768px) and (max-width: 991px) {
				#cms-space-5c3ea4607ab8c .cms-space {
					 height: 60px;
				}
			}

			@media screen and (max-width: 767px) {
				#cms-space-5c3ea4607ab8c .cms-space {
					height: 60px;
				}
			}
			"></div>	<div class="cms-space"></div>
</div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div>
    </div><!-- .entry-content -->

    </article><!-- #post-40 -->
                </main><!-- #main -->
            </div><!-- #primary -->


        </div>
    </div>

</div><!-- #content -->

<footer id="colophon" class="site-footer footer-layout2">

                    <div class="top-footer">
                <div class="container">
                    <div class="row">
                        <div class="logo-footer">
                            <a class="logo-light" href="#" title="Tripenta" rel="home"><img src="<?php echo base_url(); ?>assets/image/logo.png" alt="Logo Light"/></a>                        </div>
                    </div>
                    <div class="row">

                                                    <div class="cms-footer-item col-xl-4 col-lg-4 col-md-12">
                                <div class="f-adderess">
                                    <label>Address :</label>
                                    UL cyberpark, Kozhikode, 673603.
                                </div>
                            </div>


                        <div class="cms-footer-item col-xl-4 col-lg-4 col-md-12">
                                                        <div class="f-phone">
                                <label>Email :</label>
                                <a href="mailto:Tripentahotel@gmail.com">
                                    Tripentahotel@gmail.com                                </a>
                            </div>
                                                    </div>

                                                    <div class="cms-footer-item col-xl-4 col-lg-4 col-md-12">
                                <div class="f-phone">
                                    <label>Call phone us :</label>
                                    <a href="tel:#">
                                        1800 - 1111 - 2222                                    </a>
                                </div>
                            </div>
                                            </div>
                                            <div class="row">
                            <ul class="footer-social cms-social">
                                        <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>    <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>    <li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>                            </ul>
                        </div>
                                    </div>
            </div>

    <div class="bottom-footer">
        <div class="container">
            <div class="copyright-content">
                © 2019 Tripenta Hotel made by <a href="#">WR Solutions</a>            </div>
        </div>
    </div>
</footer><!-- #colophon -->    <div class="cms-modal cms-search-popup">
        <div class="cms-close"></div>
        <div class="cms-modal-content">
            <form role="search" method="get" class="cms-search-form placeholder-white" action="#">
                <i class="fa fa-search"></i>
                <input type="text" placeholder="Search..." name="s" class="search-field" />
            </form>
        </div>
    </div>
    <a href="#" class="scroll-top"><i class="zmdi zmdi-long-arrow-up"></i></a>
</div><!-- #page -->
<input class="cshlg-inline-css" type="hidden" value=".cshlg-popup { background-color: rgba(0, 0, 0, 0.8); }"><input class="cshlg-inline-css" type="hidden" value=""><div id="csh-login-wrap" class="cshlg-popup">

    <div class="login_dialog">

        <a class="boxclose"></a>

        <form class="login_form" id="login_form" method="post" action="#">
            <h2>Please Login</h2>
            <input type="text" class="alert_status" readonly>
                            <label for="login_user"> Username</label>

            <input type="text" name="login_user" id="login_user" />
                            <label for="pass_user"> Password </label>

            <input type="password" name="pass_user" id="pass_user" />
            <label for="rememberme" id="lb_rememberme"><input name="rememberme" type="checkbox" id="rememberme" value="forever"  /> Remember Me</label>
            <input type="submit" name="login_submit" value="LOGIN" class="login_submit" />

            <div class="social-login">
                                                    <a class="login-btn-facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a>


                            </div>
        </form>

        <form class="register_form" id="register_form"
            action="#" method="post">
            <h2>Registration</h2>
            <input type="text" class="alert_status" readonly>
            <label for="register_user">Username</label>
            <input type="text" name="register_user" id="register_user" value="" >
            <label for="register_email">E-mail</label>
            <input type="email" name="register_email" id="register_email" value="">
            <div id="allow_pass">
                <label for="register_pass">Password</label>
                <input type="password" name="register_pass" id="register_pass" value="">
                <label for="confirm_pass">Confirm Password</label>
                <input type="password" name="confirm_pass" id="confirm_pass" value="">
            </div>
                        <input type="submit" name="register_submit" id="register_submit" value="REGISTER" />
        </form>

        <form class="lost_pwd_form" action="#" method="post">
            <h2>Forgotten Password?</h2>
            <input type="text" class="alert_status" readonly>
            <label for="lost_pwd_user_email">Username or Email Adress</label>
            <input type="text" name="lost_pwd_user_email" id="lost_pwd_user_email">
            <input type="submit" name="lost_pwd_submit" id="lost_pwd_submit" value="GET NEW PASSWORD">
        </form>
                <div class="pass_and_register" id="pass_and_register">

            <a class="go_to_register_link" href="" style="">Register</a>
            <span style="color: black"> </span>
            <a class="go_to_lostpassword_link" href="">Forgot Password</a>
            <span style="color: black"></span>
            <a class="back_login" href="">Back to Login</a>

        </div>


    </div>
</div>
<script src='<?php echo base_url(); ?>assets/js/api.js'></script>
    			<script type="text/javascript">
				function revslider_showDoubleJqueryError(sliderID) {
					var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
					errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
					errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
					errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
					errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
						jQuery(sliderID).show().html(errorMessage);
				}
			</script>
<link rel='stylesheet' id='animate-css-css'  href='<?php echo base_url(); ?>assets/css/animate.min.css'  media='all' />
<link rel='stylesheet' id='swh-select-2-css'  href='<?php echo base_url(); ?>assets/css/select2.min.css'  media='all' />
<link rel='stylesheet' id='swh-jquery-ui-date-range-css-css'  href='<?php echo base_url(); ?>assets/css/jquery-ui.min.css'  media='all' />
<link property="stylesheet" rel='stylesheet' id='vc_google_fonts_montserratregular700-css'  href='//fonts.googleapis.com/css?family=Montserrat%3Aregular%2C700&#038;ver=5.0.3'  media='all' />
<link rel='stylesheet' id='vc_tta_style-css'  href='<?php echo base_url(); ?>assets/css/js_composer_tta.min.css'  media='all' />
<link rel='stylesheet' id='cshlg_layout_1-css'  href='<?php echo base_url(); ?>assets/css/layout1.css'  media='all' />

<script  src='<?php echo base_url(); ?>assets/js/cms-front.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/demo.themenovo.com\/nanovi\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script  src='<?php echo base_url(); ?>assets/js/scripts.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var nectarLove = {"ajaxurl":"https:\/\/demo.themenovo.com\/nanovi\/wp-admin\/admin-ajax.php","postID":"40","rooturl":"https:\/\/demo.themenovo.com\/nanovi\/"};
/* ]]> */
</script>
<script  src='<?php echo base_url(); ?>assets/js/post_favorite.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/headroom.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/headroom.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/cms-parallax.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/bootstrap.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/match-height-min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/sidebar-scroll-fixed.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/magnific-popup.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/nice-select.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/jquery-ui.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/main.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var newsletter = {"messages":{"email_error":"Email address is not correct","name_error":"Name is required","surname_error":"Last name is required","privacy_error":"You must accept the privacy policy"},"profile_max":"20"};
/* ]]> */
</script>
<script  src='<?php echo base_url(); ?>assets/js/validate.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/wp-embed.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/js_composer_front.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/waypoints.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/select2.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/jquery-ui.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/swh-search.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/owl.carousel.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/cms-carousel.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/skrollr.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/vc-accordion.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/vc-tta-autoplay.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/vc-tabs.min.js'></script>

</body>
</html>
