<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="#">
  <title>Rooms &#8211; Tripenta</title>
  <link rel='dns-prefetch' href='//fonts.googleapis.com' />
  <link rel='dns-prefetch' href='//s.w.org' />
  <link rel="alternate" type="application/rss+xml" title="Nanovi &raquo; Feed" href="#" />
  <link rel="alternate" type="application/rss+xml" title="Nanovi &raquo; Comments Feed" href="#" />
  <link rel="alternate" type="application/rss+xml" title="Nanovi &raquo; Rooms Feed" href="#" />
  <script type="text/javascript">window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/demo.themenovo.com\/nanovi\/wp-includes\/js\/wp-emoji-release.min.js"}};!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
  </script>
  <style type="text/css">img.wp-smiley,img.emoji{display:inline !important;border:none !important;box-shadow:none !important;height:1em !important;width:1em !important;margin:0 .07em !important;vertical-align:-0.1em !important;background:none !important;padding:0 !important}</style>
  <link rel='stylesheet' id='wp-block-library-group-css' href='<?php echo base_url(); ?>assets/css/jquery-ui1.min.css' type='text/css' media='all' />
  <style id='rs-plugin-settings-inline-css' type='text/css'>#rs-demo-id{}</style>
  <link rel='stylesheet' id='swh-front-css-group-css' href='<?php echo base_url(); ?>assets/css/style2.css' type='text/css' media='all' />
  <style id='nanovi-theme-inline-css' type='text/css'>#site-header-wrap .site-branding a img{max-height:38px}#site-header-wrap .site-branding a.logo-mobile img{max-height:45px}</style>
  <link rel='stylesheet' id='nanovi-google-fonts-css' href='https://fonts.googleapis.com/css?family=Cormorant%3A300%2C400%2C500%2C600%2C700%7CMontserrat%3A300%2C400%2C500%2C600%2C700%2C800%7CLora%3A400%2C400i%2C700%2C700i&#038;subset=latin%2Clatin-ext&#038;ver=5.0.3' media='all' />
  <script  src='<?php echo base_url(); ?>assets/js/jquery.js'></script>
  <script  src='<?php echo base_url(); ?>assets/js/jquery-migrate.min.js'></script>
  <script type='text/javascript'>
    var jsPassData = {"ajax_url":"https:\/\/demo.themenovo.com\/nanovi\/wp-admin\/admin-ajax.php","display_labels":"Labels","type_modal":"Popup","get_login_redirect":"Current Page","login_redirect":"","register_redirect":"","generated_pass":"","login_success":"Login Successfull!","login_error":"Wrong Username or Password!"};
  </script>
  <script type='text/javascript'>
    var _swhd_data = {"ajax_url":"https:\/\/demo.themenovo.com\/nanovi\/wp-admin\/admin-ajax.php","login_success":"Login Successfull!","login_error":"Wrong Username or Password!"};
  </script>
  <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/jquery-ui.min1.js'></script>
  <link rel='#' href='#' />
  <link rel="EditURI" type="application/rsd+xml" title="RSD" href="#" />
  <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="#" />
  <meta name="generator" content="WordPress 5.0.3" />
  <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/><!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://demo.themenovo.com/nanovi/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]-->
  <meta name="generator" content="Powered by Slider Revolution 5.4.7.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
  <link rel="icon" href="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/cropped-thumbnails-300x300.jpg" sizes="32x32" />
  <link rel="icon" href="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/cropped-thumbnails-300x300.jpg" sizes="192x192" />
  <link rel="apple-touch-icon-precomposed" href="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/cropped-thumbnails-300x300.jpg" />
  <meta name="msapplication-TileImage" content="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/07/cropped-thumbnails-300x300.jpg" />
  <script >function setREVStartSize(e){try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
}catch(d){console.log("Failure at Presize of Slider:"+d)}};
</script>
<style type="text/css" title="dynamic-css" class="options-output">a{color:#222}a:hover{color:#c5a46d}.site-footer .bottom-footer a{color:#c5a46d}</style>
<noscript><style type="text/css"> .wpb_animate_when_almost_visible{opacity:1}</style></noscript>
</head>
<body class="archive post-type-archive post-type-archive-swh-post header-1 hfeed visual-composer wpb-js-composer js-comp-ver-5.6 vc_responsive"><div id="page" class="site"> <header id="masthead" class="site-header"> <div id="site-header-wrap" class="header-layout1 rf-active is-sticky">
  <!--<div class="topbar-panel-mobile">
    <div class="container"> <div class="row"> <div class="col-12"> <ul class="top-information"> <li class="li-phone"> <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i>1800 - 1111 - 2222 </a> </li> <li class="li-adderess"> <i class="fa fa-map-marker" aria-hidden="true"></i> 269 King - Melbourne Australia. </li> <li class="li-sign-up"> <span class="menu-right-item btn-sign-up"> <i class="login-icon fa fa-power-off"></i> <a class="go_to_login_link" href="https://demo.themenovo.com/nanovi/wp-login.php" >Login / Register</a> </span> </li> </ul> </div> </div> </div>
  </div>
  <div class="topbar-panel"Tripenta
    <div class="container"> <div class="row"> <div class="col-lg-7"> <ul class="top-information pull-left"> <li class="li-phone"> <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i>1800 - 1111 - 2222 </a> </li> <li class="li-adderess"> <i class="fa fa-map-marker" aria-hidden="true"></i> 269 King - Melbourne Australia. </li> </ul> </div> <div class="col-lg-5"> <ul class="top-information pull-right"> <li class="li-sign-up"> <span class="menu-right-item btn-sign-up"> <i class="login-icon fa fa-power-off"></i> <a class="go_to_login_link" href="https://demo.themenovo.com/nanovi/wp-login.php" >Login / Register</a> </span> </li> </ul> </div> </div> </div>
  </div>-->
  <div id="headroom" class="site-header-main">
    <div class="container">
      <div class="header-medium">
        <div class="site-branding">
          <a class="logo-dark" href="<?php echo site_url('home'); ?>" title="Nanovi" rel="home">
            <img src="<?php echo base_url(); ?>assets/image/logo_inner_page.png" alt="Logo"/>
          </a>
          <a class="logo-light" href="<?php echo site_url('home'); ?>" title="Nanovi" rel="home">
            <img src="<?php echo base_url(); ?>assets/image/logo_inner_page.png" alt="Logo Light"/>
          </a>
          <a class="logo-mobile" href="<?php echo site_url('home'); ?>" title="Nanovi" rel="home">
            <img src="<?php echo base_url(); ?>assets/image/logo_inner_page.png" alt="Logo Mobile"/>
          </a>
        </div>
        <nav id="site-navigation" class="main-navigation">
          <ul id="mastmenu" class="primary-menu">
            <li id="menu-item-20" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-20"><a href="<?php echo site_url('home'); ?>" class="no-one-page">Home</a>
                <!--<ul class="sub-menu">
                  <li id="menu-item-48" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-48"><a href="https://demo.themenovo.com/Tripenta/" class="no-one-page">Home 1</a></li>
                  <li id="menu-item-47" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-47"><a href="https://demo.themenovo.com/Tripenta/home-2/" class="no-one-page">Home 2</a></li>
                  <li id="menu-item-2430" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2430"><a href="https://demo.themenovo.com/nanovi/home-3/" class="no-one-page">Home 3</a></li>
                </ul>-->
            </li>
          </ul>
         </nav>
         <div class="site-menu-right d-none d-lg-block"> <span class="menu-right-item h-btn-search"><i class="fa fa-search"></i></span> </div> </div> </div> <div id="main-menu-mobile"> <div id="search-mobile"> <span class="h-btn-search"><i class="fa fa-search"></i></span> </div>
         <div class="btn-login">
           <span class="menu-right-item btn-sign-up"><i class="login-icon fa fa-power-off"></i>
             <a class="go_to_login_link" href="https://demo.themenovo.com/nanovi/wp-login.php" >Login / Register</a>
           </span>
         </div> <span class="btn-nav-mobile open-menu"> <span></span> </span>
       </div>
     </div>
   </div>
 </header>
       <div id="pagetitle" class="page-title"> <div class="bg-overlay">
           <div class="container page-title-container"> <div class="row"> <div class="col-12 col-title-text">
             <div class="page-title-content clearfix"> <h1 class="page-title">Rooms Availability</h1> </div> </div>
           </div>
         </div>
        </div>
      </div>
      <div id="content" class="site-content">
        <div class="container content-container">
          <div class="row content-row">
            <div id="primary" class="content-area content-has-sidebar float-left col-xl-8 col-lg-8 col-md-12"><main id="main" class="site-main">
            <!--<div class="row">
              <form class="woocommerce-ordering" method="get">
                <div class="swh-select1">
                   <select name="sort_by" class="sort_by">
                     <option value="">--Sort By--</option>
                     <option value="h-l">Price: High-Low</option>
                     <option value="l-h">Price: Low-High</option>
                     <option value="b-s">Size: Big-Small</option>
                     <option value="s-b">Size: Small-Big</option>
                   </select>
                 </div>
               </form>
             </div>-->
            <div class="row" id="swh-rooms">
            <?php if (isset($results)) {
                     foreach ($results as $data) { ?>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                  <article class="cms-room-item cms-room-archive-item">
                     <div class="cms-room-image"><a class="cms-bgimage" href="<?php echo site_url('roomDetails/getdetails'); ?>/<?= $data->ixRoomCategory ?>">
                      <img src="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/08/room9-700x515.jpg" alt="Room9"></a>
                   </div>
                   <div class="cms-room-holder">
                     <div class="room-content">
                     <h3 class="cms-room-title"><a href="<?php echo site_url('roomDetails/getdetails'); ?>/<?=  $data->ixRoomCategory  ?>"><?= $data->categoryName ?></a></h3>
                     <ul class="archive-room-meta">
                       <li class="cms-room-meta" style="background-image: url(https://demo.themenovo.com/nanovi/wp-content/themes/nanovi/assets/images/room-meta/icon-single-room-1.png);"> <span>Acreage: 20 Ft&sup2;</span> </li>
                        <li class="cms-room-meta" style="background-image: url(https://demo.themenovo.com/nanovi/wp-content/themes/nanovi/assets/images/room-meta/icon-single-room-3.png);"> <span>Bed: 4</span>
                        </li>
                      </ul>
                        <div class="cms-room-excerpt"><?= $data->categoryDescription ?>&hellip;
                        </div>
                      </div>
                      <ul class="archive-book-room">
                        <li class="cms-room-btn"> <a class="btn-book-now" href="<?php echo site_url('roomDetails/getdetails'); ?>/<?= $data->ixRoomCategory ?>">Book Now</a> </li>
                        <li class="cms-room-meta-price"> <span class="cms-wrapper-price">
                          <span class="cms-price">&#x20b9; <?= $data->categoryBasePrice ?></span> <span class="cms-suffix">Per Night</span> </span>
                         </li>
                       </ul>
                       </div>
                     </article>
                  </div>
                <?php }
              } else { ?>
                <div class="alert alert-info">
                  <strong>Info!</strong> No Rooms available for your search dates.
                </div>
              <?php } ?>
                    <nav class="navigation posts-pagination clearfix">
                      <div class="loop-pagination">
                        <!--<span class='page-numbers current'>1</span>
                        <a class='page-numbers' href='#'>2</a>
                        <a class="next page-numbers" href="#"><i class="fa fa-angle-right"></i></a>-->

                        <!-- Show pagination links -->
                        <?php if (isset($links)) { ?>
                          <?php echo $links ?>
                        <?php } ?>
                      </div>
                    </nav>
                    </div>
                  </main>
                </div>
                    <aside id="secondary" class="widget-area widget-has-sidebar sidebar-fixed col-xl-4 col-lg-4 col-md-12"><div id="widget-filter-form-4" class="widget widget_widget-filter-form"><div class="widget-content">
                      <form class="swh-widget-search swh-search-layout-4" name="swh-search-form" method="get" action="<?= base_url() . 'index.php/rooms/availability' ?>">
                        <div class="swh-search-meta clearfix">
                          <div class="swh-search-item swh-search-checkin">
                          <div class="swh-wg-field"><input type="text" class="swh-ft-check-in" name="check-in" id="swh-ft-check-in" value="<?= date('m/d/Y') ?>" placeholder="Arrive" readonly>
                      </div></div> <div class="swh-search-item swh-search-checkout">
                        <div class="swh-wg-field">
                          <input type="text" class="swh-ft-check-out" name="check-out" id="swh-ft-check-out" value="<?= date('m/d/Y', strtotime('tomorrow')) ?>" placeholder="Depart" readonly> </div> </div> <div class="swh-search-item swh-search-guests">
                            <!--<div class="swh-wg-field">
                              <select name="number-guests">
                                <option value="">Adults</option>
                                <option value="1" selected>1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5" >5</option>
                                <option value="6" >6</option>
                                <option value="7" >7</option>
                                <option value="8" >8</option>
                                <option value="9" >9</option>
                                <option value="10" >10</option>
                              </select>
                            </div>-->
                          </div>
                          <?php /*<div class="swh-search-item swh-search-styleroom">
                            <div class="swh-wg-field">
                              <select name="room-type">
                                <option value="">Room type</option>
                                <?php foreach($categories as $category){ ?>
                                  <option value="<?= $category->ixRoomCategory ?>" ><?= $category->categoryName ?></option>
                                <?php } ?>
                              </select></div>
                            </div> */ ?>
                            <div class="swh-search-item swh-search-submit">
                              <div class="swh-wg-field">
                                <input class="btn btn-block btn-primary-lighten swh-filter-sm-change" type="submit" value="Check Availability">
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <!--<div id="widget-price-filter-2" class="widget widget_widget-price-filter">
                      <div class="widget-content">
                        <h2 class="widget-title">Max Night Price :<span class="swh-p-filter">&#36; 500</span></h2>
                        <div class="swh-price-filter" data-max="500" data-price-format="&#36; 111">
                        </div><input class="swh-price-ft-val" type="hidden" value="">
                      </div>
                    </div>-->
                      <div id="widget-best-rooms-5" class="widget widget_widget-best-rooms">
                        <div class="widget-content"><h2 class="widget-title">Best Rooms</h2>
                        <?php foreach($random as $dt){ ?>
                          <div class="posts-list">
                            <div class="entry-brief clearfix">
                              <div class="entry-media">
                                <a href="<?php echo site_url('roomDetails/getdetails'); ?>/<?= $dt['ixRoomCategory']  ?>"><img src="https://demo.themenovo.com/nanovi/wp-content/uploads/2018/08/room9-370x435.jpg" alt="King room" /></a>
                              </div>
                              <div class="entry-content">
                                <h4 class="entry-title"><a href="room-details.php" title="King room"><?= $dt['categoryName'] ?></a></h4>
                                <div class="cms-price-wg">From &#x20b9; <?= $dt['categoryBasePrice'] ?> Per night
                                </div> <a class="btn-book-now" href="<?php echo site_url('roomDetails/getdetails'); ?>/<?= $dt['ixRoomCategory']  ?>"> Book Now </a>
                              </div>
                             </div>
                            </div>
                          <?php } ?>
                          </div>
                        </div>
                        <div class="widget cms-widget-text clearfix">
                          <h2 class="widget-title"> RESERVATION SUPPORT</h2>
                          <ul> <li class="cms-widget-infor wg-infor-title">1800 - 1111 -2222</li> </ul>
                        </div>
                        </aside>
                      </div>
                    </div>
                  </div>
          <footer id="colophon" class="site-footer footer-layout1">
            <div class="top-footer">
            <div class="container">
                <div class="row row-widget-bottom">
                    <div class="cms-footer-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">

                      <div class="widget cms-widget-text clearfix">
                            <h2 class="widget-title"> CONTACT US</h2>
                        <ul>
                          <li class="cms-widget-infor wg-infor-title">Address : UL cyberpark, Kozhikode, 673016. </li>
                          <li class="cms-widget-infor wg-infor-phone"> Email : Tripentahotel@gmail.com</li>
                          <li class="cms-widget-infor wg-infor-email">Call phone us : 1800 - 1111 - 2222</li>
                        </ul>
                      </div>
                    </div>
                    <div class="cms-footer-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                      <div class="top-footer-logo">
                          <a href="<?php echo site_url('home'); ?>"><img src="<?php echo base_url(); ?>assets/image/logo.png" alt="Logo Light"/></a>
                      </div>
                      <div id="text-3" class="widget widget_text">
                        <div class="textwidget">
                          <p>We are proud to say that since our opening in ’98 we have been serving our visitors in the best possible way. live the “Tripenta experience”</p>
                        </div>
  		                </div>
                   </div>
                   <div class="cms-footer-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                     <div id="newsletterwidget-2" class="widget widget_newsletterwidget">
                       <h2 class="footer-widget-title">NEWSLETTER</h2>
                       <div class="tnp tnp-widget">
                         <form method="post" action="https://demo.themenovo.com/nanovi/?na=s" onsubmit="return newsletter_check(this)">
                            <input type="hidden" name="nr" value="widget">
                            <input type='hidden' name='nl[]' value='0'>
                            <div class="tnp-field tnp-field-email"><label>Email</label>
                              <input class="tnp-email" type="email" name="ne" required>
                            </div>
                            <div class="tnp-field tnp-field-button">
                              <input class="tnp-submit" type="submit" value="Subscribe">
                            </div>
                          </form>
                        </div>
                      </div>
                      <div id="cms_social_widget-2" class="widget widget_cms_social_widget">
                        <ul class='cms-social'>
                          <li><a target="_blank" href="#"><i class="fa fa-facebook"></i><span>Facebook</span></a></li>
                          <li><a target="_blank" href="#"><i class="zmdi zmdi-rss"></i><span>Rss</span></a></li>
                          <li><a target="_blank" href="#"><i class="fa fa-youtube"></i><span>YouTube</span></a></li>
                          <li><a target="_blank" href="#"><i class="fa fa-twitter"></i><span>Twitter</span></a></li>
                          <li><a target="_blank" href="#"><i class="fa fa-skype"></i><span>Skype</span></a></li></ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            <div class="bottom-footer">
                <div class="container">
                    <div class="copyright-content">
                        © 2018 Tripenta Hotel made by <a href="<?php echo site_url('home'); ?>">Wr Solutions</a>            </div>
                </div>
            </div>
        </footer><!-- #colophon -->
        <div class="cms-modal cms-search-popup">
                <div class="cms-close"></div>
                <div class="cms-modal-content">
                    <form role="search" method="get" class="cms-search-form placeholder-white" action="https://demo.themenovo.com/nanovi/">
                        <i class="fa fa-search"></i>
                        <input type="text" placeholder="Search..." name="s" class="search-field" />
                    </form>
                </div>
        </div>
    <a href="#" class="scroll-top"><i class="zmdi zmdi-long-arrow-up"></i></a>
</div><!-- #page -->
<input class="cshlg-inline-css" type="hidden" value=".cshlg-popup { background-color: rgba(0, 0, 0, 0.8); }"><input class="cshlg-inline-css" type="hidden" value=""><div id="csh-login-wrap" class="cshlg-popup">

    <div class="login_dialog">

        <a class="boxclose"></a>

        <form class="login_form" id="login_form" method="post" action="#">
            <h2>Please Login</h2>
            <input type="text" class="alert_status" readonly>
                            <label for="login_user"> Username</label>

            <input type="text" name="login_user" id="login_user" />
                            <label for="pass_user"> Password </label>

            <input type="password" name="pass_user" id="pass_user" />
            <label for="rememberme" id="lb_rememberme"><input name="rememberme" type="checkbox" id="rememberme" value="forever"  /> Remember Me</label>
            <input type="submit" name="login_submit" value="LOGIN" class="login_submit" />

            <div class="social-login">
                                                    <a class="login-btn-facebook" href="https://demo.themenovo.com/nanovi/wp-content/plugins/csh-login/inc/login-with-facebook"><i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a>


                            </div>
        </form>

        <form class="register_form" id="register_form"
            action="https://demo.themenovo.com/nanovi" method="post">
            <h2>Registration</h2>
            <input type="text" class="alert_status" readonly>
            <label for="register_user">Username</label>
            <input type="text" name="register_user" id="register_user" value="" >
            <label for="register_email">E-mail</label>
            <input type="email" name="register_email" id="register_email" value="">
            <div id="allow_pass">
                <label for="register_pass">Password</label>
                <input type="password" name="register_pass" id="register_pass" value="">
                <label for="confirm_pass">Confirm Password</label>
                <input type="password" name="confirm_pass" id="confirm_pass" value="">
            </div>
                        <input type="submit" name="register_submit" id="register_submit" value="REGISTER" />
        </form>

        <form class="lost_pwd_form" action="https://demo.themenovo.com/nanovi" method="post">
            <h2>Forgotten Password?</h2>
            <input type="text" class="alert_status" readonly>
            <label for="lost_pwd_user_email">Username or Email Adress</label>
            <input type="text" name="lost_pwd_user_email" id="lost_pwd_user_email">
            <input type="submit" name="lost_pwd_submit" id="lost_pwd_submit" value="GET NEW PASSWORD">
        </form>
                <div class="pass_and_register" id="pass_and_register">

            <a class="go_to_register_link" href="" style="">Register</a>
            <span style="color: black"> </span>
            <a class="go_to_lostpassword_link" href="">Forgot Password</a>
            <span style="color: black"></span>
            <a class="back_login" href="">Back to Login</a>

        </div>


    </div>
</div>

<script src='<?php echo base_url(); ?>assets/api.js'></script>
<link rel='stylesheet' id='js_composer_front-css'  href='<?php echo base_url(); ?>assets/css/js_composer.min.css'  media='all' />
<link rel='stylesheet' id='cshlg_layout_1-css'  href='<?php echo base_url(); ?>assets/css/layout1.css'  media='all' />
<script  src='<?php echo base_url(); ?>assets/js/cms-front.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/demo.themenovo.com\/nanovi\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script  src='<?php echo base_url(); ?>assets/js/scripts.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/select2.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/swh-post.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
//var search_obj = {"is_swh_archive":"true","ajax_url":"https:\/\/demo.themenovo.com\/nanovi\/wp-admin\/admin-ajax.php","room_page":"https:\/\/demo.themenovo.com\/nanovi\/room-style1\/","confirm_delete_order":"Are you sure to delete your Reservation?"};
/* ]]> */
</script>
<script  src='<?php echo base_url(); ?>assets/js/swh-search.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
//var nectarLove = {"ajaxurl":"https:\/\/demo.themenovo.com\/nanovi\/wp-admin\/admin-ajax.php","postID":"1213","rooturl":"https:\/\/demo.themenovo.com\/nanovi\/"};
/* ]]> */
</script>
<script  src='<?php echo base_url(); ?>assets/js/post_favorite.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/headroom.min.js'></script>
<script  src='<?php echo base_url(); ?>/assets/js/headroom.js'></script>
<script  src='<?php echo base_url(); ?>/assets/js/cms-parallax.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/bootstrap.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/match-height-min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/sidebar-scroll-fixed.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/magnific-popup.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/nice-select.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/jquery-ui.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/main.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var newsletter = {"messages":{"email_error":"Email address is not correct","name_error":"Name is required","surname_error":"Last name is required","privacy_error":"You must accept the privacy policy"},"profile_max":"20"};
/* ]]> */
</script>
<script  src='<?php echo base_url(); ?>assets/js/validate.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/wp-embed.min.js'></script>
<script  src='<?php echo base_url(); ?>assets/js/js_composer_front.min.js'></script>

</body>
</html>
