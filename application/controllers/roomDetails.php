<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class roomDetails extends CI_Controller {

	public function __construct() {
			parent::__construct();
			$this->load->model('RoomDetailModel');
			$this->load->model('RoomCategoryModel');
			$this->load->model('reservationModel');
	}

	public function index(){

		$this->load->view('roomDetails');

	}

	public function getdetails($id){

		$data                 = array();
		$data['details'] 		  = $this->RoomDetailModel->getCategoryDetails($id);
		$data['weekPrice'] 	  = $this->RoomDetailModel->getCategoryPriceDetails($id);
		$data['randomAll']	  = $this->RoomCategoryModel->listRandomData(6);
		$data['specialThree'] = $this->RoomCategoryModel->listRandomData(3);

		$this->load->view('roomDetails',$data);
	}

	public function redirectReservation(){
		$checkin 			= date('Y-m-d', strtotime($_POST['check-in']));
		$checkout 		= date('Y-m-d', strtotime($_POST['check-out']));
		$guests 			= $_POST['guests'];
		$nights 			= (date('d', strtotime($checkout)) - date('d', strtotime($checkin)));
		$categoryId 	= $_POST['categoryId'];

		$roomData = $this->reservationModel->getAvailableRooms($categoryId,$checkin,$checkout);

		if($roomData){
			$data = array();
			$data['resCheckin']  = $checkin;
			$data['resCheckout'] = $checkout;
			$data['guests'] 		 = $guests;
			$data['nights'] 		 = $nights;
			$data['catId'] 		 	 = $categoryId;

			$this->session->set_userdata('resData', $data);

			redirect('/reservation', 'refresh');
		} else {
			redirect('roomDetails/getdetails/'.$categoryId, 'refresh');
			exit();
		}

	}

}
