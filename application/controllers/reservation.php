<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class reservation extends CI_Controller {

	public function __construct() {
			parent::__construct();
			$this->load->model('RoomDetailModel');
			$this->load->model('RoomCategoryModel');
			$this->load->model('reservationModel');
	}

	public function index(){

		$dataSession   = $this->session->userdata('resData');
		$weekPrice     = $this->RoomDetailModel->getCategoryPriceDetails($dataSession['catId']);
		$details     	 = $this->RoomDetailModel->getCategoryDetails($dataSession['catId']);
		$extraServices = $this->RoomDetailModel->getCategoryExtraServices($dataSession['catId']);

		if(date('l', strtotime($dataSession['resCheckin'])) =='Monday'){
			$todayPrice = $weekPrice->mondayPrice;
		} else if(date('l', strtotime($dataSession['resCheckin'])) =='Tuesday'){
			$todayPrice = $weekPrice->tuesdayPrice;
		} else if(date('l', strtotime($dataSession['resCheckin'])) =='Wenesday'){
			$todayPrice = $weekPrice->wenesdayPrice;
		} else if(date('l', strtotime($dataSession['resCheckin'])) =='Thursday'){
			$todayPrice = $weekPrice->thursdayPrice;
		} else if(date('l', strtotime($dataSession['resCheckin'])) =='Friday'){
			$todayPrice = $weekPrice->fridayPrice;
		} else if(date('l', strtotime($dataSession['resCheckin'])) =='Saturday'){
			$todayPrice = $weekPrice->saturdayPrice;
		} else if(date('l', strtotime($dataSession['resCheckin'])) =='Sunday'){
			$todayPrice = $weekPrice->sundayPrice;
		} else{
			$todayPrice = $details->categoryBasePrice;
		}

		$this->session->set_userdata('dayPrice', $todayPrice);

		$data                  = array();
		$data['details'] 		   = $details;
		$data['todayPrice'] 	 = $todayPrice;
		$data['sessionData'] 	 = $dataSession;
		$data['extraServices'] = $extraServices;

		$this->load->view('reservation',$data);

	}
	public function booking(){

		$statusArray = array(
			'status' => "error",
			'message'=> "error"
		);

		if($_POST['action'] == 'swh_change_service_cookie'){
			$extServices = explode(",",$_POST['sv']);
			$this->session->set_userdata('extraServices', $extServices);
			print_r($this->session->userdata('extraServices'));
			exit();
		} else if($_POST['action'] == 'swh_checkout_handles'){

			$dataSession   = $this->session->userdata('resData');
			$dayPrice 		 = $this->session->userdata('dayPrice');
			$extraServices = $this->RoomDetailModel->getCategoryExtraServices($dataSession['catId']);
			$checkin 			 = $dataSession['resCheckin'];
			$checkout 		 = $dataSession['resCheckout'];

			$totalPrice = $dayPrice;
			foreach($extraServices as $extra){
				if (in_array($extra->ixCategoryExtraServices, $this->session->userdata('extraServices'))){
					$totalPrice = $totalPrice + $extra->servicePrice;
  			}
			}

			$name 		= $_POST['form_data']['_name'];
			$surname  = $_POST['form_data']['_surname'];
			$email  	= $_POST['form_data']['_email'];
			$address  = $_POST['form_data']['_address'];
			$phone  	= $_POST['form_data']['_phone'];
			$pincode  = $_POST['form_data']['_zipcode'];
			$message  = $_POST['form_data']['_msg'];
			$paymentType	= $_POST['form_data']['_payment'];

			
			$roomData = $this->reservationModel->getAvailableRooms($dataSession['catId'],$checkin,$checkout);
			if($roomData){
				$ixRoom 	 = $roomData->ixRoom;
			} else{
				$statusArray = array(
					'status' => "error",
					'message'=> "Rooms are not available in this date",
					'url'    => false
				);
				echo json_encode($statusArray);
				exit();
			}

			if($paymentType == 'offline'){
				$guestData = array(
								'name'      =>  $name,
								'surname'   =>  $surname,
								'email'   	=>  $email,
								'address' 	=>  $address,
								'phone'    	=>  $phone,
								'pincode'   =>  $pincode,
								'message'   =>  $message,
								'created'   =>  date("Y-m-d H:i:s"),
								'updated'   =>  date("Y-m-d H:i:s")
				 );

			 $ixGuestDetails = $this->reservationModel->addGuestDetails($guestData);
			 $reservationData = array(
							 'ixRoom'      					=>  $ixRoom,
							 'ixGuestDetails'   		=>  $ixGuestDetails,
							 'checkIn'   						=>  date('Y-m-d 0:0:0', strtotime($checkin)),
							 'checkOut' 						=>  date('Y-m-d 0:0:0', strtotime($checkout)),
							 'reservationStatus'    =>  'CONFIRMED',
							 'reservationCreated'   =>  date("Y-m-d H:i:s"),
							 'reservationUpdated'   =>  date("Y-m-d H:i:s")
				);
			 $ixReservation = $this->reservationModel->addReservationDetails($reservationData);

			 $paymentData = array(
							 'ixReservation'    =>  $ixReservation,
							 'ixGuestDetails'   =>  $ixGuestDetails,
							 'totalAmount'   		=>  $totalPrice,
							 'paymentType' 			=>  $paymentType,
							 'paymentStatus'    =>  'PENDING',
							 'paymentCreated'   =>  date("Y-m-d H:i:s"),
							 'paymentUpdated'   =>  date("Y-m-d H:i:s")
				);
			 $ixPaymentDetails = $this->reservationModel->addPaymentDetails($paymentData);

			 $extraSelected = $this->session->userdata('extraServices');
			 if($extraSelected){
				 foreach ($extraSelected as $value) {
					if($value!=""){
						$extraserviceData = array(
 									 'ixCategoryExtraServices' =>  $value,
 									 'ixReservation'   				 =>  $ixReservation,
 									 'status	'   						 =>  'CONFIRMED',
 									 'created' 								 =>  date("Y-m-d H:i:s"),
 									 'modified'    						 =>  date("Y-m-d H:i:s")
 						);
 						$ixExtraServiceReservation = $this->reservationModel->addExtraServiceReservation($extraserviceData);
					}
				 }
			 }

			}
			$statusArray = array(
				'status' => "success",
				'message'=> '<div class="swh-order-notification">
				  <div class="swh-noti-overlay"></div>
				  <div class="swh-order-noti-inner">
				    <i class="swh-noti-close zmdi zmdi-close"></i>
				    <h3 class="swh-noti-title">Order Successfully</h3>
				    <div class="swh-noti-contents">
				      <div>
				        <p>OrderID: SWH-2733</p>
				        <p>Name: SDCFV</p>
				        <p>Email: SDSFVD@sadf.com</p>
				        <p>Payment method: Offline payment</p>
				        <p>Please remember your Order ID, Your Username, Your Email. You will pay on Arrival. We will contact soon</p></div>
				      </div>
				    </div>
				  </div>',
				'url'    => false
			);
			echo json_encode($statusArray);
		}

	}


}
