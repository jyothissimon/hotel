<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class rooms extends CI_Controller {

	public function __construct() {
			parent::__construct();
			$this->load->model('RoomCategoryModel');
			$this->load->library('pagination');
	}
	public function index()
    {
				$checkin 	= "";
				$checkout = "";
				$this->session->set_userdata('checkin', $checkin);
				$this->session->set_userdata('checkout', $checkout);
				// init params
        $params = array();
				//$params['categories'] = $this->RoomCategoryModel->roomCategories();
				$params["random"] = $this->RoomCategoryModel->listRandomData(3);
        $limit_per_page = 4;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->RoomCategoryModel->get_total();

        if ($total_records > 0)
        {
            // get current page records
            $params["results"] = $this->RoomCategoryModel->get_current_page_records($limit_per_page, $start_index);

            $config['base_url'] = base_url() . '/index.php/rooms/';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;

						$config['base_url'] = base_url() . '/index.php/rooms/custom';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;

            // custom paging configuration
            $config['num_links'] = 2;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;

            $config['full_tag_open'] = '<div class="loop-pagination">';
            $config['full_tag_close'] = '</div>';

            $config['first_link'] = 'First Page';
            $config['first_tag_open'] = '<span class="page-numbers">';
            $config['first_tag_close'] = '</span>';

            $config['last_link'] = 'Last Page';
            $config['last_tag_open'] = '<span class="page-numbers ">';
            $config['last_tag_close'] = '</span>';

            $config['next_link'] = '<i class="fa fa-angle-right"></i>';
            $config['next_tag_open'] = '<span class="page-numbers">';
            $config['next_tag_close'] = '</span>';

            $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
            $config['prev_tag_open'] = '<span class="page-numbers">';
            $config['prev_tag_close'] = '</span>';

            $config['cur_tag_open'] = '<span class="page-numbers current">';
            $config['cur_tag_close'] = '</span>';

            $config['num_tag_open'] = '<span class="page-numbers">';
            $config['num_tag_close'] = '</span>';

            $this->pagination->initialize($config);

            // build paging links
            $params["links"] = $this->pagination->create_links();
        }

        $this->load->view('rooms', $params);
    }

    public function custom()
    {

        // init params
        $params = array();
				$params["random"] = $this->RoomCategoryModel->listRandomData(3);
        $limit_per_page = 4;
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        $total_records = $this->RoomCategoryModel->get_total();

        if ($total_records > 0)
        {
            // get current page records
            $params["results"] = $this->RoomCategoryModel->get_current_page_records($limit_per_page, $page*$limit_per_page);

            $config['base_url'] = base_url() . '/index.php/rooms/custom';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;

            // custom paging configuration
            $config['num_links'] = 2;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;

            $config['full_tag_open'] = '<div class="loop-pagination">';
            $config['full_tag_close'] = '</div>';

            $config['first_link'] = 'First Page';
            $config['first_tag_open'] = '<span class="page-numbers">';
            $config['first_tag_close'] = '</span>';

            $config['last_link'] = 'Last Page';
            $config['last_tag_open'] = '<span class="page-numbers ">';
            $config['last_tag_close'] = '</span>';

            $config['next_link'] = '<i class="fa fa-angle-right"></i>';
            $config['next_tag_open'] = '<span class="page-numbers">';
            $config['next_tag_close'] = '</span>';

            $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
            $config['prev_tag_open'] = '<span class="page-numbers">';
            $config['prev_tag_close'] = '</span>';

            $config['cur_tag_open'] = '<span class="page-numbers current">';
            $config['cur_tag_close'] = '</span>';

            $config['num_tag_open'] = '<span class="page-numbers current">';
            $config['num_tag_close'] = '</span>';

            $this->pagination->initialize($config);

            // build paging links
            $params["links"] = $this->pagination->create_links();
        }

        $this->load->view('rooms', $params);
    }
		public function availability()
		{
			$checkin 	= date('Y-m-d', strtotime($_GET['check-in']));
			$checkout = date('Y-m-d', strtotime($_GET['check-out']));
			$this->session->set_userdata('checkin', $checkin);
			$this->session->set_userdata('checkout', $checkout);
			$params = array();
			$params["random"] = $this->RoomCategoryModel->listRandomData(3);
			$limit_per_page = 4;
			$page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
			$total_records = $this->RoomCategoryModel->getCountAvailableCategories($checkin, $checkout);
			if ($total_records > 0)
			{
					// get current page records
					$params["results"] = $this->RoomCategoryModel->getAvailableCategories($checkin, $checkout, $limit_per_page, $page*$limit_per_page);

					$config['base_url'] = base_url() . '/index.php/rooms/availability';
					$config['total_rows'] = $total_records;
					$config['per_page'] = $limit_per_page;
					$config["uri_segment"] = 3;

					// custom paging configuration
					$config['num_links'] = 2;
					$config['use_page_numbers'] = TRUE;
					$config['reuse_query_string'] = TRUE;

					$config['full_tag_open'] = '<div class="loop-pagination">';
					$config['full_tag_close'] = '</div>';

					$config['first_link'] = 'First Page';
					$config['first_tag_open'] = '<span class="page-numbers">';
					$config['first_tag_close'] = '</span>';

					$config['last_link'] = 'Last Page';
					$config['last_tag_open'] = '<span class="page-numbers ">';
					$config['last_tag_close'] = '</span>';

					$config['next_link'] = '<i class="fa fa-angle-right"></i>';
					$config['next_tag_open'] = '<span class="page-numbers">';
					$config['next_tag_close'] = '</span>';

					$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
					$config['prev_tag_open'] = '<span class="page-numbers">';
					$config['prev_tag_close'] = '</span>';

					$config['cur_tag_open'] = '<span class="page-numbers current">';
					$config['cur_tag_close'] = '</span>';

					$config['num_tag_open'] = '<span class="page-numbers current">';
					$config['num_tag_close'] = '</span>';

					$this->pagination->initialize($config);

					// build paging links
					$params["links"] = $this->pagination->create_links();
			}

			$this->load->view('rooms', $params);
		}

}
