<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {

	public function __construct() {
			parent::__construct();
			$this->load->model('RoomCategoryModel');
	}
	public function index()
	{
		$data = array();
		$data['resultSet'] = $this->RoomCategoryModel->listRandomData(6);
		$data['special'] =  $this->RoomCategoryModel->listRandomData(3);
		$this->load->view('home',$data);
	}
}
