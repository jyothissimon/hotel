<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class RoomDetailModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getCategoryDetails($id){
      $this->db->where('ixRoomCategory', $id);
      $query = $this->db->get('roomcategory');
      return $query->row();
    }
    public function getCategoryPriceDetails($id){
      $this->db->where('ixRoomCategory', $id);
      $query = $this->db->get('roomprice');
      return $query->row();
    }
    public function getCategoryExtraServices($id){
      $query  = $this->db->query("SELECT cs.ixRoomCategory,cs.ixCategoryExtraServices,es.ixExtraServices,es.serviceName,es.servicePrice
                                  FROM categoryextraservices cs LEFT JOIN extraservices es ON es.ixExtraServices=cs.ixExtraServices
                                  WHERE cs.ixRoomCategory = ". $id ." AND cs.status='ACTIVE'");
      return $query->result();
    }

}
?>
