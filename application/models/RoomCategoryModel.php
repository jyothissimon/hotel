<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class RoomCategoryModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function listRandomData($limit){
        $query  = $this->db->query("SELECT * FROM roomcategory ORDER BY RAND() LIMIT ".$limit);
        return $query->result_array();
    }

    // Count all record of table "roomcategory" in database.
    public function record_count() {
        return $this->db->count_all("roomcategory");
    }

    public function get_current_page_records($limit, $start)
    {
        $this->db->limit($limit, $start);
        $query = $this->db->get("roomcategory");

        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }

    public function get_total(){
        return $this->db->count_all("roomcategory");
    }

    public function getCountAvailableCategories($checkin, $checkout) {
      $query  = $this->db->query("SELECT DISTINCT COUNT( DISTINCT rc.`ixRoomCategory`) as count
                                  FROM roomcategory rc LEFT JOIN room r ON r.ixRoomCategory = rc.ixRoomCategory
                                  WHERE r.ixRoom NOT IN (SELECT DISTINCT ixRoom FROM reservation WHERE DATE(checkIn) <= '$checkout' AND DATE(checkOut) >= '$checkin')");
      return $query->row()->count;
    }

    public function getAvailableCategories($checkin, $checkout, $limit, $start) {
      $query  = $this->db->query("SELECT DISTINCT rc.`ixRoomCategory`,rc.`categoryName`,rc.`categoryDescription`,rc.`categoryImage`,rc.`categoryBasePrice`,rc.`categoryServices`
                                  FROM roomcategory rc LEFT JOIN room r ON r.ixRoomCategory = rc.ixRoomCategory
                                  WHERE r.ixRoom NOT IN (SELECT DISTINCT ixRoom FROM reservation WHERE DATE(checkIn) <= '$checkout' AND DATE(checkOut) >= '$checkin')
                                  LIMIT ". $start . ",". $limit);

      if ($query->num_rows() > 0){
        foreach ($query->result() as $row){
          $data[] = $row;
        }
        return $data;
      }
      return false;

    }

    public function roomCategories(){
      $parms = array();
      $query = $this->db->get("roomcategory");
      foreach ($query->result() as $row){
        $data[] = $row;
      }
      return $data;
    }

}
?>
