<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class reservationModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function addGuestDetails($data){
       //$this->db->trans_start();
       $this->db->insert('guestdetails', $data);
       //$this->db->trans_complete();
       return $this->db->insert_id();
    }

    public function getAvailableRooms($categoryId,$checkin,$checkout)
    {
      $query  = $this->db->query("SELECT r.ixRoom,r.`roomNumber`,r.`roomStatus` FROM room r
                                    WHERE r.ixRoom NOT IN (SELECT DISTINCT ixRoom
                                    FROM reservation
                                    WHERE DATE(checkIn) <= '$checkout' AND DATE(checkOut) >= '$checkin')
                                    AND r.`ixRoomCategory` = '$categoryId' LIMIT 1 ");

        if ($query->num_rows() > 0){
          return $query->row();
        }
        return false;
    }

    public function addReservationDetails($data){
       //$this->db->trans_start();
       $this->db->insert('reservation', $data);
       //$this->db->trans_complete();
       return $this->db->insert_id();
    }

    public function addPaymentDetails($data){
       //$this->db->trans_start();
       $this->db->insert('paymentdetails', $data);
       //$this->db->trans_complete();
       return $this->db->insert_id();
    }

    public function addExtraServiceReservation($data){
       $this->db->trans_start();
       $this->db->insert('extraservicereservation', $data);
       $this->db->trans_complete();
       return $this->db->insert_id();
    }

}
?>
