-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2019 at 12:04 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tripenta`
--

-- --------------------------------------------------------

--
-- Table structure for table `categoryextraservices`
--

CREATE TABLE `categoryextraservices` (
  `ixCategoryExtraServices` bigint(20) NOT NULL,
  `ixRoomCategory` bigint(20) NOT NULL,
  `ixExtraServices` bigint(20) NOT NULL,
  `status` enum('ACTIVE','DISABLED') NOT NULL DEFAULT 'ACTIVE',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categoryextraservices`
--

INSERT INTO `categoryextraservices` (`ixCategoryExtraServices`, `ixRoomCategory`, `ixExtraServices`, `status`, `created`, `updated`) VALUES
(1, 1, 1, 'ACTIVE', '2019-02-04 15:54:23', '2019-02-04 15:54:23'),
(2, 1, 2, 'ACTIVE', '2019-02-04 15:54:23', '2019-02-04 15:54:23'),
(3, 1, 3, 'ACTIVE', '2019-02-04 15:54:36', '2019-02-04 15:54:36'),
(4, 1, 4, 'ACTIVE', '2019-02-04 15:54:36', '2019-02-04 15:54:36'),
(5, 2, 1, 'ACTIVE', '2019-02-04 15:54:56', '2019-02-04 15:54:56'),
(6, 2, 2, 'ACTIVE', '2019-02-04 15:54:56', '2019-02-04 15:54:56'),
(7, 3, 1, 'ACTIVE', '2019-02-04 15:55:31', '2019-02-04 15:55:31'),
(8, 3, 2, 'ACTIVE', '2019-02-04 15:55:31', '2019-02-04 15:55:31'),
(9, 3, 3, 'ACTIVE', '2019-02-04 15:55:57', '2019-02-04 15:55:57'),
(10, 3, 4, 'ACTIVE', '2019-02-04 15:55:57', '2019-02-04 15:55:57'),
(11, 4, 1, 'ACTIVE', '2019-02-04 15:56:41', '2019-02-04 15:56:41'),
(12, 4, 2, 'ACTIVE', '2019-02-04 15:56:41', '2019-02-04 15:56:41'),
(13, 4, 3, 'ACTIVE', '2019-02-04 15:56:42', '2019-02-04 15:56:42'),
(14, 4, 4, 'ACTIVE', '2019-02-04 15:56:42', '2019-02-04 15:56:42'),
(15, 5, 1, 'ACTIVE', '2019-02-04 15:57:35', '2019-02-04 15:57:35'),
(16, 5, 2, 'ACTIVE', '2019-02-04 15:57:35', '2019-02-04 15:57:35'),
(17, 5, 3, 'ACTIVE', '2019-02-04 15:57:35', '2019-02-04 15:57:35'),
(18, 5, 4, 'ACTIVE', '2019-02-04 15:57:35', '2019-02-04 15:57:35'),
(19, 6, 1, 'ACTIVE', '2019-02-04 15:57:36', '2019-02-04 15:57:36'),
(20, 6, 2, 'ACTIVE', '2019-02-04 15:57:36', '2019-02-04 15:57:36'),
(21, 6, 3, 'ACTIVE', '2019-02-04 15:57:36', '2019-02-04 15:57:36'),
(22, 6, 4, 'ACTIVE', '2019-02-04 15:57:36', '2019-02-04 15:57:36');

-- --------------------------------------------------------

--
-- Table structure for table `extraservicereservation`
--

CREATE TABLE `extraservicereservation` (
  `ixExtraServiceReservation` bigint(20) NOT NULL,
  `ixCategoryExtraServices` int(11) NOT NULL,
  `ixReservation` int(11) NOT NULL,
  `status` enum('CONFIRMED','PENDING') NOT NULL DEFAULT 'PENDING',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extraservicereservation`
--

INSERT INTO `extraservicereservation` (`ixExtraServiceReservation`, `ixCategoryExtraServices`, `ixReservation`, `status`, `created`, `modified`) VALUES
(1, 1, 0, 'CONFIRMED', '2019-02-09 05:18:52', '2019-02-09 05:18:52'),
(2, 2, 0, 'CONFIRMED', '2019-02-09 05:18:52', '2019-02-09 05:18:52'),
(10, 15, 6, 'CONFIRMED', '2019-02-09 05:58:58', '2019-02-09 05:58:58'),
(11, 16, 6, 'CONFIRMED', '2019-02-09 05:58:58', '2019-02-09 05:58:58'),
(12, 17, 6, 'CONFIRMED', '2019-02-09 05:58:59', '2019-02-09 05:58:59'),
(13, 0, 7, 'CONFIRMED', '2019-02-09 06:02:37', '2019-02-09 06:02:37'),
(14, 11, 9, 'CONFIRMED', '2019-02-09 07:35:16', '2019-02-09 07:35:16'),
(15, 12, 9, 'CONFIRMED', '2019-02-09 07:35:16', '2019-02-09 07:35:16'),
(16, 4, 10, 'CONFIRMED', '2019-02-09 07:38:06', '2019-02-09 07:38:06'),
(17, 4, 11, 'CONFIRMED', '2019-02-09 07:38:19', '2019-02-09 07:38:19'),
(18, 20, 12, 'CONFIRMED', '2019-02-09 07:45:03', '2019-02-09 07:45:03'),
(19, 7, 13, 'CONFIRMED', '2019-02-09 08:06:40', '2019-02-09 08:06:40'),
(20, 8, 13, 'CONFIRMED', '2019-02-09 08:06:40', '2019-02-09 08:06:40'),
(21, 5, 14, 'CONFIRMED', '2019-02-09 08:08:21', '2019-02-09 08:08:21'),
(22, 5, 15, 'CONFIRMED', '2019-02-09 08:08:42', '2019-02-09 08:08:42'),
(23, 11, 16, 'CONFIRMED', '2019-02-09 08:10:13', '2019-02-09 08:10:13'),
(24, 5, 17, 'CONFIRMED', '2019-02-09 08:14:50', '2019-02-09 08:14:50'),
(25, 1, 18, 'CONFIRMED', '2019-02-09 08:23:16', '2019-02-09 08:23:16'),
(26, 19, 19, 'CONFIRMED', '2019-02-09 08:37:49', '2019-02-09 08:37:49'),
(27, 1, 20, 'CONFIRMED', '2019-02-09 08:39:42', '2019-02-09 08:39:42'),
(28, 15, 21, 'CONFIRMED', '2019-02-09 08:52:48', '2019-02-09 08:52:48'),
(29, 16, 21, 'CONFIRMED', '2019-02-09 08:52:48', '2019-02-09 08:52:48'),
(30, 1, 22, 'CONFIRMED', '2019-02-09 12:02:15', '2019-02-09 12:02:15'),
(31, 2, 22, 'CONFIRMED', '2019-02-09 12:02:15', '2019-02-09 12:02:15');

-- --------------------------------------------------------

--
-- Table structure for table `extraservices`
--

CREATE TABLE `extraservices` (
  `ixExtraServices` bigint(20) NOT NULL,
  `serviceName` varchar(255) NOT NULL,
  `servicePrice` decimal(10,2) NOT NULL,
  `status` enum('ACTIVE','DISABLED') NOT NULL DEFAULT 'ACTIVE',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `extraservices`
--

INSERT INTO `extraservices` (`ixExtraServices`, `serviceName`, `servicePrice`, `status`, `created`, `updated`) VALUES
(1, 'Breakfast ', '100.00', 'ACTIVE', '2019-02-04 15:29:51', '2019-02-04 15:29:51'),
(2, 'Laundry', '100.00', 'ACTIVE', '2019-02-04 15:29:51', '2019-02-04 15:29:51'),
(3, 'Massage ', '500.00', 'ACTIVE', '2019-02-04 15:31:04', '2019-02-04 15:31:04'),
(4, 'Wifi ', '500.00', 'ACTIVE', '2019-02-04 15:31:04', '2019-02-04 15:31:04');

-- --------------------------------------------------------

--
-- Table structure for table `guestdetails`
--

CREATE TABLE `guestdetails` (
  `ixGuestDetails` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `pincode` varchar(11) NOT NULL,
  `message` varchar(500) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `guestdetails`
--

INSERT INTO `guestdetails` (`ixGuestDetails`, `name`, `surname`, `email`, `address`, `phone`, `pincode`, `message`, `created`, `updated`) VALUES
(1, 'Jyothis', 'Simon', 'jyothis2191@gmail.com', 'Kozhikode', '9605617676', '673603', NULL, '2019-02-06 09:53:48', '2019-02-06 09:53:48'),
(2, 'Rakesh', 'Narayanan', 'rakesh@wrsolutions.in', 'Kozhikode', '9745290246', '673016', NULL, '2019-02-06 09:53:48', '2019-02-06 09:53:48'),
(3, 'asdf', 'asdf', 'sdsfd@sdf.com', 'scdfv', 'asd', '0', '', '2019-02-09 05:18:52', '2019-02-09 05:18:52'),
(4, 'Jomon', 'Simon', 'jomon@gmail.com', 'kozhikode', '9605617676', '673603', 'hiii', '2019-02-09 05:23:04', '2019-02-09 05:23:04'),
(5, 'akhil', 'sdf', 'akhil@gmail.com', 'sadfg', '9605617676', '673603', 'sdfghj', '2019-02-09 05:30:57', '2019-02-09 05:30:57'),
(6, 'akhil one', 'sxcdvf', 'vvg@nnn.com', 'scdsfvdg', '9605617676', '673603', 'ssdfghn', '2019-02-09 05:32:03', '2019-02-09 05:32:03'),
(7, 'sdf', 'cvbg', 'sdfg@df.com', 'scdvfgf', 'assddf', 'dfg@gm.com', 'sdfg', '2019-02-09 05:34:37', '2019-02-09 05:34:37'),
(8, 'sdf', 'cvbg', 'sdfg@df.com', 'scdvfgf', 'assddf', 'dfg@gm.com', 'sdfg', '2019-02-09 05:36:16', '2019-02-09 05:36:16'),
(9, 'sdf', 'cvbg', 'sdfg@df.com', 'scdvfgf', 'assddf', 'dfg@gm.com', 'sdfg', '2019-02-09 05:37:56', '2019-02-09 05:37:56'),
(10, 'sdf', 'cvbg', 'sdfg@df.com', 'scdvfgf', 'assddf', 'dfg@gm.com', 'sdfg', '2019-02-09 05:38:26', '2019-02-09 05:38:26'),
(11, 'sdf', 'cvbg', 'sdfg@df.com', 'scdvfgf', 'assddf', 'dfg@gm.com', 'sdfg', '2019-02-09 05:39:50', '2019-02-09 05:39:50'),
(12, 'sdf', 'cvbg', 'sdfg@df.com', 'scdvfgf', 'assddf', 'dfg@gm.com', 'sdfg', '2019-02-09 05:40:11', '2019-02-09 05:40:11'),
(13, 'asd', 'sdcf', 'sd@gmail.com', 'wdfv', '9605617676', '673603', 'sadvg', '2019-02-09 05:41:11', '2019-02-09 05:41:11'),
(14, 'Akhil ', 'P', 'akhilpa@gmail.com', 'Vadakara', '9447452250', '673016', 'nil', '2019-02-09 05:58:58', '2019-02-09 05:58:58'),
(15, 'new', 'aaa', 'aa@gmail.com', 'sd', '9605617676', 'dfv', 'dcfv', '2019-02-09 06:02:37', '2019-02-09 06:02:37'),
(16, 'zasdfd', 'dfvgbfh', 'efgbhn@gmail.com', 'edfg', '566262+3+', '673603', 'werf', '2019-02-09 06:04:46', '2019-02-09 06:04:46'),
(17, 'zasdfd', 'dfvgbfh', 'efgbhn@gmail.com', 'edfg', '566262+3+', '673603', 'werf', '2019-02-09 06:06:22', '2019-02-09 06:06:22'),
(18, 'ZAdfgthy', 'fghjkl', 'fdghjk@gmail.cm', 'SDRFGH', 'ASDF', 'SD', 'DFGHJ', '2019-02-09 06:08:07', '2019-02-09 06:08:07'),
(19, 'hi', 'sysytem', 'ias@gmail.com', 'ssdf', '9605617676', 'dscfv', 'sdxcv', '2019-02-09 07:32:54', '2019-02-09 07:32:54'),
(20, 'wedf', 'dfv', 'frgt@gmail.com', 'sdfg', 'axscdf', 'dfrg@gmail.', 'swdefr', '2019-02-09 07:35:16', '2019-02-09 07:35:16'),
(21, 'wedf', 'dfv', 'frgt@gmail.com', 'sdfg', 'axscdf', 'dfrg@gmail.', 'swdefr', '2019-02-09 07:36:58', '2019-02-09 07:36:58'),
(22, 'ZXcxfv', 'asdfg', 'sdefg@gmail.com', 'wdefr', 'sdfg', 'efrgth', 'dwefrg', '2019-02-09 07:38:06', '2019-02-09 07:38:06'),
(23, 'ZXcxfv', 'asdfg', 'sdefg@gmail.com', 'wdefr', 'sdfg', 'efrgth', 'dwefrg', '2019-02-09 07:38:18', '2019-02-09 07:38:18'),
(24, 'ZXcxfv', 'asdfg', 'sdefg@gmail.com', 'wdefr', 'sdfg', 'efrgth', 'dwefrg', '2019-02-09 07:39:09', '2019-02-09 07:39:09'),
(25, 'asdf', 'dvfgbn@', 'dvfgbn@gmail.com', 'dfvg', 'wddf', 'fv', 'dfv', '2019-02-09 07:39:48', '2019-02-09 07:39:48'),
(26, 'asdf', 'dvfgbn@', 'dvfgbn@gmail.com', 'dfvg', 'wddf', 'fv', 'dfv', '2019-02-09 07:40:54', '2019-02-09 07:40:54'),
(27, 'axscd', 'scdvf', 'scdfvgb@gmail.com', 'dsdcvf', 'sdfv', 'dcfv', 'swdf', '2019-02-09 07:45:03', '2019-02-09 07:45:03'),
(28, 'axscd', 'scdvf', 'scdfvgb@gmail.com', 'dsdcvf', 'sdfv', 'dcfv', 'swdf', '2019-02-09 07:45:21', '2019-02-09 07:45:21'),
(29, 'axscd', 'scdvf', 'scdfvgb@gmail.com', 'dsdcvf', 'sdfv', 'dcfv', 'swdf', '2019-02-09 07:47:16', '2019-02-09 07:47:16'),
(30, 'axscd', 'scdvf', 'scdfvgb@gmail.com', 'dsdcvf', 'sdfv', 'dcfv', 'swdf', '2019-02-09 07:47:28', '2019-02-09 07:47:28'),
(31, 'axscd', 'scdvf', 'scdfvgb@gmail.com', 'dsdcvf', 'sdfv', 'dcfv', 'swdf', '2019-02-09 07:47:32', '2019-02-09 07:47:32'),
(32, 'axscd', 'scdvf', 'scdfvgb@gmail.com', 'dsdcvf', 'sdfv', 'dcfv', 'swdf', '2019-02-09 07:49:11', '2019-02-09 07:49:11'),
(33, 'axssfv', 'dcfgdb', 'dfvgb@gmail.com', 'derf', 'swdcfv', 'defrgb', 'defg', '2019-02-09 07:49:40', '2019-02-09 07:49:40'),
(34, 'axssfv', 'dcfgdb', 'dfvgb@gmail.com', 'derf', 'swdcfv', 'defrgb', 'defg', '2019-02-09 07:54:10', '2019-02-09 07:54:10'),
(35, 'axssfv', 'dcfgdb', 'dfvgb@gmail.com', 'derf', 'swdcfv', 'defrgb', 'defg', '2019-02-09 07:54:15', '2019-02-09 07:54:15'),
(36, 'xascvd', 'scfgb', 'sdfv@gmail.com', 'dcfvrg', 'dwsfv', 'frgt', 'fgb', '2019-02-09 07:55:21', '2019-02-09 07:55:21'),
(37, 'xascvd', 'scfgb', 'sdfv@gmail.com', 'dcfvrg', 'dwsfv', 'frgt', 'fgb', '2019-02-09 07:56:43', '2019-02-09 07:56:43'),
(38, 'xascvd', 'scfgb', 'sdfv@gmail.com', 'dcfvrg', 'dwsfv', 'frgt', 'fgb', '2019-02-09 08:00:29', '2019-02-09 08:00:29'),
(39, 'axscdv', 'xscdfv', 'cdvgb@gmail.com', 'cfvbg', 'sqwdef', 'fg', 'fvgb', '2019-02-09 08:00:59', '2019-02-09 08:00:59'),
(40, 'asdc', 'xscdfv', 'scdfv@gmail.com', 'cv ', 'cdfv', 'dsfvd', 'sadfv', '2019-02-09 08:06:40', '2019-02-09 08:06:40'),
(41, 'asdc', 'xscdfv', 'scdfv@gmail.com', 'cv ', 'cdfv', 'dsfvd', 'sadfv', '2019-02-09 08:07:23', '2019-02-09 08:07:23'),
(42, 'axscd', 'sdfvd', 'dfd@gmail.com', 'sdf', 'asdf', 'dfgb', 'sdfd', '2019-02-09 08:08:21', '2019-02-09 08:08:21'),
(43, 'axscd', 'sdfvd', 'dfd@gmail.com', 'sdf', 'asdf', 'dfgb', 'sdfd', '2019-02-09 08:08:42', '2019-02-09 08:08:42'),
(44, 'axscdfvsdfvg', 'b b', 'dvbn@mail.com', 'sdfgvfb', 'scdf', 'sdfgb', 'scfv', '2019-02-09 08:10:12', '2019-02-09 08:10:12'),
(45, 'xascdf', 'scdfv', 'cdfv@gmail.com', 'dcfv', 'scdfv', 'dfg', 'dfv', '2019-02-09 08:14:00', '2019-02-09 08:14:00'),
(46, 'axscd', 'xscfv', 'scfv@gmail.com', 'sdcfvg', 'wcdfd', 'dscdv', 'scdfv', '2019-02-09 08:14:50', '2019-02-09 08:14:50'),
(47, 'asdcfv', 'dcfv', 'dcfv@hmail.com', 'dcfv', 'wdcfv', 'dcfv', 'dfv', '2019-02-09 08:23:16', '2019-02-09 08:23:16'),
(48, 'ASD', 'SADEF', 'SQWDERF@gmail.com', 'WDEF', 'WDER', 'DEFR', 'sdfg', '2019-02-09 08:37:49', '2019-02-09 08:37:49'),
(49, 'ZAXsa', 'ASD', 'ASD@GMAIL.COM', 'EDRF', 'DSF', 'WDEFR', 'DEFRG', '2019-02-09 08:39:42', '2019-02-09 08:39:42'),
(50, 'ASD', 'SDC', 'SDCFV@GAmail.com', 'SDFV', 'ASXCD', 'DFVG', 'DF', '2019-02-09 08:52:48', '2019-02-09 08:52:48'),
(51, 'ASD', 'SDC', 'SDCFV@GAmail.com', 'SDFV', 'ASXCD', 'DFVG', 'DF', '2019-02-09 08:57:49', '2019-02-09 08:57:49'),
(52, 'ASD', 'SDC', 'SDCFV@GAmail.com', 'SDFV', 'ASXCD', 'DFVG', 'DF', '2019-02-09 08:57:52', '2019-02-09 08:57:52'),
(53, 'ASD', 'SDC', 'SDCFV@GAmail.com', 'SDFV', 'ASXCD', 'DFVG', 'DF', '2019-02-09 08:57:55', '2019-02-09 08:57:55'),
(54, 'ASD', 'SDC', 'SDCFV@GAmail.com', 'SDFV', 'ASXCD', 'DFVG', 'DF', '2019-02-09 08:57:57', '2019-02-09 08:57:57'),
(55, 'ASD', 'SDC', 'SDCFV@GAmail.com', 'SDFV', 'ASXCD', 'DFVG', 'DF', '2019-02-09 08:57:59', '2019-02-09 08:57:59'),
(56, 'ASD', 'SDC', 'SDCFV@GAmail.com', 'SDFV', 'ASXCD', 'DFVG', 'DF', '2019-02-09 08:58:30', '2019-02-09 08:58:30'),
(57, 'zAxs', 'sdfgh', 'dfgthy@gmail.com', 'gh', 'fg', 'gh', 'swdef', '2019-02-09 12:02:14', '2019-02-09 12:02:14');

-- --------------------------------------------------------

--
-- Table structure for table `guestextraservices`
--

CREATE TABLE `guestextraservices` (
  `ixGuestExtraServices` bigint(20) NOT NULL,
  `ixGuestDetails` bigint(20) NOT NULL,
  `ixExtraServices` bigint(20) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `paymentdetails`
--

CREATE TABLE `paymentdetails` (
  `ixPaymentDetails` bigint(20) NOT NULL,
  `ixReservation` bigint(20) NOT NULL,
  `ixGuestDetails` bigint(20) NOT NULL,
  `totalAmount` decimal(10,2) NOT NULL,
  `paymentType` enum('ONLINE','OFFLINE') NOT NULL,
  `paymentStatus` enum('PAID','PENDING','FAILED') NOT NULL DEFAULT 'PENDING',
  `paymentCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `paymentUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paymentdetails`
--

INSERT INTO `paymentdetails` (`ixPaymentDetails`, `ixReservation`, `ixGuestDetails`, `totalAmount`, `paymentType`, `paymentStatus`, `paymentCreated`, `paymentUpdated`) VALUES
(1, 1, 1, '1000.00', 'OFFLINE', 'PAID', '2019-02-06 09:55:25', '2019-02-06 09:55:25'),
(2, 2, 2, '1000.00', 'OFFLINE', 'PAID', '2019-02-06 09:55:25', '2019-02-06 09:55:25'),
(3, 6, 14, '750.00', 'OFFLINE', 'PENDING', '2019-02-09 05:58:58', '2019-02-09 05:58:58'),
(4, 7, 15, '1750.00', 'OFFLINE', 'PENDING', '2019-02-09 06:02:37', '2019-02-09 06:02:37'),
(5, 8, 18, '1800.00', 'OFFLINE', 'PENDING', '2019-02-09 06:08:07', '2019-02-09 06:08:07'),
(6, 9, 20, '1450.00', 'OFFLINE', 'PENDING', '2019-02-09 07:35:16', '2019-02-09 07:35:16'),
(7, 10, 22, '1500.00', 'OFFLINE', 'PENDING', '2019-02-09 07:38:06', '2019-02-09 07:38:06'),
(8, 11, 23, '1500.00', 'OFFLINE', 'PENDING', '2019-02-09 07:38:19', '2019-02-09 07:38:19'),
(9, 12, 27, '2300.00', 'OFFLINE', 'PENDING', '2019-02-09 07:45:03', '2019-02-09 07:45:03'),
(10, 13, 40, '1950.00', 'OFFLINE', 'PENDING', '2019-02-09 08:06:40', '2019-02-09 08:06:40'),
(11, 14, 42, '1100.00', 'OFFLINE', 'PENDING', '2019-02-09 08:08:21', '2019-02-09 08:08:21'),
(12, 15, 43, '1100.00', 'OFFLINE', 'PENDING', '2019-02-09 08:08:42', '2019-02-09 08:08:42'),
(13, 16, 44, '1400.00', 'OFFLINE', 'PENDING', '2019-02-09 08:10:13', '2019-02-09 08:10:13'),
(14, 17, 46, '850.00', 'OFFLINE', 'PENDING', '2019-02-09 08:14:50', '2019-02-09 08:14:50'),
(15, 18, 47, '1300.00', 'OFFLINE', 'PENDING', '2019-02-09 08:23:16', '2019-02-09 08:23:16'),
(16, 19, 48, '2100.00', 'OFFLINE', 'PENDING', '2019-02-09 08:37:49', '2019-02-09 08:37:49'),
(17, 20, 49, '1300.00', 'OFFLINE', 'PENDING', '2019-02-09 08:39:42', '2019-02-09 08:39:42'),
(18, 21, 50, '1950.00', 'OFFLINE', 'PENDING', '2019-02-09 08:52:48', '2019-02-09 08:52:48'),
(19, 22, 57, '1200.00', 'OFFLINE', 'PENDING', '2019-02-09 12:02:15', '2019-02-09 12:02:15');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `ixReservation` bigint(20) NOT NULL,
  `ixRoom` bigint(20) NOT NULL,
  `ixGuestDetails` bigint(20) NOT NULL,
  `checkIn` datetime NOT NULL,
  `checkOut` datetime NOT NULL,
  `reservationStatus` enum('CONFIRMED','PENDING') NOT NULL DEFAULT 'PENDING',
  `reservationCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reservationUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`ixReservation`, `ixRoom`, `ixGuestDetails`, `checkIn`, `checkOut`, `reservationStatus`, `reservationCreated`, `reservationUpdated`) VALUES
(1, 1, 1, '2019-02-06 00:00:00', '2019-02-08 00:00:00', 'CONFIRMED', '2019-02-06 09:51:20', '2019-02-06 09:51:20'),
(2, 2, 2, '2019-02-06 00:00:00', '2019-02-07 00:00:00', 'CONFIRMED', '2019-02-06 09:51:20', '2019-02-06 09:51:20'),
(6, 3, 14, '2019-02-14 00:00:00', '2019-02-15 00:00:00', 'CONFIRMED', '2019-02-09 05:58:58', '2019-02-09 05:58:58'),
(7, 7, 15, '2019-02-21 00:00:00', '2019-02-22 00:00:00', 'CONFIRMED', '2019-02-09 06:02:37', '2019-02-09 06:02:37'),
(8, 7, 18, '2019-02-09 00:00:00', '2019-02-10 00:00:00', 'CONFIRMED', '2019-02-09 06:08:07', '2019-02-09 06:08:07'),
(9, 6, 20, '2019-02-24 00:00:00', '2019-02-27 00:00:00', 'CONFIRMED', '2019-02-09 07:35:16', '2019-02-09 07:35:16'),
(10, 1, 22, '2019-02-26 00:00:00', '2019-02-27 00:00:00', 'CONFIRMED', '2019-02-09 07:38:06', '2019-02-09 07:38:06'),
(11, 2, 23, '2019-02-26 00:00:00', '2019-02-27 00:00:00', 'CONFIRMED', '2019-02-09 07:38:19', '2019-02-09 07:38:19'),
(12, 8, 27, '2019-02-09 00:00:00', '2019-02-10 00:00:00', 'CONFIRMED', '2019-02-09 07:45:03', '2019-02-09 07:45:03'),
(13, 5, 40, '2019-02-09 00:00:00', '2019-02-10 00:00:00', 'CONFIRMED', '2019-02-09 08:06:40', '2019-02-09 08:06:40'),
(14, 3, 42, '2019-02-09 00:00:00', '2019-02-10 00:00:00', 'CONFIRMED', '2019-02-09 08:08:21', '2019-02-09 08:08:21'),
(15, 4, 43, '2019-02-09 00:00:00', '2019-02-10 00:00:00', 'CONFIRMED', '2019-02-09 08:08:42', '2019-02-09 08:08:42'),
(16, 6, 44, '2019-02-09 00:00:00', '2019-02-10 00:00:00', 'CONFIRMED', '2019-02-09 08:10:13', '2019-02-09 08:10:13'),
(17, 3, 46, '2019-02-26 00:00:00', '2019-02-28 00:00:00', 'CONFIRMED', '2019-02-09 08:14:50', '2019-02-09 08:14:50'),
(18, 1, 47, '2019-02-09 00:00:00', '2019-02-10 00:00:00', 'CONFIRMED', '2019-02-09 08:23:16', '2019-02-09 08:23:16'),
(19, 8, 48, '2019-02-20 00:00:00', '2019-02-27 00:00:00', 'CONFIRMED', '2019-02-09 08:37:49', '2019-02-09 08:37:49'),
(20, 2, 49, '2019-02-09 00:00:00', '2019-02-10 00:00:00', 'CONFIRMED', '2019-02-09 08:39:42', '2019-02-09 08:39:42'),
(21, 7, 50, '2019-02-27 00:00:00', '2019-02-28 00:00:00', 'CONFIRMED', '2019-02-09 08:52:48', '2019-02-09 08:52:48'),
(22, 1, 57, '2019-02-19 00:00:00', '2019-02-22 00:00:00', 'CONFIRMED', '2019-02-09 12:02:15', '2019-02-09 12:02:15');

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `ixRoom` bigint(20) NOT NULL,
  `roomNumber` varchar(255) DEFAULT NULL,
  `ixRoomCategory` bigint(20) NOT NULL,
  `roomStatus` enum('ACTIVE','DISABLED') NOT NULL DEFAULT 'ACTIVE',
  `roomCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `roomUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`ixRoom`, `roomNumber`, `ixRoomCategory`, `roomStatus`, `roomCreated`, `roomUpdated`) VALUES
(1, '101', 1, 'ACTIVE', '2019-02-04 15:52:08', '2019-02-04 15:52:08'),
(2, '102', 1, 'ACTIVE', '2019-02-04 15:52:08', '2019-02-04 15:52:08'),
(3, '103', 2, 'ACTIVE', '2019-02-04 15:52:17', '2019-02-04 15:52:17'),
(4, '104', 2, 'ACTIVE', '2019-02-04 15:52:17', '2019-02-04 15:52:17'),
(5, '105', 3, 'ACTIVE', '2019-02-04 15:52:29', '2019-02-04 15:52:29'),
(6, '106', 4, 'ACTIVE', '2019-02-04 15:52:29', '2019-02-04 15:52:29'),
(7, '107', 5, 'ACTIVE', '2019-02-04 15:52:40', '2019-02-04 15:52:40'),
(8, '108', 6, 'ACTIVE', '2019-02-04 15:52:40', '2019-02-04 15:52:40');

-- --------------------------------------------------------

--
-- Table structure for table `roomcategory`
--

CREATE TABLE `roomcategory` (
  `ixRoomCategory` bigint(20) NOT NULL,
  `categoryName` varchar(255) NOT NULL,
  `categoryDescription` text,
  `categoryImage` varchar(255) DEFAULT NULL,
  `categoryBasePrice` int(11) DEFAULT NULL,
  `categoryServices` text,
  `guestNumber` int(255) NOT NULL DEFAULT '1',
  `bedNumber` int(11) NOT NULL DEFAULT '1',
  `categoryCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `categoryUpdated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `categoryStatus` enum('ACTIVE','DISABLED') NOT NULL DEFAULT 'ACTIVE'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roomcategory`
--

INSERT INTO `roomcategory` (`ixRoomCategory`, `categoryName`, `categoryDescription`, `categoryImage`, `categoryBasePrice`, `categoryServices`, `guestNumber`, `bedNumber`, `categoryCreated`, `categoryUpdated`, `categoryStatus`) VALUES
(1, 'Single Room AC', 'Rooms with One bed,These type can occupy 2 Guests.It provide airconditioning throuhtout the stay.', NULL, 1000, '{\r\n \"services\": [\r\n   {\r\n     \"name\": \"Air Conditioning\",\r\n     \"icon\": \"air_conditioning.png\"\r\n   },\r\n   {\r\n     \"name\": \"Daily Cleaning\",\r\n     \"icon\": \"room_cleaning.png\"\r\n   },\r\n   {\r\n     \"name\": \"Free Parking\",\r\n     \"icon\": \"car_rentel.png\"\r\n   },\r\n   {\r\n     \"name\": \"Television\",\r\n     \"icon\": \"tv_provide.png\"\r\n   }\r\n ]\r\n}', 2, 1, '2019-02-04 15:38:51', '2019-02-04 15:38:51', 'ACTIVE'),
(2, 'Single Room NON-AC', 'Rooms with One bed,These type can occupy 2 Guests.It not provide airconditioning. ', NULL, 750, '{\r\n \"services\": [\r\n   {\r\n     \"name\": \"Daily Cleaning\",\r\n     \"icon\": \"room_cleaning.png\"\r\n   },\r\n   {\r\n     \"name\": \"Free Parking\",\r\n     \"icon\": \"car_rentel.png\"\r\n   },\r\n   {\r\n     \"name\": \"Television\",\r\n     \"icon\": \"tv_provide.png\"\r\n   }\r\n ]\r\n}', 2, 1, '2019-02-04 15:38:51', '2019-02-04 15:38:51', 'ACTIVE'),
(3, 'Double Room AC', 'Rooms with Two beds,These type can occupy 4 Guests.It provide air conditioning  throughout the stay.', NULL, 1500, '{\r\n \"services\": [\r\n    {\r\n     \"name\": \"Air Conditioning\",\r\n     \"icon\": \"air_conditioning.png\"\r\n   },\r\n   {\r\n     \"name\": \"Daily Cleaning\",\r\n     \"icon\": \"room_cleaning.png\"\r\n   },\r\n   {\r\n     \"name\": \"Free Parking\",\r\n     \"icon\": \"car_rentel.png\"\r\n   },\r\n   {\r\n     \"name\": \"Television\",\r\n     \"icon\": \"tv_provide.png\"\r\n   }\r\n ]\r\n}', 4, 2, '2019-02-04 15:43:58', '2019-02-04 15:43:58', 'ACTIVE'),
(4, 'Double Room NON-AC', 'Rooms with Two beds,These type can occupy 4 Guests.It not provide air conditioning.', NULL, 1250, '{\r\n \"services\": [\r\n   {\r\n     \"name\": \"Daily Cleaning\",\r\n     \"icon\": \"room_cleaning.png\"\r\n   },\r\n   {\r\n     \"name\": \"Free Parking\",\r\n     \"icon\": \"car_rentel.png\"\r\n   },\r\n   {\r\n     \"name\": \"Television\",\r\n     \"icon\": \"tv_provide.png\"\r\n   }\r\n ]\r\n}', 4, 2, '2019-02-04 15:43:58', '2019-02-04 15:43:58', 'ACTIVE'),
(5, 'Deluxe Rooms', 'Rooms with One bed,These type can occupy 2 Guests.It provide air conditioning throughout the stay.And also provide breakfast. And anytime service.', NULL, 1750, '{\r\n \"services\": [\r\n   {\r\n     \"name\": \"Air Conditioning\",\r\n     \"icon\": \"air_conditioning.png\"\r\n   },\r\n   {\r\n     \"name\": \"Daily Cleaning\",\r\n     \"icon\": \"room_cleaning.png\"\r\n   },\r\n   {\r\n     \"name\": \"Free Parking\",\r\n     \"icon\": \"car_rentel.png\"\r\n   },\r\n   {\r\n     \"name\": \"Swimming Pool\",\r\n     \"icon\": \"swimming_pool.png\"\r\n   },\r\n   {\r\n     \"name\": \"Television\",\r\n     \"icon\": \"tv_provide.png\"\r\n   }\r\n ]\r\n}\r\n', 6, 2, '2019-02-04 15:49:20', '2019-02-04 15:49:20', 'ACTIVE'),
(6, 'Super Deluxe Rooms', 'Rooms with One bed,These type can occupy 2 Guests.It provide air conditioning throughout the stay.And also provide breakfast. And anytime service.And also provide beverages.It will give a King like service.', NULL, 2000, '{\r\n \"services\": [\r\n   {\r\n     \"name\": \"Air Conditioning\",\r\n     \"icon\": \"air_conditioning.png\"\r\n   },\r\n   {\r\n     \"name\": \"Daily Cleaning\",\r\n     \"icon\": \"room_cleaning.png\"\r\n   },\r\n   {\r\n     \"name\": \"Free Parking\",\r\n     \"icon\": \"car_rentel.png\"\r\n   },\r\n   {\r\n     \"name\": \"Swimming Pool\",\r\n     \"icon\": \"swimming_pool.png\"\r\n   },\r\n   {\r\n     \"name\": \"Television\",\r\n     \"icon\": \"tv_provide.png\"\r\n   },\r\n   {\r\n     \"name\": \"Mini Bar\",\r\n     \"icon\": \"bar.png\"\r\n   }\r\n ]\r\n}', 6, 2, '2019-02-04 15:49:20', '2019-02-04 15:49:20', 'ACTIVE');

-- --------------------------------------------------------

--
-- Table structure for table `roomprice`
--

CREATE TABLE `roomprice` (
  `ixRoomPrice` bigint(20) NOT NULL,
  `ixRoomCategory` bigint(20) NOT NULL,
  `mondayPrice` decimal(10,2) NOT NULL,
  `tuesdayPrice` decimal(10,2) NOT NULL,
  `wenesdayPrice` decimal(10,2) NOT NULL,
  `thursdayPrice` decimal(10,2) NOT NULL,
  `fridayPrice` decimal(10,2) NOT NULL,
  `saturdayPrice` decimal(10,2) NOT NULL,
  `sundayPrice` decimal(10,2) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roomprice`
--

INSERT INTO `roomprice` (`ixRoomPrice`, `ixRoomCategory`, `mondayPrice`, `tuesdayPrice`, `wenesdayPrice`, `thursdayPrice`, `fridayPrice`, `saturdayPrice`, `sundayPrice`, `created`, `updated`) VALUES
(1, 1, '1000.00', '1000.00', '1000.00', '1000.00', '1200.00', '1200.00', '1000.00', '2019-02-04 15:59:34', '2019-02-04 15:59:34'),
(2, 2, '750.00', '750.00', '750.00', '750.00', '1000.00', '1000.00', '750.00', '2019-02-04 15:59:34', '2019-02-04 15:59:34'),
(3, 3, '1500.00', '1500.00', '1500.00', '1500.00', '1750.00', '1750.00', '1500.00', '2019-02-04 16:01:30', '2019-02-04 16:01:30'),
(4, 4, '1250.00', '1250.00', '1250.00', '1250.00', '1300.00', '1300.00', '1250.00', '2019-02-04 16:01:30', '2019-02-04 16:01:30'),
(5, 5, '1750.00', '1750.00', '1750.00', '1750.00', '1800.00', '1800.00', '1750.00', '2019-02-04 16:01:30', '2019-02-04 16:01:30'),
(6, 6, '2000.00', '2000.00', '2000.00', '2000.00', '2200.00', '2200.00', '2000.00', '2019-02-04 16:01:30', '2019-02-04 16:01:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoryextraservices`
--
ALTER TABLE `categoryextraservices`
  ADD PRIMARY KEY (`ixCategoryExtraServices`);

--
-- Indexes for table `extraservicereservation`
--
ALTER TABLE `extraservicereservation`
  ADD PRIMARY KEY (`ixExtraServiceReservation`);

--
-- Indexes for table `extraservices`
--
ALTER TABLE `extraservices`
  ADD PRIMARY KEY (`ixExtraServices`);

--
-- Indexes for table `guestdetails`
--
ALTER TABLE `guestdetails`
  ADD PRIMARY KEY (`ixGuestDetails`);

--
-- Indexes for table `guestextraservices`
--
ALTER TABLE `guestextraservices`
  ADD PRIMARY KEY (`ixGuestExtraServices`);

--
-- Indexes for table `paymentdetails`
--
ALTER TABLE `paymentdetails`
  ADD PRIMARY KEY (`ixPaymentDetails`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`ixReservation`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`ixRoom`);

--
-- Indexes for table `roomcategory`
--
ALTER TABLE `roomcategory`
  ADD PRIMARY KEY (`ixRoomCategory`);

--
-- Indexes for table `roomprice`
--
ALTER TABLE `roomprice`
  ADD PRIMARY KEY (`ixRoomPrice`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoryextraservices`
--
ALTER TABLE `categoryextraservices`
  MODIFY `ixCategoryExtraServices` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `extraservicereservation`
--
ALTER TABLE `extraservicereservation`
  MODIFY `ixExtraServiceReservation` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `extraservices`
--
ALTER TABLE `extraservices`
  MODIFY `ixExtraServices` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `guestdetails`
--
ALTER TABLE `guestdetails`
  MODIFY `ixGuestDetails` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `guestextraservices`
--
ALTER TABLE `guestextraservices`
  MODIFY `ixGuestExtraServices` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paymentdetails`
--
ALTER TABLE `paymentdetails`
  MODIFY `ixPaymentDetails` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `ixReservation` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `ixRoom` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `roomcategory`
--
ALTER TABLE `roomcategory`
  MODIFY `ixRoomCategory` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roomprice`
--
ALTER TABLE `roomprice`
  MODIFY `ixRoomPrice` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
